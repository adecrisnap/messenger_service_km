﻿//var jwt = require('jwt-simple');
//var validateUser = require('../routes/auth').validateUser;
var auth = require('./auth.js');
var varpool = require('../config/dbPool.js');
var pool = varpool.createPool();
    
module.exports = function (req, res, next) {

    // When performing a cross domain request, you will recieve
    // a preflighted request first. This is to check if our the app
    // is safe.

    // We skip the token outh for [OPTIONS] requests.
    //if(req.method == 'OPTIONS') next();
    var token = '';
    if (req.headers["authorization"]) token = req.headers["authorization"];
    if (req.headers["Authorization"]) token = req.headers["Authorization"];
    if (token=='') {
        auth.setResponse(res,401,'Unauthorized');
        return;
    }
    else {
        //var sess = req.session;
        //if (sess.sescustomerkey) {
            pool.getConnection(function (err, connection) {
                if (err) {
                    if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                    auth.setResponse(res,401,'Unauthorized');
                    return;
                }
                else {
                    var ids = auth.decrypt(token);
                    var arrs = ids.split('|');
                    if (arrs.length>0) {
                        connection.query('select customer_email,pairkey from t_customers where customer_email=? and pairkey is not null',[arrs[0]], function (err, rows) {
                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                            if (err) { 
                                auth.setResponse(res,401,'Unauthorized');
                                return;
                            }
                            else {
                                if (rows.length > 0) {
                                    var pairkey = auth.decrypt(rows[0].pairkey);
                                    if (pairkey==arrs[1])
                                        next();
                                    else {
                                        auth.setResponse(res,401,'Unauthorized');
                                        return;
                                    }
                                }
                                else {
                                    auth.setResponse(res,401,'Unauthorized');
                                    return;
                                }
                            }
                        });
                    }
                    else {
                        auth.setResponse(res,401,'Unauthorized');
                        return;
                    }
                }
            });
        //}
        //else {
        //    auth.setResponse(res,401,'Session expired');
        //    return;
        //}
    };
    /*
        var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'] || (req.body && req.body.username) ;
        var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'] || (req.body && req.body.api_key);
    
        if (token || key) {
        try {
          //var decoded = jwt.decode(token, require('../config/secret.js')());
          
            if (decoded.exp <= Date.now()) {
                res.status(400);
                res.json({
                "status": 400,
                "message": "Token Expired"
                });
                return;
            }
          
          // Authorize the user to see if s/he can access our resources
     
          auth.validateUser(token,key,function(err,rows){ 
            if (err) {
                res.status(500);
                res.json({
                  "status": 500,
                  "message": "Oops something went wrong",
                  "error": err
                });
                return;
            }
            else {        
                if (rows.length>0) {
                    next(); // To move to next middleware
                } else {
                    res.status(403);
                    res.json({
                    "status": 403,
                    "message": "Not Authorized"
                    });
                    return;
                }
            }
          }); // The key would be the logged in user's username 
        } catch (err) {
            res.status(500);
            res.json({
                "status": 500,
                "message": "Oops something went wrong",
                "error": err
            });
            return;
        }
      } else {
            res.status(401);
            res.json({
            "status": 401,
            "message": "Invalid Token or Key"
            });
            return;
      }
    */
}

