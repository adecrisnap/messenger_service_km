const sql = require('mssql');

var helper = {    
    formatDate : function(dt) {
        var dtx = null;
        if (dt) {
            var dtm = (dt.getMonth()+1).toString();
            var dtt = dt.getDate().toString();
            if (dtt.length===1) dtt = '0' + dtt;
            if (dtm.length===1) dtm = '0' + dtm;
            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            hours = hours < 10 ? '0' +hours : hours;
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            dtx = dt.getFullYear().toString() + '-' + dtm + '-' + dtt + ' ' + strTime;
        }
        return dtx;
    },

    formatDateYearMonth : function(dt) {
        var dtx = null;
        if (dt) {
            var dtm = (dt.getMonth()+1).toString();
            var dtt = dt.getDate().toString();
            if (dtt.length===1) dtt = '0' + dtt;
            if (dtm.length===1) dtm = '0' + dtm;
            dtx = dt.getFullYear().toString() + '-' + dtm;
        }
        return dtx;
    },

    formatDateShort : function(dt) {
        var dtx = null;
        if (dt) {
            var dtm = (dt.getMonth()+1).toString();
            var dtt = dt.getDate().toString();
            if (dtt.length===1) dtt = '0' + dtt;
            if (dtm.length===1) dtm = '0' + dtm;
            dtx = dt.getFullYear().toString() + '-' + dtm + '-' + dtt;
        }
        return dtx;
    },

    formatDateShortWorkingDay : function(dt) {
        var dtx = null;

        var config = {
            user: 'report',
            password: 'KiranaMegatara21',
            server: '10.0.0.32', 
            database: 'ReportTemplate',
            port : 1433,
            requestTimeout : 360000,
            connectionTimeout: 15000,
            pool: {
                max: 10,
                min: 0,
                idleTimeoutMillis: 30000
              },
              options: {
                cryptoCredentialsDetails: {
                    minVersion: 'TLSv1'
                },
                encrypt: false, // for azure
                trustServerCertificate: true // change to true for local dev / self-signed certs
              }
        }
    
        return new Promise(function(resolve,reject){
            const conn = new sql.ConnectionPool(config);
            conn.connect(function(err) {
                if (err) {
                    reject(err);
                }
                else {
                    var request = new sql.Request(conn);
                    request.input("keyword", sql.DateTime, dt);
                    //var sqlstr = "SELECT MAX(ACTDT) tgl FROM ZKISSTT_0138 t WHERE T.HOLDT='' and T.ACTDT<=@keyword";
                    var sqlstr = "SELECT rpt.s_max_date_working_day(@keyword) as tgl";
                    request.query(sqlstr, function (erry, recordset) {
                        if (erry) {
                            conn.close();
                            reject(erry);
                        }
                        else {
                            var ret = recordset.recordsets[0][0].tgl;
                            conn.close();
                            if (ret) {
                                var dtm = (ret.getMonth()+1).toString();
                                var dtt = ret.getDate().toString();
                                if (dtt.length===1) dtt = '0' + dtt;
                                if (dtm.length===1) dtm = '0' + dtm;
                                dtx = new Date(ret.getFullYear().toString() + '-' + dtm + '-' + dtt);
                            }
                            resolve(dtx);
                        }
                    });
                }
            }); 
        });
    },

    secondsDiff : function (d1, d2) {
        //let millisecondDiff = d2 - d1;
        let secDiff = Math.floor( ( d2 - d1) / 1000 );
        return secDiff;
    },

    minutesDiff : function (d1, d2) {
        let seconds = secondsDiff(d1, d2);
        let minutesDiff = Math.floor( seconds / 60 );
        return minutesDiff;
    },

    hoursDiff : function (d1, d2) {
        let minutes = minutesDiff(d1, d2);
        let hoursDiff = Math.floor( minutes / 60 );
        return hoursDiff;
    },

    daysDiff : function (d1, d2) {
        let hours = hoursDiff(d1, d2);
        let daysDiff = Math.floor( hours / 24 );
        return daysDiff;
    },

    weeksDiff : function (d1, d2) {
        let days = daysDiff(d1, d2);
        let weeksDiff = Math.floor( days/ 7 );
        return weeksDiff;
    },

    yearsDiff : function (d1, d2) {
        let date1 = new Date(d1);
        let date2 = new Date(d2);
        let yearsDiff =  date2.getFullYear() - date1.getFullYear();
        return yearsDiff;
    },

    monthsDiff : function (d1, d2) {
        let date1 = new Date(d1);
        let date2 = new Date(d2);
        let years = yearsDiff(d1, d2);
        let months =(years * 12) + (date2.getMonth() - date1.getMonth()) ;
        return months;
    },

    addDays : function(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    },

    addMonth : function(date,m) {
        var result = new Date(date);
        var newDate = new Date(result.setMonth(result.getMonth()+m));
        return newDate;
    }

}

module.exports = helper;