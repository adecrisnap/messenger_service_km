var express = require('express');
var router = express.Router();
var mainOps = require('./mainOps.js');
var copyReport = require('./copyReport')
var kpis = require('./kpiOps.js')
var b = require('./budgetOps.js');
const kpiOps = require('./kpiOps.js');

    //router.get('/getallroleskpi', kpiOps.getAllRoles)
    router.get('/getrolesbyselection/:valhoplant/:nik', kpiOps.getRolesBySelection)
    router.post('/getrolesbyselection/', kpiOps.getRolesBySelection2)
    router.get('/getroleskpi', kpiOps.getRolesKPI)
    router.get('/getrolesroles/:roleid', kpiOps.getRolesRoles)
    router.post('/deleterolesroles', kpiOps.deleteRolesRoles)
    router.post('/setrolesroles', kpiOps.setRolesRoles)
    router.post('/createkpigroup1', kpiOps.createKPIGroup1);
    router.put('/editkpigroup1/:id',kpiOps.editKPIGroup1);
    router.get('/getkpigroups1/:is_active', kpiOps.getKPIGroups1);
    router.get('/getkpigroups1/:is_active/:roleid', kpiOps.getKPIGroups1);
    router.get('/getkpigroup1byid/:id', kpiOps.getKPIGroup1byID);

    router.post('/createkpigroup2', kpiOps.createKPIGroup2);
    router.put('/editkpigroup2/:id',kpiOps.editKPIGroup2);
    router.get('/getkpigroups2/:is_active', kpiOps.getKPIGroups2);
    router.get('/getkpigroups2/:is_active/:roleid', kpiOps.getKPIGroups2);
    router.get('/getkpigroup2byid/:id', kpiOps.getKPIGroup2byID);

    router.post('/createkpigroup3', kpiOps.createKPIGroup3);
    router.put('/editkpigroup3/:id',kpiOps.editKPIGroup3);
    router.get('/getkpigroups3/:is_active', kpiOps.getKPIGroups3);
    router.get('/getkpigroups3/:is_active/:roleid', kpiOps.getKPIGroups3);
    router.get('/getkpigroup3byid/:id', kpiOps.getKPIGroup3byID);

    router.get('/gettopicsbyrole/:roleid', mainOps.getTopicsByRole);
    router.get('/getemployeemaster', mainOps.getEmployeeMaster)
    router.put('/setemployeemaster/:id', mainOps.setEmployeeMaster)

    router.get('/getconfigurationbyid/', kpiOps.getConfigurationByID)
    router.get('/getconfigurationbyid/:roleid', kpiOps.getConfigurationByID)
    router.get('/getconfigurationbytahun/:tahun', kpiOps.getConfigurationByTahun)
    router.get('/getconfigurationbytahun/:tahun/:roleid', kpiOps.getConfigurationByTahun)
    router.get('/gettargetbyid/', kpiOps.getTargetByID)
    router.get('/gettargetbyid/:roleid', kpiOps.getTargetByID)
    
    router.get('/gettargetbytahun/:tahun/:plant', kpiOps.getTargetByTahun)
    router.get('/gettargetbytahun/:tahun/:plant/:roleid', kpiOps.getTargetByTahun)

    router.get('/gettargetbytahunlevelplant/:tahun/:plant/:level', kpiOps.getTargetByTahun)
    router.get('/gettargetbytahunlevelplant/:tahun/:plant/:level/:roleid', kpiOps.getTargetByTahun)
    
    router.get('/getkpisinlevel/:level', kpiOps.getKPISInLevel)
    router.get('/getkpisinlevel/:level/:roleid', kpiOps.getKPISInLevel)

    router.post('/uploadconfiguration', kpiOps.uploadConfiguration)

    router.post('/uploadtargetscore', kpiOps.uploadTargetScore)

    router.post('/getcurrentscores', kpiOps.getCurrentScores)
    router.post('/editscoreskpis', kpiOps.editScoresKPIS)
    router.post('/calculatekpi', kpiOps.calculateKPI)
    router.get('/copyconfigurationbytahun/:tahun/:roleid', kpiOps.copyConfigurationByTahun)
    //router.get('/getkpicalculationstatus', kpiOps.getKPICalculationStatus)
    router.post('/copyreport', async function(req, res){
        copyReport(req.body.sourcereport_id,req.body.destreport_code).then(function(result) {
            if (result!=true) {
                return res.status(400).json({result : 'error'});
            }
            else {
                return res.status(200).json({result : 'ok'});
            }
        });
    });
    
    router.post('/copytopic', mainOps.copyTopic);

    router.post('/createapp', mainOps.createApp);
    router.get('/getapps/:is_active', mainOps.getApps);
    router.get('/getappbyid/:id', mainOps.getAppByID);
    router.put('/editapp/:id', mainOps.editApp);

    router.get('/getmenusbyapp/:id', mainOps.getMenusByApp);
    router.post('/createmenu', mainOps.createMenu);
    router.put('/editmenu/:report_appid/:report_id', mainOps.editMenu);
    router.get('/getmenubyid/:report_appid/:report_id', mainOps.getMenuByID);
    router.put('/setrolemenus/:id', mainOps.setRoleMenus);
    router.post('/createrolemenus/', mainOps.createRoleMenus);
    router.get('/getrolemenus/:roleid', mainOps.getRoleMenus)
    router.get('/getrolemenusdetail/:id', mainOps.getRoleMenusDetail)
    router.post('/loginapp', mainOps.loginApp);
    router.post('/getuserinfobynik', mainOps.getUserInfoByNIK)
    router.post('/createpassword', mainOps.createPassword)
    router.post('/getuserinfobytoken', mainOps.getUserInfobyToken)
    router.post('/getmenus', mainOps.getMenus)


    router.get('/getmasterplants/:plant', b.getMasterPlants);
    router.post('/getmasterdepos', b.getMasterDepos);
    router.get('/forceschedule', mainOps.forceSchedule);

    router.post('/getvolumebyplant', b.getBudgetVolumeByPlant);
    router.post('/getvolumebydepo', b.getBudgetVolumeByDepo);
    router.post('/insertvolumebydepo', b.insertBudgetVolumeByDepo);
    router.post('/editvolumebydepo', b.editBudgetVolumeByDepo);
    router.post('/deletevolumebydepo', b.deleteBudgetVolumeByDepo);

    router.post('/getpmbyplant', b.getBudgetPMByPlant);
    router.post('/getpmbydepo', b.getBudgetPMByDepo);
    router.post('/insertpmbydepo', b.insertBudgetPMByDepo);
    router.post('/editpmbydepo', b.editBudgetPMByDepo);
    router.post('/deletepmbydepo', b.deleteBudgetPMByDepo);

    router.post('/getopttariffbyplant', b.getBudgetOPTTariffByPlant);
    router.post('/getopttariffbydepo', b.getBudgetOPTTariffByDepo);
    router.post('/insertopttariffbydepo', b.insertBudgetOPTTariffByDepo);
    router.post('/editopttariffbydepo', b.editBudgetOPTTariffByDepo);
    router.post('/deleteopttariffbydepo', b.deleteBudgetOPTTariffByDepo);

    router.post('/getoptbyplant', b.getBudgetOPTByPlant);
    router.post('/getoptbydepo', b.getBudgetOPTByDepo);
    router.post('/insertoptbydepo', b.insertBudgetOPTByDepo);
    router.post('/editoptbydepo', b.editBudgetOPTByDepo);
    router.post('/deleteoptbydepo', b.deleteBudgetOPTByDepo);

    router.post('/getoatariffbyplant', b.getBudgetOATariffByPlant);
    router.post('/getoatariffbydepo', b.getBudgetOATariffByDepo);
    router.post('/insertoatariffbydepo', b.insertBudgetOATariffByDepo);
    router.post('/editoatariffbydepo', b.editBudgetOATariffByDepo);
    router.post('/deleteoatariffbydepo', b.deleteBudgetOATariffByDepo);

    router.post('/getoabyplant', b.getBudgetOAByPlant);
    router.post('/getoabydepo', b.getBudgetOAByDepo);
    router.post('/insertoabydepo', b.insertBudgetOAByDepo);
    router.post('/editoabydepo', b.editBudgetOAByDepo);
    router.post('/deleteoabydepo', b.deleteBudgetOAByDepo);

    router.post('/getfeetariffbyplant', b.getBudgetFeeTariffByPlant);
    router.post('/getfeetariffbydepo', b.getBudgetFeeTariffByDepo);
    router.post('/insertfeetariffbydepo', b.insertBudgetFeeTariffByDepo);
    router.post('/editfeetariffbydepo', b.editBudgetFeeTariffByDepo);
    router.post('/deletefeetariffbydepo', b.deleteBudgetFeeTariffByDepo);

    router.post('/getfeebyplant', b.getBudgetFeeByPlant);
    router.post('/getfeebydepo', b.getBudgetFeeByDepo);
    router.post('/insertfeebydepo', b.insertBudgetFeeByDepo);
    router.post('/editfeebydepo', b.editBudgetFeeByDepo);
    router.post('/deletefeebydepo', b.deleteBudgetFeeByDepo);

    router.post('/getkadarbyplant', b.getBudgetKadarByPlant);
    router.post('/getkadarbydepo', b.getBudgetKadarByDepo);
    router.post('/insertkadarbydepo', b.insertBudgetKadarByDepo);
    router.post('/editkadarbydepo', b.editBudgetKadarByDepo);
    router.post('/deletekadarbydepo', b.deleteBudgetKadarByDepo);

    router.get('/emailforcerun/:id', mainOps.emailForceRun);
    router.get('/getplants', mainOps.getPlants);
    router.get('/getlogs', mainOps.getLogs);

    router.get('/getcolumns/:id_report', mainOps.getColumns);
    router.get('/getcolumnbyid/:id', mainOps.getColumnByID);
    router.put('/editcolumn/:id', mainOps.editColumn);
    router.post('/createcolumn', mainOps.createColumn);
    router.delete('/deletecolumn/:id', mainOps.deleteColumn);

    router.post('/createserver', mainOps.createServer);
    router.put('/editserver/:id', mainOps.editServer);
    router.get('/getservers/:is_active', mainOps.getServers);
    router.get('/getserverbyid/:id', mainOps.getServerByID);

    router.post('/createmailserver', mainOps.createMailServer);
    router.put('/editmailserver/:id', mainOps.editMailServer);
    router.get('/getmailservers/:is_active', mainOps.getMailServers);
    router.get('/getmailserverbyid/:id', mainOps.getMailServerByID);
    

    router.post('/createrole', mainOps.createRole);
    router.put('/editrole/:id', mainOps.editRole);
    router.delete('/deleterole/:id', mainOps.deleteRole);

    router.post('/createrolemember', mainOps.createRoleMember);
    router.post('/createexrolemember', mainOps.createExRoleMember);
    router.get('/getroleplants/:id', mainOps.getRolePlants);
    router.get('/getrolemembers/:roleid', mainOps.getRoleMembers);
    router.get('/getexrolemembers/:roleid', mainOps.getExRoleMembers);
    router.delete('/deleterolemember/:id', mainOps.deleteRoleMember);
    router.delete('/deleteexrolemember/:id', mainOps.deleteExRoleMember);
    router.get('/getemployees/:roleid', mainOps.getEmployees);
    router.get('/getemployees2/:roleid', mainOps.getEmployees2);
    router.post('/getemployeebyid/', mainOps.getEmployeeByID);

    router.get('/getroles/:is_active', mainOps.getRoles);
    router.get('/getrolebyid/:id', mainOps.getRoleByID);
    router.get('/getemployeesbyrole/:id', mainOps.getEmployeesByRole);
    //router.get('/gettopicmessagetypesbyrole/:id', mainOps.getTopicMessageTypesByRole);

    router.post('/createreport', mainOps.createReport);
    router.put('/editreport/:id', mainOps.editReport);
    router.delete('/deletereport/:id', mainOps.deleteReport);
    router.get('/getreports/:is_active', mainOps.getReports);
    router.get('/getreportbyid/:id', mainOps.getReportByID);
    router.get('/gettopicsbyreport/:id', mainOps.getTopicsByReport);
    router.post('/alignschedule', mainOps.alignSchedule);

    router.post('/createtopic', mainOps.createTopic);
    router.put('/edittopic/:id', mainOps.editTopic);
    router.put('/edittopicwa/:id', mainOps.editTopicWA);
    router.put('/edittopicschedule/', mainOps.editTopicSchedule);
    router.delete('/deletetopic/:id', mainOps.deleteTopic);
    router.get('/gettopics/:is_active', mainOps.getTopics);
    router.get('/gettopicserror', mainOps.getTopicsError);
    router.get('/geterror', mainOps.getError);
    router.get('/gettopicbyid/:id', mainOps.getTopicByID);
    router.post('/addreporttotpic', mainOps.addMessageTypeRoleToTopic);
    router.delete('/removereportfromtopic/:id', mainOps.removeReportFromTopic);
    router.get('/getreportsbytopic/:id', mainOps.getReportsByTopic);
    router.post('/addmessagetyperoletotopic', mainOps.addMessageTypeRoleToTopic);
    router.delete('/removemessagetyperoletotopic/:id', mainOps.removeReportFromTopic);
    router.get('/getrolesbytopic/:id', mainOps.getRolesByTopic);


module.exports = router;
