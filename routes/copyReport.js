var mongo = require('mongodb');
var url = require('../config/mgConfig.js');
var dbname = require('../config/dbPool');

var asyncx = require('async');
var ObjectId = require('mongodb').ObjectId;
//const { resolve } = require('path');

/*
const copyReport = async (sourcereport_id,destreport_code) => {
    var MongoClient = mongo.MongoClient;
    var rst = false;

    const client = new MongoClient(url.getDbString());
    await client.connect();
    const session = client.startSession();
    const transactionOptions = {
        readPreference: 'primary',
        readConcern: { level: 'local' },
        writeConcern: { w: 'majority' }
    };
    try {
        await session.withTransaction(async () => {
            
            var query1 = { _id : new ObjectId(sourcereport_id)};
            var query2 = { id_report : new ObjectId(sourcereport_id)}
            const db =  client.db(dbname.getDbName());
            const coll1 = await db.collection('t_reports').find(query1).toArray();
            const coll1a = db.collection('t_reports');
            var myobj = { 
                reportname: coll1[0].reportname, 
                functionname: coll1[0].functionname, 
                reportcode : destreport_code,
                reportheader : coll1[0].reportheader,
                reportfooter : coll1[0].reportfooter,
                requestedby : coll1[0].requestedby,
                serveraddress : new ObjectId(coll1[0].serveraddress),
                lastmodified : new Date(),
                is_active : coll1[0].is_active,
                plantcolumn : coll1[0].plantcolumn,
                isfooter : coll1[0].isfooter,
                isplantvisible : coll1[0].isplantvisible,
                isperorganization : coll1[0].isperorganization
            };
            let result = await coll1a.insertOne(myobj, {session});
            var id = result.insertedId;
            const coll2 = await db.collection('t_columns').find(query2).toArray();
            const coll2a = db.collection('t_columns');
            asyncx.eachSeries(coll2,async function(obj,callback){
                var myobj = { 
                    id_report : new ObjectId(id),
                    columnname : obj.columnname,
                    displayname : obj.displayname,
                    textalignment : obj.textalignment,
                    formatcolumn : obj.formatcolumn,
                    columnhighlight : obj.columnhighlight,    
                    columnconditional : obj.columnconditional,
                    columntarget : obj.columntarget,
                    columnactual : obj.columnactual,
                    columntarget2 : obj.columntarget2,
                    columnactual2 : obj.columnactual2,
                    columnoperand1 : obj.columnoperand1,
                    columnoperand2 : obj.columnoperand2,
                    lastmodified : new Date(),
                    columncoloractual : obj.columncoloractual,
                    columncoloractual2 : obj.columncoloractual2,
                    columntarget3 : obj.columntarget3,
                    columnactual3 : obj.columnactual3,
                    columnoperand3 : obj.columnoperand3,
                    columncoloractual3 : obj.columncoloractual3
                };
                await coll2a.insertOne(myobj, { session });
                return callback();
            }, async function(err){
                if (err) {
                    throw err;
                }
                else {
                    await session.commitTransaction();
                    rst = true;
                }
            });
        }, transactionOptions);
    }
    catch(err) {
        console.log(err)
        await session.abortTransaction();
        rst = false;
    }
    finally {
        await session.endSession();
        await client.close();
        return rst;
    }
  


}
*/

const copyReport = async (sourcereport_id,destreport_code) => {
        var MongoClient = mongo.MongoClient;
        var rst = false;
        var cols = [];

        const client = new MongoClient(url.getDbString());
        await client.connect();
        try {
            var query1 = { _id : new ObjectId(sourcereport_id)};
            var query2 = { id_report : new ObjectId(sourcereport_id)}
            var query3 = { report_code : destreport_code}
            const db =  client.db(dbname.getDbName());
            const collfind = await db.collection('t_reports').find(query3).toArray();
            if (collfind.length>0) {
                rst = false;
            }
            else {
                const coll1 = await db.collection('t_reports').find(query1).toArray();
                const coll1a = db.collection('t_reports');
                var myobj = { 
                    reportname: coll1[0].reportname, 
                    functionname: coll1[0].functionname, 
                    reportcode : destreport_code,
                    reportheader : coll1[0].reportheader,
                    reportfooter : coll1[0].reportfooter,
                    requestedby : coll1[0].requestedby,
                    serveraddress : new ObjectId(coll1[0].serveraddress),
                    lastmodified : new Date(),
                    is_active : coll1[0].is_active,
                    plantcolumn : coll1[0].plantcolumn,
                    isfooter : coll1[0].isfooter,
                    isplantvisible : coll1[0].isplantvisible,
                    isperorganization : coll1[0].isperorganization
                };
                let result = await coll1a.insertOne(myobj);
                var id = result.insertedId;
                const coll2 = await db.collection('t_columns').find(query2).toArray();
                const coll2a = db.collection('t_columns');
                asyncx.eachSeries(coll2,function(obj,callback){
                    var myobj = { 
                        id_report : new ObjectId(id),
                        columnname : obj.columnname,
                        displayname : obj.displayname,
                        textalignment : obj.textalignment,
                        formatcolumn : obj.formatcolumn,
                        columnhighlight : obj.columnhighlight,    
                        columnconditional : obj.columnconditional,
                        columntarget : obj.columntarget,
                        columnactual : obj.columnactual,
                        columntarget2 : obj.columntarget2,
                        columnactual2 : obj.columnactual2,
                        columnoperand1 : obj.columnoperand1,
                        columnoperand2 : obj.columnoperand2,
                        lastmodified : new Date(),
                        columncoloractual : obj.columncoloractual,
                        columncoloractual2 : obj.columncoloractual2,
                        columntarget3 : obj.columntarget3,
                        columnactual3 : obj.columnactual3,
                        columnoperand3 : obj.columnoperand3,
                        columncoloractual3 : obj.columncoloractual3
                    };
                    cols.push(myobj)
                    return callback();
                }, function(err){
                    if (err) {
                        console.log('sini ' + err);
                        rst=false;
                    }
                    else {
                        coll2a.insertMany(cols)
                        rst = true;
                    }
                });
            }
        }
        catch(err) {
            console.log(err)
            rst = false;
        }
        finally {
            await client.close();
            return rst;
        }
      
    

}
module.exports = copyReport