//var url = require('../config/mgConfig.js');
var dbsql = require('../config/dbSQLPool.js');
var auth = require('../middlewares/auth.js');
const sql = require('mssql');

var budgetOps = {
    //MASTER
        getMasterPlants : function(req,res) {
            var plant = req.params.plant;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName2 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.query("SELECT * FROM dbo.vw_tbl_acc_master_plant where plant=@plant order by plant", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getMasterDepos : function(req,res) {
            var plant = req.body.plant;
            var jenis = req.body.jenis;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName1
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('jnsdp', sql.VarChar, jenis);
                    request.query("SELECT * FROM dbo.ZKISSTT_0113 where plant=@plant and jnsdp=@jnsdp order by plant,depnm", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

    //VOLUME
        getBudgetVolumeByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_VOLUME where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetVolumeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_VOLUME where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetVolumeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_volume (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetVolumeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_volume set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_volume set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_volume set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_volume set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_volume set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_volume set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_volume set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_volume set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_volume set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_volume set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_volume set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_volume set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetVolumeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_volume where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },
    
    //PM
        getBudgetPMByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_PM where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetPMByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_PM where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetPMByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_PM (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetPMByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_PM set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_PM set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_PM set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_PM set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_PM set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_PM set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_PM set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_PM set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_PM set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_PM set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_PM set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_PM set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetPMByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_PM where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },
 
    //OPT Tariff
        getBudgetOPTTariffByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OPT_Tariff where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetOPTTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OPT_Tariff where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetOPTTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_OPT_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetOPTTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_OPT_Tariff set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_OPT_Tariff set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_OPT_Tariff set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_OPT_Tariff set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_OPT_Tariff set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_OPT_Tariff set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_OPT_Tariff set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_OPT_Tariff set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_OPT_Tariff set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_OPT_Tariff set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_OPT_Tariff set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_OPT_Tariff set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetOPTTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_OPT_Tariff where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },
    
    //OPT
        getBudgetOPTByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OPT where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetOPTByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OPT where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetOPTByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_OPT (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetOPTByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_OPT set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_OPT set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_OPT set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_OPT set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_OPT set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_OPT set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_OPT set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_OPT set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_OPT set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_OPT set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_OPT set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_OPT set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetOPTByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_OPT where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },

    //OA Tariff
        getBudgetOATariffByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OA_Tariff where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetOATariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OA_Tariff where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetOATariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_OA_Tariff (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetOATariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_OA_Tariff set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_OA_Tariff set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_OA_Tariff set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_OA_Tariff set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_OA_Tariff set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_OA_Tariff set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_OA_Tariff set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_OA_Tariff set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_OA_Tariff set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_OA_Tariff set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_OA_Tariff set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_OA_Tariff set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetOATariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_OA_Tariff where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },
    
    //OA
        getBudgetOAByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OA where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetOAByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_OA where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetOAByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_OA (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetOAByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_OA set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_OA set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_OA set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_OA set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_OA set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_OA set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_OA set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_OA set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_OA set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_OA set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_OA set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_OA set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetOAByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_OA where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },

    //FEE Tariff
        getBudgetFeeTariffByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_FEE_Tariff where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetFeeTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_Fee_Tariff where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetFeeTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_Fee_TARIFF (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetFeeTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_Fee_TARIFF set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_Fee_TARIFF set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_Fee_TARIFF set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_Fee_TARIFF set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_Fee_TARIFF set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_Fee_TARIFF set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_Fee_TARIFF set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_Fee_TARIFF set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_Fee_TARIFF set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_Fee_TARIFF set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_Fee_TARIFF set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_Fee_TARIFF set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetFeeTariffByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_Fee_TARIFF where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },

    //FEE
        getBudgetFeeByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_FEE where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetFeeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.vw_depo_Fee where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetFeeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,1,@jan)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,2,@feb)", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,3,@mar)", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,4,@apr)", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,5,@may)", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,6,@jun)", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,7,@jul)", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,8,@aug)", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,9,@sep)", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,10,@oct)", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,11,@nov)", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("insert into t_sp_Fee (plant,depo,year,month,nilai) values (@plant,@depo,@year,12,@dece)", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        editBudgetFeeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var jan = req.body.jan;
            var feb = req.body.feb;
            var mar = req.body.mar;
            var apr = req.body.apr;
            var may = req.body.may;
            var jun = req.body.jun;
            var jul = req.body.jul;
            var aug = req.body.aug;
            var sep = req.body.sep;
            var oct = req.body.oct;
            var nov = req.body.nov;
            var dece = req.body.dece;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('jan', sql.Numeric, jan);
                    request.query("update t_sp_Fee set nilai=@jan where plant=@plant and depo=@depo and year=@year and month=1", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var request = new sql.Request();
                            request.input('plant', sql.VarChar, plant);
                            request.input('depo', sql.VarChar, depo);
                            request.input('year', sql.Int, thn);
                            request.input('feb', sql.Numeric, feb);
                            request.query("update t_sp_Fee set nilai=@feb where plant=@plant and depo=@depo and year=@year and month=2", function (err, recordset) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request();
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('depo', sql.VarChar, depo);
                                    request.input('year', sql.Int, thn);
                                    request.input('mar', sql.Numeric, mar);
                                    request.query("update t_sp_Fee set nilai=@mar where plant=@plant and depo=@depo and year=@year and month=3", function (err, recordset) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('plant', sql.VarChar, plant);
                                            request.input('depo', sql.VarChar, depo);
                                            request.input('year', sql.Int, thn);
                                            request.input('apr', sql.Numeric, apr);
                                            request.query("update t_sp_Fee set nilai=@apr where plant=@plant and depo=@depo and year=@year and month=4", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var request = new sql.Request();
                                                    request.input('plant', sql.VarChar, plant);
                                                    request.input('depo', sql.VarChar, depo);
                                                    request.input('year', sql.Int, thn);
                                                    request.input('may', sql.Numeric, may);
                                                    request.query("update t_sp_Fee set nilai=@may where plant=@plant and depo=@depo and year=@year and month=5", function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            var request = new sql.Request();
                                                            request.input('plant', sql.VarChar, plant);
                                                            request.input('depo', sql.VarChar, depo);
                                                            request.input('year', sql.Int, thn);
                                                            request.input('jun', sql.Numeric, jun);
                                                            request.query("update t_sp_Fee set nilai=@jun where plant=@plant and depo=@depo and year=@year and month=6", function (err, recordset) {
                                                                if (err) {
                                                                    sql.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.input('plant', sql.VarChar, plant);
                                                                    request.input('depo', sql.VarChar, depo);
                                                                    request.input('year', sql.Int, thn);
                                                                    request.input('jul', sql.Numeric, jul);
                                                                    request.query("update t_sp_Fee set nilai=@jul where plant=@plant and depo=@depo and year=@year and month=7", function (err, recordset) {
                                                                        if (err) {
                                                                            sql.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            var request = new sql.Request();
                                                                            request.input('plant', sql.VarChar, plant);
                                                                            request.input('depo', sql.VarChar, depo);
                                                                            request.input('year', sql.Int, thn);
                                                                            request.input('aug', sql.Numeric, aug);
                                                                            request.query("update t_sp_Fee set nilai=@aug where plant=@plant and depo=@depo and year=@year and month=8", function (err, recordset) {
                                                                                if (err) {
                                                                                    sql.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    var request = new sql.Request();
                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                    request.input('year', sql.Int, thn);
                                                                                    request.input('sep', sql.Numeric, sep);
                                                                                    request.query("update t_sp_Fee set nilai=@sep where plant=@plant and depo=@depo and year=@year and month=9", function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            auth.setResponse(res,400,err);
                                                                                            return;
                                                                                        }
                                                                                        else {
                                                                                            var request = new sql.Request();
                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                            request.input('year', sql.Int, thn);
                                                                                            request.input('oct', sql.Numeric, oct);
                                                                                            request.query("update t_sp_Fee set nilai=@oct where plant=@plant and depo=@depo and year=@year and month=10", function (err, recordset) {
                                                                                                if (err) {
                                                                                                    sql.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    var request = new sql.Request();
                                                                                                    request.input('plant', sql.VarChar, plant);
                                                                                                    request.input('depo', sql.VarChar, depo);
                                                                                                    request.input('year', sql.Int, thn);
                                                                                                    request.input('nov', sql.Numeric, nov);
                                                                                                    request.query("update t_sp_Fee set nilai=@nov where plant=@plant and depo=@depo and year=@year and month=11", function (err, recordset) {
                                                                                                        if (err) {
                                                                                                            sql.close();
                                                                                                            auth.setResponse(res,400,err);
                                                                                                            return;
                                                                                                        }
                                                                                                        else {
                                                                                                            var request = new sql.Request();
                                                                                                            request.input('plant', sql.VarChar, plant);
                                                                                                            request.input('depo', sql.VarChar, depo);
                                                                                                            request.input('year', sql.Int, thn);
                                                                                                            request.input('dece', sql.Numeric, dece);
                                                                                                            request.query("update t_sp_Fee set nilai=@dece where plant=@plant and depo=@depo and year=@year and month=12", function (err, recordset) {
                                                                                                                if (err) {
                                                                                                                    sql.close();
                                                                                                                    auth.setResponse(res,400,err);
                                                                                                                    return;
                                                                                                                }
                                                                                                                else {
                                                                                                                    res.status(200).json({message : 'success'});
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });    
        },

        deleteBudgetFeeByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_Fee where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },

    //KADAR
        getBudgetKadarByPlant : function(req,res) {
            var plant = req.body.plant;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.T_sp_Kadar where plant=@plant and year=@year order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        getBudgetKadarByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("SELECT * FROM rpt.t_sp_kadar where plant=@plant and year=@year and depo=@depo order by plant,depo", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });    
        },

        insertBudgetKadarByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var nilai = req.body.nilai;
            
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('nilai', sql.Numeric, nilai);
                    request.query("insert into t_sp_kadar (plant,depo,year,nilai) values (@plant,@depo,@year,@nilai)", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });    
        },

        editBudgetKadarByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var nilai = req.body.nilai;
           
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.input('nilai', sql.Numeric, nilai);
                    request.query("update t_sp_kadar set nilai=@nilai where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });    
        },

        deleteBudgetKadarByDepo : function(req,res) {
            var plant = req.body.plant;
            var depo = req.body.depo;
            var thn = req.body.thn;
            var config = {
                user: dbsql.getUser,
                password: dbsql.getPwd,
                server: dbsql.getServer, 
                database: dbsql.getDbName3 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('plant', sql.VarChar, plant);
                    request.input('depo', sql.VarChar, depo);
                    request.input('year', sql.Int, thn);
                    request.query("delete from t_sp_kadar where plant=@plant and depo=@depo and year=@year", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json({message : 'success'});
                        }
                    });
                }
            });   
        },
}

module.exports = budgetOps;
