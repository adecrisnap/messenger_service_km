var mongo = require('mongodb');
var url = require('../config/mgConfig.js');
var dbname = require('../config/dbPool');
var auth = require('../middlewares/auth.js');
var helper = require('../middlewares/helper');
var async = require('async');
var ObjectId = require('mongodb').ObjectId;
const sql = require('mssql');
var nodemailer = require('nodemailer');
const configDb = {
    user: 'report',
    password: 'KiranaMegatara21',
    server: '10.0.0.32', 
    database: 'ReportTemplate',
    port : 1433 
}
var topicname = '';
var topiccode = '';
var topicid = '';


    var mainOps = {
	getTopicsByRole : function(req,res) {
            const { roleid } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var rst = []
                    var query = { is_active : 1 };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                async.eachSeries(result,function(obj,callback){
                                    obj.emailroles.filter(function(item) {
                                        if (item.value==roleid)  {                                            
                                            rst.push({
                                                _id : new ObjectId(obj._id),
                                                topiccode : obj.topiccode,
                                                topicname : obj.topicname,
						lastmodified : obj.lastmodified
                                            })
                                        }
                                    });
                                        
                                    return callback()
                                },function(err) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        var rstF = removeDuplicates(rst)
                                        return res.status(200).json(rstF);                                    }
                                });   
                            }
                            else {
                                db.close();
                                return res.status(200).json([]);
                            }
                        }
                    })
                }
            });
        },

        forceSchedule : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    writeErrorLog('Step align schedule 1', err)
                    //auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find({topiccode : 'TPUR-025'}).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            writeErrorLog('Step align schedule 2', err)
                            //auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            async.eachSeries(result, function(obj,callback){
                                var tm = '07:00';
                                var dtnew = obj.emaillastrun;
                                var tmnew = obj.startdate;
                                
                                //console.log('tmnew ' + tmnew);
                                var hr = tmnew.getHours();
                                //console.log('hr lama ' + hr)
                                if (obj.emailscheduleselected==4) {
                                    hr = hr-4;
                                    dtnew = new Date();
                                }
                                if (obj.emailscheduleselected==8) {
                                    hr = hr-8;
                                    dtnew = new Date();
                                } 
                                //console.log('hr baru ' + hr)
                                if (tmnew!=null && tmnew!=undefined) tm = hr.toString() + ':' + tmnew.getMinutes().toString();
                                //console.log('itu ' + tm);
                                var dt1 = dtnew.getFullYear().toString() + '-' + (dtnew.getMonth()+1).toString() + '-' + dtnew.getDate().toString() + ' ' + tm; 
                                var dt = new Date(dt1);
                                //console.log('ini ' + dt)
                                
                                newvalues = { $set: 
                                    {
                                        emaillastrun : dt
                                    }
                                    
                                };
                                dbo.collection("t_topics").updateOne({_id : new ObjectId(obj._id)}, newvalues, function(err, result2) {
                                    if (err) {
                                        return callback(err);
                                    }
                                    else {
                                        return callback();
                                    }
                                });
                            }, function(err3){
                                if (err3) {
                                    writeErrorLog('Error update schedule',err3);
                                    db.close();
                                    //auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    return res.status(200).json('success');
                                }
                            });
                        }
                    });
                }
            });
        },


        emailForceRun : function(req,res) {
            var id = req.params.id;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    writeErrorLog('Step 1 - Connect mongodb',err);
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var jsonTopFinal = [];

                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics")
                        .aggregate([
                            { 
                                $match:
                                {
                                    _id : new ObjectId(id)
                                },
                            },
                            {
                                $project: 
                                {
                                    _id : true,
                                    topicname : true,
                                    topiccode : true,
                                    reports : true,
                                    emailroles : true,
                                    emailscheduleselected : true,
                                    emailsuccess : true,
                                    emaillastrun : true,
                                    forceRun : true,
                                    lasteighthours : {
                            
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 8
                                            ]        
                                    
                                    },
                                    lastfivehours : {
                                    
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 5
                                            ]        
                                    
                                    },
                                    lastfourhours : {
                                    
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 4
                                            ]        
                                    
                                    },
                                    lastrunday : {
                                    
                                                $divide: 
                                                    [    
                                                        { 
                                                            $subtract: [new Date(), '$emaillastrun'] 
                                                        },  1000 * 60 * 60 * 24
                                                    ]        
                                         
                                    },
                                    lastrunweek : {
                                       
                                                $divide: 
                                                    [
                                                        {$divide: 
                                                            [
                                                                { 
                                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                                },  1000 * 60 * 60 * 24
                                                            ]
                                                        }, 7
                                                    ]            
                                       
                                    },
                                    lastrunmonth : {
                                       
                                                $divide:
                                                [
                                                    {$divide: 
                                                        [
                                                            { 
                                                                $subtract: [new Date(), '$emaillastrun'] 
                                                            },  (1000 * 60 * 60 * 24)
                                                        ]
                                                    }, 30
                                                ]    
                                         
                                    },
                                },
                               
                            },
                            /*
                            {
                                $match:
                                { 
                                    $or : 
                                        [ 
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunday : { $gt : 0.5}, emailscheduleselected :0 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunweek : { $gt : 0.5}, emailscheduleselected :1 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunmonth : { $gt : 0.5}, emailscheduleselected :2 }
                                                    ]
                                            },
                                        ]
                                },
                            },
                            */
                            {   
                                
                                $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'reports.value',
                                    foreignField: '_id',
                                    as: 'reportdetails'
                                },
                               
                                    
                                   
                            },
                            {
                                $sort: {"reportdetails.reportcode": 1}
                            },
                            {
                                $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'emailroles.value',
                                    foreignField: '_id',
                                    as: 'roledetails'
                                },
                            },
                        ])
                        .toArray(function(errB,result) { 
                            if (errB) {
                                db.close();
                                console.log('halo 1');
                                writeErrorLog('Step 2 - to Array',errB);
                                auth.setResponse(res,400,errB);
                                return;
                            }
                            else {
                                async.eachSeries(result, function(objT, callback00) {
                                    var topics = {
                                        _id : new ObjectId(objT._id),
                                        topicname : objT.topicname,
                                        topiccode : objT.topiccode,
                                        reports : [],
                                        special_recipients : [],
                                        HR_recipients : [],
                                        ex_recipients : []
                                    }
                                    /*
                                        var myobj = { 
                                            topiccode : objT.topiccode,
                                            topicname : objT.topicname,
                                            emaillastrun : null,
                                            statusemail : null,
                                            emailforcerun : lastrun,
                                            statusemailforcerun : 'Success',
                                            msg1lastrun : null,
                                            statusmsg1 : null,
                                            msg1forcerun : null,
                                            statusmsg1forcerun : null,    
                                            lastmodified : lastrun,
                                        };
                                        dbo.collection("t_logs").insertOne(myobj, function(err, result) {
                                            if (err) {
                                                writeErrorLog('Step 2 - to Array',errB);
                                                return callback00(err);
                                            }
                                            else {
                                    */
                                    var myquery = { _id : new ObjectId(objT._id)};
                                    var forceRun = (objT.forceRun==null || objT.forceRun==undefined) ? []: objT.forceRun
                                    forceRun.push({value : new Date()});
                                    var newvalues = { $set: 
                                        {
                                            forceRun : forceRun
                                        } 
                                    };
                                    dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                                       if (err) {
                                        console.log('halo 3');
                                           writeErrorLog('Step 2b',err);
                                           return callback00(err);
                                        }
                                        else {
                                            async.eachSeries(objT.reportdetails, function(obj1,callback1){
                                                if (obj1.is_active==1) {
                                                    dbo.collection("t_servers").find({_id : new ObjectId(obj1.serveraddress)}).toArray(function(err0,servernames){
                                                        if (err0) {
                                                            writeErrorLog('Step 3 - t_servers',err0);
                                                            return callback1(err0);
                                                        }
                                                        else {
                                                            var config = null;
                                                            if (servernames[0].servertype==0) { 
                                                                config = {
                                                                    user: servernames[0].username,
                                                                    password: auth.decrypt(servernames[0].pwd),
                                                                    server: servernames[0].serveraddress, 
                                                                    database: servernames[0].dbname,
                                                                    port : 1433,
                                                                    requestTimeout : 360000,
                                                                    connectionTimeout: 15000
                                                                }
                                                            }
                                                            sql.connect(config, function (errx) {
                                                                if (errx) {
                                                                    sql.close();
                                                                    writeErrorLog('Step 3 - SQL Connect',errx);
                                                                    return callback1(errx);
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.execute(obj1.functionname, function (erry, recordset) {
                                                                        if (erry) {
                                                                            sql.close();
                                                                            writeErrorLog('Step 5 - ' + obj1.functionname, erry);
                                                                            return callback1(erry);
                                                                        }
                                                                        else {
                                                                            sql.close();
                                                                            var query2 = { id_report : obj1._id };
                                                                            dbo.collection("t_columns").find(query2).toArray(function(err1, colnames) {
                                                                                if (err1) {
                                                                                    writeErrorLog('Step 6 - Column Execute',err1);
                                                                                    return callback1(err1);
                                                                                }
                                                                                else {
                                                                                    topics.reports.push({
                                                                                        reportname : obj1.reportname,
                                                                                        reportcode : obj1.reportcode,
                                                                                        reportheader : obj1.reportheader,
                                                                                        reportfooter : obj1.reportfooter,
                                                                                        requestedby : obj1.requestedby,
                                                                                        plantcolumn : obj1.plantcolumn,
                                                                                        isfooter : obj1.isfooter,
                                                                                        columnnames : colnames,
                                                                                        isperorganization : (obj1.isperorganization!=null && obj1.isperorganization!=undefined) ? obj1.isperorganization : 0,
                                                                                        isplantvisible : (obj1.isplantvisible!=null && obj1.isplantvisible!=undefined) ? obj1.isplantvisible : 0,
                                                                                        rows : recordset.recordsets[0]
                                                                                    });
                                                                                    return callback1();
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                else {
                                                    return callback1();
                                                }
                                            }, function(err0){
                                                if (err0) {
                                                    writeErrorLog('Step 7 - Summary',err0);
                                                    return callback00(err0);
                                                }
                                                else {
                                                    /*
                                                        var config2 = null;
                                                        config2 = {
                                                        user: 'report',
                                                        password: 'KiranaMegatara21',//auth.decrypt(servernames2[0].pwd),
                                                        server: '10.0.0.32', 
                                                        database: 'ReportTemplate',
                                                        port : 1433,
                                                        requestTimeout : 360000,
                                                        connectionTimeout: 15000 

                                                        }
                                                        sql.connect(config2, function (errx) {
                                                            if (errx) {
                                                                sql.close();
                                                                writeErrorLog('Step 9 - SQL Execute',errx);
                                                                return callback00(errx); 
                                                            }
                                                            else {
                                                    */
                                                    async.eachSeries(objT.roledetails, function(obj2,callback0z){
                                                        if (obj2.is_active==1) {
                                                            dbo.collection("t_servers").find({_id : new ObjectId(obj2.dbserver)}).toArray(function(err4,servernames2){
                                                                if (err4) {
                                                                    writeErrorLog('Step 8 - t_servers',err4);
                                                                    return callback0z(err4);
                                                                }
                                                                else {
                                                                    var config = null;
                                                                    if (servernames2[0].servertype==0) { 
                                                                        config = {
                                                                            user: servernames2[0].username,
                                                                            password: auth.decrypt(servernames2[0].pwd),
                                                                            server: servernames2[0].serveraddress, 
                                                                            database: servernames2[0].dbname,
                                                                            port : 1433,
                                                                            requestTimeout : 360000,
                                                                            connectionTimeout: 15000
                                                                        }
                                                                    }
                                                                    sql.connect(config, function (errx) {
                                                                        if (errx) {
                                                                            sql.close();
                                                                            writeErrorLog('Step 3 - SQL Connect',errx);
                                                                            return callback0z(errx);
                                                                        }
                                                                        else {
                                                                            var keyword = obj2.keyword;
                                                                            if (keyword=='') {
                                                                                sql.close();
                                                                                jsonEmp = [];
                                                                                var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                    if (err2) {
                                                                                        writeErrorLog('Step 10 - Role members',err2);
                                                                                        return callback0z(err2);
                                                                                    }
                                                                                    else {
                                                                                        async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                            topics.special_recipients.push({ 
                                                                                                nik : obj3.nik,
                                                                                                nama : obj3.nama,
                                                                                                email : obj3.email,
                                                                                                plants : obj3.plantselected,
                                                                                                posisi : obj3.posisi,
                                                                                                divisi : obj3.divisi,
                                                                                                dept : obj3.dept
                                                                                            });
                                                                                            //send email
                                                                                            return callback3();
                                                                                        }, function(err3){
                                                                                            if (err3) {
                                                                                                writeErrorLog('Step 11 - Summary',err3);
                                                                                                return callback0z(err3);
                                                                                            }
                                                                                            else {
                                                                                                var activateExclude = 1;
                                                                                                if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                if (activateExclude==0) {
                                                                                                    return callback0z();
                                                                                                }
                                                                                                else {
                                                                                                    var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                // console.log(JSON.stringify(obj2));
                                                                                                    dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                    //    console.log(JSON.stringify(rolemembers))
                                                                                                        if (err2) {
                                                                                                            writeErrorLog('Step 11a',err2);
                                                                                                            return callback0z(err2);
                                                                                                        }
                                                                                                        else {
                                                                                                            async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                topics.ex_recipients.push({ 
                                                                                                                    nik : obj3.nik,
                                                                                                                    nama : obj3.nama,
                                                                                                                    email : obj3.email,
                                                                                                                    plants : obj3.plantselected,
                                                                                                                    posisi : obj3.posisi
                                                                                                                });
                                                                                                                //send email
                                                                                                                return callback3();
                                                                                                            }, function(err3) {
                                                                                                                if (err3) {
                                                                                                                    writeErrorLog('Step 11a',err3);
                                                                                                                    return callback0z(err3);
                                                                                                                }
                                                                                                                else {
                                                                                                                    return callback0z();
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                var request = new sql.Request();
                                                                                var arrKeyword = keyword.split(';');
                                                                                var strFilter = '';
                                                                                var strFilter2 = '';
                                                                                var strFilter3 = '';
                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter+="t1.posst like @" + i + "keyword OR ";
                                                                                }
                                                                                //strFilter = strFilter.substring(0,strFilter.length-4);
                                                                                
                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword1", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter2+="t3.nama like @" + i + "keyword1 OR ";
                                                                                }
                                                                                //strFilter2 = strFilter2.substring(0,strFilter2.length-4);

                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword2", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter3+="t4.nama like @" + i + "keyword2 OR ";
                                                                                }
                                                                                strFilter3 = strFilter3.substring(0,strFilter3.length-4);
                                                                            
                                                                                request.query("SELECT t1.*,t3.nama as divisi,t4.nama as dept FROM tbl_karyawan t1 left outer join tbl_user t2 on t1.id_karyawan=t2.id_karyawan left outer join tbl_divisi t3 on t2.id_divisi=t3.id_divisi left outer join tbl_departemen t4 on t2.id_departemen=t4.id_departemen  WHERE t1.na='n' and t1.email<>'' and (" + strFilter + ' ' + strFilter2 + ' ' + strFilter3 + ')', function (err, recordsetx) {
                                                                                    if (err) {
                                                                                        sql.close();
                                                                                        //consoleconsole.log(err);
                                                                                        writeErrorLog('Step 12 - SQL Execute',err);
                                                                                        return callback0z(err); 
                                                                                    }
                                                                                    else {
                                                                                        sql.close();
                                                                                        //console.log(JSON.stringify(recordsetx.recordsets[0]));
                                                                                        topics.HR_recipients.push(recordsetx.recordsets[0]);
                                                                                        var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                        dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                            if (err2) {
                                                                                                writeErrorLog('Step 13 - Role members',err2);
                                                                                                return callback0z(err2);
                                                                                            }
                                                                                            else {
                                                                                                async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                    topics.special_recipients.push({ 
                                                                                                        nik : obj3.nik,
                                                                                                        nama : obj3.nama,
                                                                                                        email : obj3.email,
                                                                                                        plants : obj3.plantselected,
                                                                                                        posisi : obj3.posisi,
                                                                                                        divisi : obj3.divisi,
                                                                                                        dept : obj3.dept
                                                                                                    });
                                                                                                    //send email
                                                                                                    return callback3();
                                                                                                }, function(err3){
                                                                                                    if (err3) {
                                                                                                        writeErrorLog('Step 14 - Summary',err3);
                                                                                                        return callback0z(err3);
                                                                                                    }
                                                                                                    else {
                                                                                                        var activateExclude = 1;
                                                                                                        if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                        if (activateExclude==0) {
                                                                                                            return callback0z();
                                                                                                        }
                                                                                                        else {
                                                                                                            var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                        //    console.log(JSON.stringify(obj2));
                                                                                                            dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                        //        console.log(JSON.stringify(rolemembers))
                                                                                                                if (err2) {
                                                                                                                    writeErrorLog('Step 11a',err2);
                                                                                                                    return callback0z(err2);
                                                                                                                }
                                                                                                                else {
                                                                                                                    async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                        topics.ex_recipients.push({ 
                                                                                                                            nik : obj3.nik,
                                                                                                                            nama : obj3.nama,
                                                                                                                            email : obj3.email,
                                                                                                                            plants : obj3.plantselected,
                                                                                                                            posisi : obj3.posisi
                                                                                                                        });
                                                                                                                        //send email
                                                                                                                        return callback3();
                                                                                                                    }, function(err3) {
                                                                                                                        if (err3) {
                                                                                                                            writeErrorLog('Step 11a',err3);
                                                                                                                            return callback0z(err3);
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            return callback0z();
                                                                                                                        }
                                                                                                                    });
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            return callback0z();
                                                        }
                                                    }, function(err2){
                                                        if (err2) {
                                                            writeErrorLog('Step 15 - Summary',err2);
                                                            return callback00(err2);
                                                        }
                                                        else {
                                                            jsonTopFinal.push(topics);
                                                            return callback00();
                                                        }
                                                    });
                                                }
                                                //});
                                            })
                                        }
                                    });
                                }, function(errG){
                                    if (errG) {
                                        db.close();
                                        writeErrorLog('Step 16 - Summary',errG);
                                        auth.setResponse(res,400,errG);
                                        return;
                                    }
                                    else {
                                        sendEmail(jsonTopFinal,true);
                                        return res.status(200).json(jsonTopFinal);
                                    }
                                });
                            }
                        }
                    );
                }
            });
        },

        //TODO:employee type 2
        emailForceRun2 : function(req,res) {
            var id = req.params.id;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    writeErrorLog('Step 1 - Connect mongodb',err);
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var jsonTopFinal = [];

                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics")
                        .aggregate([
                            { 
                                $match:
                                {
                                    _id : new ObjectId(id)
                                },
                            },
                            {
                                $project: 
                                {
                                    _id : true,
                                    topicname : true,
                                    topiccode : true,
                                    reports : true,
                                    emailroles : true,
                                    emailscheduleselected : true,
                                    emailsuccess : true,
                                    emaillastrun : true,
                                    forceRun : true,
                                    lasteighthours : {
                            
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 8
                                            ]        
                                    
                                    },
                                    lastfivehours : {
                                    
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 5
                                            ]        
                                    
                                    },
                                    lastfourhours : {
                                    
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 4
                                            ]        
                                    
                                    },
                                    lastrunday : {
                                    
                                                $divide: 
                                                    [    
                                                        { 
                                                            $subtract: [new Date(), '$emaillastrun'] 
                                                        },  1000 * 60 * 60 * 24
                                                    ]        
                                         
                                    },
                                    lastrunweek : {
                                       
                                                $divide: 
                                                    [
                                                        {$divide: 
                                                            [
                                                                { 
                                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                                },  1000 * 60 * 60 * 24
                                                            ]
                                                        }, 7
                                                    ]            
                                       
                                    },
                                    lastrunmonth : {
                                       
                                                $divide:
                                                [
                                                    {$divide: 
                                                        [
                                                            { 
                                                                $subtract: [new Date(), '$emaillastrun'] 
                                                            },  (1000 * 60 * 60 * 24)
                                                        ]
                                                    }, 30
                                                ]    
                                         
                                    },
                                },
                               
                            },
                            /*
                            {
                                $match:
                                { 
                                    $or : 
                                        [ 
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunday : { $gt : 0.5}, emailscheduleselected :0 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunweek : { $gt : 0.5}, emailscheduleselected :1 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunmonth : { $gt : 0.5}, emailscheduleselected :2 }
                                                    ]
                                            },
                                        ]
                                },
                            },
                            */
                            {   
                                
                                $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'reports.value',
                                    foreignField: '_id',
                                    as: 'reportdetails'
                                },
                               
                                    
                                   
                            },
                            {
                                $sort: {"reportdetails.reportcode": 1}
                            },
                            {
                                $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'emailroles.value',
                                    foreignField: '_id',
                                    as: 'roledetails'
                                },
                            },
                        ])
                        .toArray(function(errB,result) { 
                            if (errB) {
                                db.close();
                                console.log('halo 1');
                                writeErrorLog('Step 2 - to Array',errB);
                                auth.setResponse(res,400,errB);
                                return;
                            }
                            else {
                                async.eachSeries(result, function(objT, callback00) {
                                    var topics = {
                                        _id : new ObjectId(objT._id),
                                        topicname : objT.topicname,
                                        topiccode : objT.topiccode,
                                        reports : [],
                                        special_recipients : [],
                                        HR_recipients : [],
                                        ex_recipients : []
                                    }
                                    /*
                                        var myobj = { 
                                            topiccode : objT.topiccode,
                                            topicname : objT.topicname,
                                            emaillastrun : null,
                                            statusemail : null,
                                            emailforcerun : lastrun,
                                            statusemailforcerun : 'Success',
                                            msg1lastrun : null,
                                            statusmsg1 : null,
                                            msg1forcerun : null,
                                            statusmsg1forcerun : null,    
                                            lastmodified : lastrun,
                                        };
                                        dbo.collection("t_logs").insertOne(myobj, function(err, result) {
                                            if (err) {
                                                writeErrorLog('Step 2 - to Array',errB);
                                                return callback00(err);
                                            }
                                            else {
                                    */
                                    var myquery = { _id : new ObjectId(objT._id)};
                                    var forceRun = (objT.forceRun==null || objT.forceRun==undefined) ? []: objT.forceRun
                                    forceRun.push({value : new Date()});
                                    var newvalues = { $set: 
                                        {
                                            forceRun : forceRun
                                        } 
                                    };
                                    dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                                       if (err) {
                                        console.log('halo 3');
                                           writeErrorLog('Step 2b',err);
                                           return callback00(err);
                                        }
                                        else {
                                            async.eachSeries(objT.reportdetails, function(obj1,callback1){
                                                if (obj1.is_active==1) {
                                                    dbo.collection("t_servers").find({_id : new ObjectId(obj1.serveraddress)}).toArray(function(err0,servernames){
                                                        if (err0) {
                                                            writeErrorLog('Step 3 - t_servers',err0);
                                                            return callback1(err0);
                                                        }
                                                        else {
                                                            var config = null;
                                                            if (servernames[0].servertype==0) { 
                                                                config = {
                                                                    user: servernames[0].username,
                                                                    password: auth.decrypt(servernames[0].pwd),
                                                                    server: servernames[0].serveraddress, 
                                                                    database: servernames[0].dbname,
                                                                    port : 1433,
                                                                    requestTimeout : 360000,
                                                                    connectionTimeout: 15000
                                                                }
                                                            }
                                                            sql.connect(config, function (errx) {
                                                                if (errx) {
                                                                    sql.close();
                                                                    writeErrorLog('Step 3 - SQL Connect',errx);
                                                                    return callback1(errx);
                                                                }
                                                                else {
                                                                    var request = new sql.Request();
                                                                    request.execute(obj1.functionname, function (erry, recordset) {
                                                                        if (erry) {
                                                                            sql.close();
                                                                            writeErrorLog('Step 5 - ' + obj1.functionname, erry);
                                                                            return callback1(erry);
                                                                        }
                                                                        else {
                                                                            sql.close();
                                                                            var query2 = { id_report : obj1._id };
                                                                            dbo.collection("t_columns").find(query2).toArray(function(err1, colnames) {
                                                                                if (err1) {
                                                                                    writeErrorLog('Step 6 - Column Execute',err1);
                                                                                    return callback1(err1);
                                                                                }
                                                                                else {
                                                                                    topics.reports.push({
                                                                                        reportname : obj1.reportname,
                                                                                        reportcode : obj1.reportcode,
                                                                                        reportheader : obj1.reportheader,
                                                                                        reportfooter : obj1.reportfooter,
                                                                                        requestedby : obj1.requestedby,
                                                                                        plantcolumn : obj1.plantcolumn,
                                                                                        isfooter : obj1.isfooter,
                                                                                        columnnames : colnames,
                                                                                        isperorganization : (obj1.isperorganization!=null && obj1.isperorganization!=undefined) ? obj1.isperorganization : 0,
                                                                                        isplantvisible : (obj1.isplantvisible!=null && obj1.isplantvisible!=undefined) ? obj1.isplantvisible : 0,
                                                                                        rows : recordset.recordsets[0]
                                                                                    });
                                                                                    return callback1();
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                else {
                                                    return callback1();
                                                }
                                            }, function(err0){
                                                if (err0) {
                                                    writeErrorLog('Step 7 - Summary',err0);
                                                    return callback00(err0);
                                                }
                                                else {
                                                    /*
                                                        var config2 = null;
                                                        config2 = {
                                                        user: 'report',
                                                        password: 'KiranaMegatara21',//auth.decrypt(servernames2[0].pwd),
                                                        server: '10.0.0.32', 
                                                        database: 'ReportTemplate',
                                                        port : 1433,
                                                        requestTimeout : 360000,
                                                        connectionTimeout: 15000 

                                                        }
                                                        sql.connect(config2, function (errx) {
                                                            if (errx) {
                                                                sql.close();
                                                                writeErrorLog('Step 9 - SQL Execute',errx);
                                                                return callback00(errx); 
                                                            }
                                                            else {
                                                    */
                                                    async.eachSeries(objT.roledetails, function(obj2,callback0z){
                                                        if (obj2.is_active==1) {
                                                            dbo.collection("t_servers").find({_id : new ObjectId(obj2.dbserver)}).toArray(function(err4,servernames2){
                                                                if (err4) {
                                                                    writeErrorLog('Step 8 - t_servers',err4);
                                                                    return callback0z(err4);
                                                                }
                                                                else {
                                                                    var config = null;
                                                                    if (servernames2[0].servertype==0) { 
                                                                        config = {
                                                                            user: servernames2[0].username,
                                                                            password: auth.decrypt(servernames2[0].pwd),
                                                                            server: servernames2[0].serveraddress, 
                                                                            database: servernames2[0].dbname,
                                                                            port : 1433,
                                                                            requestTimeout : 360000,
                                                                            connectionTimeout: 15000
                                                                        }
                                                                    }
                                                                    sql.connect(config, function (errx) {
                                                                        if (errx) {
                                                                            sql.close();
                                                                            writeErrorLog('Step 3 - SQL Connect',errx);
                                                                            return callback0z(errx);
                                                                        }
                                                                        else {
                                                                            var keyword = obj2.keyword;
                                                                            if (keyword=='') {
                                                                                sql.close();
                                                                                jsonEmp = [];
                                                                                var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                    if (err2) {
                                                                                        writeErrorLog('Step 10 - Role members',err2);
                                                                                        return callback0z(err2);
                                                                                    }
                                                                                    else {
                                                                                        async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                            topics.special_recipients.push({ 
                                                                                                nik : obj3.nik,
                                                                                                nama : obj3.nama,
                                                                                                email : obj3.email,
                                                                                                divisi : obj3.divisi,
                                                                                            });
                                                                                            //send email
                                                                                            return callback3();
                                                                                        }, function(err3){
                                                                                            if (err3) {
                                                                                                writeErrorLog('Step 11 - Summary',err3);
                                                                                                return callback0z(err3);
                                                                                            }
                                                                                            else {
                                                                                                var activateExclude = 1;
                                                                                                if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                if (activateExclude==0) {
                                                                                                    return callback0z();
                                                                                                }
                                                                                                else {
                                                                                                    var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                // console.log(JSON.stringify(obj2));
                                                                                                    dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                    //    console.log(JSON.stringify(rolemembers))
                                                                                                        if (err2) {
                                                                                                            writeErrorLog('Step 11a',err2);
                                                                                                            return callback0z(err2);
                                                                                                        }
                                                                                                        else {
                                                                                                            async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                topics.ex_recipients.push({ 
                                                                                                                    nik : obj3.nik,
                                                                                                                    nama : obj3.nama,
                                                                                                                    email : obj3.email,
                                                                                                                    //plants : obj3.plantselected,
                                                                                                                    posisi : obj3.posisi
                                                                                                                });
                                                                                                                //send email
                                                                                                                return callback3();
                                                                                                            }, function(err3) {
                                                                                                                if (err3) {
                                                                                                                    writeErrorLog('Step 11a',err3);
                                                                                                                    return callback0z(err3);
                                                                                                                }
                                                                                                                else {
                                                                                                                    return callback0z();
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                var request = new sql.Request();
                                                                                var arrKeyword = keyword.split(';');
                                                                                var strFilter = '';
                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter+="t1.divisi like @" + i + "keyword OR ";
                                                                                }
                                                                                strFilter = strFilter.substring(0,strFilter.length-4);
                                                                            
                                                                                request.query("SELECT t1.* FROM tbl_ass_user t1  WHERE t1.na='n' and t1.email<>'' and t1.del='n' and (" + strFilter + ')', function (err, recordsetx) {
                                                                                    if (err) {
                                                                                        sql.close();
                                                                                        //consoleconsole.log(err);
                                                                                        writeErrorLog('Step 12 - SQL Execute',err);
                                                                                        return callback0z(err); 
                                                                                    }
                                                                                    else {
                                                                                        sql.close();
                                                                                        //console.log(JSON.stringify(recordsetx.recordsets[0]));
                                                                                        topics.HR_recipients.push(recordsetx.recordsets[0]);
                                                                                        var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                        dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                            if (err2) {
                                                                                                writeErrorLog('Step 13 - Role members',err2);
                                                                                                return callback0z(err2);
                                                                                            }
                                                                                            else {
                                                                                                async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                    topics.special_recipients.push({ 
                                                                                                        nik : obj3.nik,
                                                                                                        nama : obj3.nama,
                                                                                                        email : obj3.email,
                                                                                                        divisi : obj3.divisi,
                                                                                                    });
                                                                                                    //send email
                                                                                                    return callback3();
                                                                                                }, function(err3){
                                                                                                    if (err3) {
                                                                                                        writeErrorLog('Step 14 - Summary',err3);
                                                                                                        return callback0z(err3);
                                                                                                    }
                                                                                                    else {
                                                                                                        var activateExclude = 1;
                                                                                                        if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                        if (activateExclude==0) {
                                                                                                            return callback0z();
                                                                                                        }
                                                                                                        else {
                                                                                                            var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                        //    console.log(JSON.stringify(obj2));
                                                                                                            dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                        //        console.log(JSON.stringify(rolemembers))
                                                                                                                if (err2) {
                                                                                                                    writeErrorLog('Step 11a',err2);
                                                                                                                    return callback0z(err2);
                                                                                                                }
                                                                                                                else {
                                                                                                                    async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                        topics.ex_recipients.push({ 
                                                                                                                            nik : obj3.nik,
                                                                                                                            nama : obj3.nama,
                                                                                                                            email : obj3.email,
                                                                                                                            divisi : obj3.divisi
                                                                                                                        });
                                                                                                                        //send email
                                                                                                                        return callback3();
                                                                                                                    }, function(err3) {
                                                                                                                        if (err3) {
                                                                                                                            writeErrorLog('Step 11a',err3);
                                                                                                                            return callback0z(err3);
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            return callback0z();
                                                                                                                        }
                                                                                                                    });
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            return callback0z();
                                                        }
                                                    }, function(err2){
                                                        if (err2) {
                                                            writeErrorLog('Step 15 - Summary',err2);
                                                            return callback00(err2);
                                                        }
                                                        else {
                                                            jsonTopFinal.push(topics);
                                                            return callback00();
                                                        }
                                                    });
                                                }
                                                //});
                                            })
                                        }
                                    });
                                }, function(errG){
                                    if (errG) {
                                        db.close();
                                        writeErrorLog('Step 16 - Summary',errG);
                                        auth.setResponse(res,400,errG);
                                        return;
                                    }
                                    else {
                                        sendEmail2(jsonTopFinal,true);
                                        return res.status(200).json(jsonTopFinal);
                                    }
                                });
                            }
                        }
                    );
                }
            });
        },

        getLogs : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_logs").find(query).sort({emaillastrun : -1}).limit(1000).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

    //APP
        createApp : function(req,res) {
            const { appname, appdesc, appcode, is_active } = req.body; 
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { appname : appname };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_apps").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'App the same name already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    appname : appname,
                                    appdesc : appdesc,
                                    appcode : appcode,
                                    lastmodified : new Date(),
                                    is_active : is_active
                                };
                                dbo.collection("t_apps").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);  
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        getApps : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_apps").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getAppByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_apps").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        editApp : function(req,res) {
            var id = req.params.id;
            const { appname, appdesc, appcode, is_active } = req.body; 
            
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            appname : appname,
                            appdesc : appdesc,
                            appcode : appcode,
                            lastmodified : new Date(),
                            is_active : is_active
                        } 
                    };
                    dbo.collection("t_apps").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },
    //

    //new
    /*
       
        */

    //APP MENU
       getMenusByApp : function(req,res) {
            const { id } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id) }
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_apps").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            sql.connect(configDb, function (err) {
                                if (err) {
                                    sql.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
				    db.close();
                                    var request = new sql.Request();
console.log(result[0].appcode);
                                    request.input('report_appid', sql.Int, parseInt(result[0].appcode));
                                    request.query("SELECT * FROM ReportTemplate.rpt.M_Report where report_appid=@report_appid", function (err, recordset) {
                                        if (err) {
console.log(err);

                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            sql.close();
                                            res.status(200).json(recordset.recordsets[0]);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },
        createMenu : function(req,res) {
            const {
                report_appid,
                report_id,
                report_name,
                report_category,
                report_link,
                report_proc,
                report_status,
                report_accessstatus
            } = req.body;
            sql.connect(configDb, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    var sqlQ = '';
                    request.input('report_appid', sql.Int, report_appid);
                    request.input('report_id', sql.Int, report_id);
                    request.input('report_name', sql.VarChar, report_name);
                    request.input('report_category', sql.Int, report_category);
                    request.input('report_link', sql.VarChar, report_link);
                    request.input('report_proc', sql.VarChar, report_proc);
                    request.input('report_status', sql.Char, report_status);
                    request.input('report_accessstatus', sql.Char, report_accessstatus);
                    sqlQ = 'insert into M_Reports (report_appid,report_id,report_name,report_category,report_link,report_proc,report_status,report_accessstatus,report_createdate,report_createid) values (@report_appid,@report_id,@report_name,@report_category,@report_link,@report_proc,@report_status,@report_accessstatus,getdate(),1)';
                    request.query(sqlQ, function (erry, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            sql.close();
                            return res.status(200).json({message : 'ok'})
                        }
                    });
                }
            });
        },

        editMenu : function(req,res) {
            const {
                report_appid,
                report_id
            } = req.params;
            const {
                report_name,
                report_category,
                report_link,
                report_proc,
                report_status,
                report_accessstatus
            } = req.body;
            sql.connect(configDb, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    var sqlQ = '';
                    request.input('report_appid', sql.Int, report_appid);
                    request.input('report_id', sql.Int, report_id);
                    request.input('report_name', sql.VarChar, report_name);
                    request.input('report_category', sql.Int, report_category);
                    request.input('report_link', sql.VarChar, report_link);
                    request.input('report_proc', sql.VarChar, report_proc);
                    request.input('report_status', sql.Char, report_status);
                    request.input('report_accessstatus', sql.Char, report_accessstatus);
                    sqlQ = 'update M_Reports set report_name=@report_name,report_category=@report_category,report_link=@report_link,report_proc=@report_proc,report_status=@report_status,report_accessstatus=@report_accessstatus where report_appid=@report_appid and report_id=@report_id';
                    request.query(sqlQ, function (erry, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            sql.close();
                            return res.status(200).json({message : 'ok'})
                        }
                    });
                }
            });
        },

        getMenuByID : function(req,res) {
            const { report_id,report_appid } = req.params;
            sql.connect(configDb, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.input('report_id', sql.VarChar, report_id);
                    request.input('report_appid', sql.VarChar, report_appid);
                    request.query("SELECT * FROM ReportTemplate.rpt.M_Reports where report_did=@report_id and report_appid=@report_appid", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            sql.close();
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });
        },

    
    //SET AND UNSET ROLE TO APP MENU
        setRoleMenus : function(req,res) {
            const {
                id,
                role_id,
                report_appid,
                menu_ids
            } = req.body;
            var menus = [];
          
            for(i=0;i<menu_ids.length;i++) {
                menus.push({value : menu_ids[i]});
            }
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    if (id!=null) {
                        var myquery = { _id: new ObjectId(id) };
                        var newvalues = { $set: 
                            {
                                roleid : new ObjectId(role_id),
                                report_appid : new ObjectId(report_appid),
                                menus : menus,
                                lastmodified : new Date(),
                            } 
                        };
                        dbo.collection("t_role_menus").updateOne(myquery, newvalues, function(err, result2) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json('success');
                            }
                        });
                    }
                    else {
                        var myobj = { 
                            roleid : new ObjectId(role_id),
                            report_appid : new ObjectId(report_appid),
                            menus : menus,
                            lastmodified : new Date(),
                        };
                        dbo.collection("t_role_menus").insertOne(myobj, function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json({id : result.insertedId});
                            }
                        });
                    }
                    
                }
            });
        },

    //GET APP MENU BY ROLES INCLUDING LOGIN
        loginApp : function(req,res) {
            const {
                nik,
                appid,
                pwd
            } = req.body;
            var rolemembers = [];
            var excluderolemembers = [];
            var roles = [];
            var rolesfinal = [];

            var posst = '';
            var div = '';
            var dept = '';
            sql.connect(configDb, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    var sqlQ = '';
                    request.input('nik', sql.VarChar, nik);
                    request.input('pwd', sql.VarChar, md5(pwd));
                
                    sqlQ = "select t1.*,t2.posst,t3.nama divisi,t4.nama dept from portal.dbo.tbl_user t1 left join portal.dbo.tbl_karyawan t2 on t1.id_karyawan=t2.id_karyawan left outer join tbl_divisi t3 on t2.id_divisi=t3.id_divisi left outer join tbl_departemen t4 on t2.id_departemen=t4.id_departemen where t1.nik=@nik and t1.pass=@pwd and t1.na='n' and t1.del='n'";
                    request.query(sqlQ, function (erry, recordset) {
                        if (erry) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            posst = recordset.recordsets[0][0].posst;
                            div = recordset.recordsets[0][0].div;
                            dept = recordset.recordsets[0][0].dept;
                            sql.close();
                            var MongoClient = mongo.MongoClient;
                            MongoClient.connect(url.getDbString(), function(err, db) {
                                if (err) {
                                    auth.setResponse(res,400,err);
                                    db.close();
                                    return;
                                }
                                else {
                                    var query = { nik : nik};
                                    var dbo = db.db(dbname.getDbName());
                                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                                        if (err) {
                                            db.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            async.eachSeries(result, function(obj,callback){
                                                rolemembers.push({value : obj.roleid});
                                                return callback();
                                            },function(err) {
                                                if (err) {
                                                    db.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var query = { nik : nik};
                                                    dbo.collection("t_exclude_role_members").find(query).toArray(function(err, result) {
                                                        if (err) {
                                                            db.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            async.eachSeries(result, function(obj,callback){
                                                                excluderolemembers.push({value : obj.roleid});
                                                                return callback();
                                                            },function(err) {
                                                                if (err) {
                                                                    db.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    var query = { $or: [{keyword: {$regex: posst,$options:'i'}},{keyword: {$regex: div,$options:'i'}},{keyword: {$regex: dept,$options:'i'}} ]};
                                                                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                                                                        if (err) {
                                                                            db.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            async.eachSeries(result, function(obj,callback){
                                                                                var arrKeyword = obj.keyword.split(';');
                                                                                var strFilter = '';
                                                                                var strFilter2 = '';
                                                                                var strFilter3 = '';
                                                                                var request = new sql.Request();
                                                                                request.input('nik', sql.VarChar, nik);
                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter+="t1.posst like @" + i + "keyword OR ";
                                                                                }
                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword1", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter2+="t3.nama like @" + i + "keyword1 OR ";
                                                                                }
                                                                                for(var i=0;i<arrKeyword.length;i++) {
                                                                                    request.input(i+"keyword2", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                    strFilter3+="t4.nama like @" + i + "keyword2 OR ";
                                                                                }
                                                                                strFilter3 = strFilter3.substring(0,strFilter3.length-4);
                                                                                
                                                                                request.query("SELECT t1.*,t3.nama as divisi,t4.nama as dept FROM tbl_karyawan t1 left outer join tbl_user t2 on t1.id_karyawan=t2.id_karyawan left outer join tbl_divisi t3 on t2.id_divisi=t3.id_divisi left outer join tbl_departemen t4 on t2.id_departemen=t4.id_departemen WHERE t1.nik=@nik and t1.na='n' and t1.email<>'' and (" + strFilter + ' ' + strFilter2 + ' ' + strFilter3 + ')', function (err, recordset) {
                                                                                    if (err) {
                                                                                        return callback(err);
                                                                                    }
                                                                                    else {
                                                                                        if (recordset.recordsets[0].length>0) roles.push({value:obj._id})
                                                                                        return callback();
                                                                                    }
                                                                                });
                                                                            },function(err) {
                                                                                if (err) {
                                                                                    db.close();
                                                                                    auth.setResponse(res,400,err);
                                                                                    return;
                                                                                }
                                                                                else {
                                                                                    async.eachSeries(rolemembers, function(obj,callback){
                                                                                        rolesfinal.push({value:obj.value});
                                                                                        return callback();
                                                                                    },function(err){
                                                                                        async.eachSeries(roles, function(obj,callback){
                                                                                            rolesfinal.push({value:obj.value});
                                                                                            return callback();
                                                                                        },function(err) {
                                                                                            rolesfinal = removeDuplicates(rolesfinal);
                                                                                            async.eachSeries(rolesfinal, function(obj,callback){
                                                                                                var query = { $and: [{roleid: obj.value}, {report_appid : new ObjectId(appid)}]};
                                                                                                dbo.collection("t_role_menus").find(query).toArray(function(err, result) {
                                                                                                    if (err) {
                                                                                                       return callback(err);
                                                                                                    }
                                                                                                    else {
                                                                                                        async.eachSeries(result[0].menus, function(obj,callback2){
                                                                                                            menus.push({value :obj.value});
                                                                                                            return callback2();
                                                                                                        },function(err2){
                                                                                                            if (err2) {
                                                                                                                return callback(err2);
                                                                                                            }
                                                                                                            else {
                                                                                                                return callback();
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                });
                                                                                            },function(err){
                                                                                                if (err) {
                                                                                                    db.close();
                                                                                                    auth.setResponse(res,400,err);
                                                                                                    return;
                                                                                                }
                                                                                                else {
                                                                                                    db.close();
                                                                                                    menus = removeDuplicates(menus);
                                                                                                    res.status(200).json(menus);
                                                                                                }
                                                                                            });
                                                                                                
                                                                                        });
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

    //DAFTAR PABRIK
        getPlants : function(req,res) {
            sql.connect(configDb, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.query("SELECT * FROM ReportTemplate.rpt.vw_PlantRegion", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            sql.close();
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });
            
        },
    //MAIL SERVER
        createMailServer : function(req,res) {
            var servername = req.body.servername;
            var serveraddress = req.body.serveraddress;
            var username = req.body.username;
            var pwd = null;
            if (req.body.pwd!=='')
                pwd = auth.encrypt(req.body.pwd);
            else
                pwd = ''
            var token = null;
            if (req.body.token!=='')
                token = auth.encrypt(req.body.token);
            else
                token = ''
            var is_active = req.body.is_active;
            var port = req.body.port;
            var noreply = req.body.noreply;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { servername: servername };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Mail server with the same name already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    servername : servername,
                                    username : username,
                                    pwd : pwd,
                                    port : port,
                                    noreply : noreply,
                                    serveraddress : serveraddress,
                                    token : token,
                                    lastmodified : new Date(),
                                    is_active : is_active
                                };
                                dbo.collection("t_mail_servers").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editMailServer : function(req,res) {
            var id = req.params.id;
            var servername = req.body.servername;
            var serveraddress = req.body.serveraddress;
            var username = req.body.username;
            var pwd = null;
            if (req.body.pwd!=='')
                pwd = auth.encrypt(req.body.pwd);
            else
                pwd = ''
            var token = null;
            if (req.body.token!=='')
                token = auth.encrypt(req.body.token);
            else
                token = ''
            var is_active = req.body.is_active;
            var port = req.body.port;
            var noreply = req.body.noreply;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            servername : servername,
                            username : username,
                            pwd : pwd,
                            port : port,
                            noreply : noreply,
                            token : token,
                            serveraddress : serveraddress,
                            lastmodified : new Date(),
                            is_active : is_active
                        } 
                    };
                    dbo.collection("t_mail_servers").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        getMailServers : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                   
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getMailServerByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

    //DATABASE SERVER
        createServer : function(req,res) {
            var servername = req.body.servername;
            var serveraddress = req.body.serveraddress;
            var dbname2 = req.body.dbname;
            var servertype = req.body.servertype;
            var username = req.body.username;
            var pwd = auth.encrypt(req.body.pwd);
            var is_active = req.body.is_active;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { servername : servername };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
  
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Report with the same code already exists');

                                return;
                            }
                            else {
                                var myobj = { 
                                    servername : servername,
                                    servertype : servertype,
                                    dbname : dbname2,
                                    username : username,
                                    pwd : pwd,
                                    serveraddress : serveraddress,
                                    lastmodified : new Date(),
                                    is_active : is_active
                                };
                                dbo.collection("t_servers").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);  
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editServer : function(req,res) {
            var id = req.params.id;
            var servername = req.body.servername;
            var dbname2 = req.body.dbname;
            var serveraddress = req.body.serveraddress;
            var servertype = req.body.servertype;
            var username = req.body.username;
            var pwd = req.body.pwd;
            var is_active = req.body.is_active;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            servername : servername,
                            servertype : servertype,
                            dbname : dbname2,
                            username : username,
                            pwd : pwd,
                            serveraddress : serveraddress,
                            lastmodified : new Date(),
                            is_active : is_active
                        } 
                    };
                    dbo.collection("t_servers").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        getServers : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);

                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getServerByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);

                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {

                            db.close();
                            return res.status(200).json(result);
                        
                        }
                    });
                }
            });
        },

    //ROLES
        createRole : function(req,res) {
            var rolename = req.body.rolename;
            var keyword = req.body.keyword;
            var is_active = req.body.is_active;
            var dbserver = req.body.dbserver;
            var activateExclude = req.body.activateExclude;
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);

                    return;
                }
                else {
                    var query = { rolename: rolename };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);

                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Role with the same name already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    rolename: rolename, 
                                    keyword: keyword,
                                    is_active : is_active,
                                    lastmodified : new Date(),
                                    dbserver : new ObjectId(dbserver),
                                    activateExclude : activateExclude
                                };
                                dbo.collection("t_roles").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
 
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editRole : function(req,res) {
            var id = req.params.id;
            var rolename = req.body.rolename;
            var keyword = req.body.keyword;
            var is_active = req.body.is_active;
            var dbserver = req.body.dbserver;
            var activateExclude = req.body.activateExclude;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: {
                        rolename: rolename, 
                        keyword: keyword,
                        is_active : is_active,
                        lastmodified : new Date(),
                        dbserver : new ObjectId(dbserver),
                        activateExclude : activateExclude
                    } };
                    dbo.collection("t_roles").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        deleteRole : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRoles : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).sort({rolename : 1}).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRoleByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                   
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getEmployeesByRole : async function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var keyword = result[0].keyword;
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    //if (result[0].servertype==0) {
                                        var username = result[0].username;
                                        var pwd = auth.decrypt(result[0].pwd);
                                        var dbname = result[0].dbname;
                                        var serveraddress = result[0].serveraddress;
                                        var config = {
                                            user: username,
                                            password: pwd,
                                            server: serveraddress, 
                                            database: dbname 
                                        };
                                        sql.connect(config, function (err) {
                                            if (err) {
                                                sql.close();
                                                auth.setResponse(res,400,err);
                                                return;
                                            }
                                            else {
                                                var request = new sql.Request();
                                                if (keyword=='') {
                                                    sql.close();
                                                    res.status(200).json([]);
                                                }
                                                else {
                                                    var arrKeyword = keyword.split(';');
                                                    var strFilter = '';
                                                    var strFilter2 = '';
                                                    var strFilter3 = '';
                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                        request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                        strFilter+="t1.posst like @" + i + "keyword OR ";
                                                    }
                                                    //strFilter = strFilter.substring(0,strFilter.length-4);
                                                    //console.log(strFilter)
                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                        request.input(i+"keyword1", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                        strFilter2+="t3.nama like @" + i + "keyword1 OR ";
                                                    }
                                                    //strFilter2 = strFilter2.substring(0,strFilter2.length-4);
                                                    //console.log(strFilter2)
                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                        request.input(i+"keyword2", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                        strFilter3+="t4.nama like @" + i + "keyword2 OR ";
                                                    }
                                                    strFilter3 = strFilter3.substring(0,strFilter3.length-4);
                                                    //console.log(strFilter3)
                                                    //var sql = 
                                                    request.query("SELECT t1.*,t3.nama as divisi,t4.nama as dept FROM tbl_karyawan t1 left outer join tbl_user t2 on t1.id_karyawan=t2.id_karyawan left outer join tbl_divisi t3 on t2.id_divisi=t3.id_divisi left outer join tbl_departemen t4 on t2.id_departemen=t4.id_departemen  WHERE t1.na='n' and t1.email<>'' and (" + strFilter + ' ' + strFilter2 + ' ' + strFilter3 + ')', function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            sql.close();
                                                            res.status(200).json(recordset.recordsets[0]);
                                                        }
                                                    });
                                                }
                                            }
                                            
                                        });
                                    //}
                                    //else {

                                    //}
                                }
                            });
                        }
                    });
                }
            });
            
        },

        //TODO:DONE employee type 2
        getEmployeesByRole2 : async function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var keyword = result[0].keyword;
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    //if (result[0].servertype==0) {
                                        var username = result[0].username;
                                        var pwd = auth.decrypt(result[0].pwd);
                                        var dbname = result[0].dbname;
                                        var serveraddress = result[0].serveraddress;
                                        var config = {
                                            user: username,
                                            password: pwd,
                                            server: serveraddress, 
                                            database: dbname 
                                        };
                                        sql.connect(config, function (err) {
                                            if (err) {
                                                sql.close();
                                                auth.setResponse(res,400,err);
                                                return;
                                            }
                                            else {
                                                var request = new sql.Request();
                                                if (keyword=='') {
                                                    sql.close();
                                                    res.status(200).json([]);
                                                }
                                                else {
                                                    var arrKeyword = keyword.split(';');
                                                    var strFilter = '';
                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                        request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                        strFilter+="t1.divisi like @" + i + "keyword OR ";
                                                    }
                                                    strFilter = strFilter.substring(0,strFilter.length-4);
                                                    request.query("SELECT t1.* FROM tbl_ass_user t1 WHERE t1.na='n' and t1.del='n' and t1.email<>'' and (" + strFilter + ')', function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            sql.close();
                                                            res.status(200).json(recordset.recordsets[0]);
                                                        }
                                                    });
                                                }
                                            }
                                            
                                        });
                                    //}
                                    //else {

                                    //}
                                }
                            });
                        }
                    });
                }
            });
            
        },

    //COLUMNS
        createColumn : function(req,res) {
            var id_report = req.body.id_report;
            var columnname = req.body.columnname;
            var displayname = req.body.displayname;
            var textalignment = req.body.textalignment;
            var formatcolumn = req.body.formatcolumn;
            var columnhighlight = req.body.columnhighlight;
            var columnconditional = req.body.columnconditional;
            var columntarget = req.body.columntarget;
            var columnactual = req.body.columnactual;
            var columntarget2 = req.body.columntarget2;
            var columnactual2 = req.body.columnactual2;
            var columnoperand1 = req.body.columnoperand1;
            var columnoperand2 = req.body.columnoperand2;
            var columncoloractual = req.body.columncoloractual;
            var columncoloractual2 = req.body.columncoloractual2;
            var columntarget3 = req.body.columntarget3;
            var columnactual3 = req.body.columnactual3;
            var columnoperand3 = req.body.columnoperand3;
            var columncoloractual3 = req.body.columncoloractual3;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { id_report : new ObjectId(id_report), columnname : columnname };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Data already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    id_report : new ObjectId(id_report),
                                    columnname : columnname,
                                    displayname : displayname,
                                    textalignment : textalignment,
                                    formatcolumn : formatcolumn,
                                    columnhighlight : columnhighlight,    
                                    columnconditional : columnconditional,
                                    columntarget : columntarget,
                                    columnactual : columnactual,
                                    columntarget2 : columntarget2,
                                    columnactual2 : columnactual2,
                                    columnoperand1 : columnoperand1,
                                    columnoperand2 : columnoperand2,
                                    lastmodified : new Date(),
                                    columncoloractual : columncoloractual,
                                    columncoloractual2 : columncoloractual2,
                                    columntarget3 : columntarget3,
                                    columnactual3 : columnactual3,
                                    columnoperand3 : columnoperand3,
                                    columncoloractual3 : columncoloractual3
                                };
                                dbo.collection("t_columns").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editColumn : function(req,res) {
            var id = req.params.id;
            var id_report = req.body.id_report;
            var columnname = req.body.columnname;
            var displayname = req.body.displayname;
            var textalignment = req.body.textalignment;
            var formatcolumn = req.body.formatcolumn;
            var columnhighlight = req.body.columnhighlight;
            var columnconditional = req.body.columnconditional;
            var columntarget = req.body.columntarget;
            var columnactual = req.body.columnactual;
            var columntarget2 = req.body.columntarget2;
            var columnactual2 = req.body.columnactual2;
            var columnoperand1 = req.body.columnoperand1;
            var columnoperand2 = req.body.columnoperand2;
            var columncoloractual = req.body.columncoloractual;
            var columncoloractual2 = req.body.columncoloractual2;
            var columntarget3 = req.body.columntarget3;
            var columnactual3 = req.body.columnactual3;
            var columnoperand3 = req.body.columnoperand3;
            var columncoloractual3 = req.body.columncoloractual3;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {

                                var myobj = { $set: {
                                    id_report : new ObjectId(id_report),
                                    columnname : columnname,
                                    displayname : displayname,
                                    textalignment : textalignment,
                                    formatcolumn : formatcolumn,
                                    columnhighlight : columnhighlight,    
                                    columnconditional : columnconditional,
                                    columntarget : columntarget,
                                    columnactual : columnactual,
                                    columntarget2 : columntarget2,
                                    columnactual2 : columnactual2,
                                    columnoperand1 : columnoperand1,
                                    columnoperand2 : columnoperand2,
                                    lastmodified : new Date(),
                                    columncoloractual : columncoloractual,
                                    columncoloractual2 : columncoloractual2,
                                    columntarget3 : columntarget3,
                                    columnactual3 : columnactual3,
                                    columnoperand3 : columnoperand3,
                                    columncoloractual3 : columncoloractual3
                                }
                                };
                                //console.log(JSON.stringify(myobj));
                                //console.log(id);
                                dbo.collection("t_columns").updateOne({_id : new ObjectId(id)}, myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        //console.log(err);
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json('success');
                                    }
                                });
                            
                        }
                    });
                }
            });
        },

        deleteColumn : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getColumns : function(req,res) {
            var id_report = req.params.id_report;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    query = { id_report : new ObjectId(id_report) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getColumnByID : function(req,res) {
            var id = req.params.id;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    query = { _id : new ObjectId(id) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },


    //ROLE MEMBER
        createRoleMember : function(req,res) {
            var roleid = req.body.roleid;
            var id_karyawan = req.body.id_karyawan;
            var nik = req.body.nik;
            var pabrik = req.body.pabrik;
            var nama = req.body.nama;
            var posisi = req.body.posisi;
            var ho = req.body.ho;
            var email = req.body.email;
            var plantselected = req.body.plantselected;
            var divisi = req.body.divisi;
            var dept = req.body.dept;
        
            var pl = [];
          
            for(i=0;i<plantselected.length;i++) {
                pl.push({value : plantselected[i]});
            }
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { id_karyawan : id_karyawan, roleid : new ObjectId(roleid) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Role member with the same ID already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    roleid : new ObjectId(roleid),
                                    id_karyawan : id_karyawan,
                                    nik : nik,
                                    gsber : pabrik,
                                    nama : nama,
                                    posisi : posisi,
                                    ho : ho,
                                    email : email,
                                    lastmodified : new Date(),
                                    plantselected : pl,
                                    divisi : divisi,
                                    dept : dept
                                };
                                dbo.collection("t_role_members").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        deleteRoleMember : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRoleMembers : function(req,res) {
            var roleid = req.params.roleid;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    query = { roleid : new ObjectId(roleid) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        createExRoleMember : function(req,res) {
            var roleid = req.body.roleid;
            var id_karyawan = req.body.id_karyawan;
            var nik = req.body.nik;
            var pabrik = req.body.pabrik;
            var nama = req.body.nama;
            var posisi = req.body.posisi;
            var ho = req.body.ho;
            var email = req.body.email;
            //var plantselected = req.body.plantselected;
        
            //var pl = [];
          
            //for(i=0;i<plantselected.length;i++) {
            //    pl.push({value : plantselected[i]});
            //}
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { id_karyawan : id_karyawan, roleid : new ObjectId(roleid) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_exclude_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Role member with the same ID already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    roleid : new ObjectId(roleid),
                                    id_karyawan : id_karyawan,
                                    nik : nik,
                                    gsber : pabrik,
                                    nama : nama,
                                    posisi : posisi,
                                    ho : ho,
                                    email : email,
                                    lastmodified : new Date(),
                                };
                                dbo.collection("t_exclude_role_members").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        deleteExRoleMember : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_exclude_role_members").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getExRoleMembers : function(req,res) {
            var roleid = req.params.roleid;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    query = { roleid : new ObjectId(roleid) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_exclude_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRolePlants : function(req,res) {
            var id = req.params.id;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = { _id : new ObjectId(id) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getEmployees :  function(req,res) {
            var roleid = req.params.roleid;
            
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(roleid)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    var username = result[0].username;
                                    var pwd = auth.decrypt(result[0].pwd);
                                    var dbname = result[0].dbname;
                                    var serveraddress = result[0].serveraddress;
                                    var config = {
                                        user: username,
                                        password: pwd,
                                        server: serveraddress, 
                                        database: dbname 
                                    };
                                    //console.log(JSON.stringify(config))
                                    sql.connect(config, function (err) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.query("SELECT * FROM tbl_karyawan where na='n' order by nama", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    sql.close();
                                                    res.status(200).json(recordset.recordsets[0]);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        //TODO:DONE employee type 2
        getEmployees2 :  function(req,res) {
            var roleid = req.params.roleid;
            
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(roleid)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    var username = result[0].username;
                                    var pwd = auth.decrypt(result[0].pwd);
                                    var dbname = result[0].dbname;
                                    var serveraddress = result[0].serveraddress;
                                    var config = {
                                        user: username,
                                        password: pwd,
                                        server: serveraddress, 
                                        database: dbname 
                                    };
                                    //console.log(JSON.stringify(config))
                                    sql.connect(config, function (err) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.query("SELECT * FROM tbl_ass_user where na='n' and del='n' order by nama", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    sql.close();
                                                    res.status(200).json(recordset.recordsets[0]);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getEmployeeByID : function(req,res) {
            var roleid = req.body.roleid;
            
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(roleid)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    var username = result[0].username;
                                    var pwd = auth.decrypt(result[0].pwd);
                                    var dbname = result[0].dbname;
                                    var serveraddress = result[0].serveraddress;
                                    var config = {
                                        user: username,
                                        password: pwd,
                                        server: serveraddress, 
                                        database: dbname 
                                    };
                                    sql.connect(config, function (err) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('nik', sql.VarChar, req.body.id);
                                            request.query("SELECT t1.*,t3.nama divisi,t4.nama dept FROM tbl_karyawan t1 left outer join tbl_user t2 on t1.id_karyawan=t2.id_karyawan left outer join tbl_divisi t3 on t2.id_divisi=t3.id_divisi left outer join tbl_departemen t4 on t2.id_departemen=t4.id_departemen where nik=@nik", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    sql.close();
console.log('halo  ' +JSON.stringify(recordset.recordsets[0]));
                                                    res.status(200).json(recordset.recordsets[0]);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        //TODO:DONE employee type 2
        getEmployeeByIDx : function(req,res) {
            var roleid = req.body.roleid;
            console.log('body ' + JSON.stringify(req.body));
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(roleid)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    var username = result[0].username;
                                    var pwd = auth.decrypt(result[0].pwd);
                                    var dbname = result[0].dbname;
                                    var serveraddress = result[0].serveraddress;
                                    var config = {
                                        user: username,
                                        password: pwd,
                                        server: serveraddress, 
                                        database: dbname 
                                    };
                                    sql.connect(config, function (err) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('nik', sql.VarChar, req.body.id);
                                            request.query("SELECT t1.* FROM tbl_ass_user t1 where nik=@nik", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    sql.close();
console.log('halo  ' +JSON.stringify(recordset.recordsets[0]));

                                                    res.status(200).json(recordset.recordsets[0]);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },


    //REPORTS
        createReport : function(req,res) {
            var reportname = req.body.reportname; 
            var functionname = req.body.functionname; 
            var reportcode = req.body.reportcode;
            var reportheader = req.body.reportheader;
            var reportfooter = req.body.reportfooter;
            var requestedby = req.body.requestedby;
            var serveraddress = req.body.serveraddress;
            var is_active = req.body.is_active;
            var plantcolumn = req.body.plantcolumn;
            var isfooter = req.body.isfooter;
            var isplantvisible = req.body.isplantvisible;
            var isperorganization = req.body.isperorganization;
        
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { reportcode: reportcode };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Report with the same code already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    reportname: reportname, 
                                    functionname: functionname, 
                                    reportcode : reportcode,
                                    reportheader : reportheader,
                                    reportfooter : reportfooter,
                                    requestedby : requestedby,
                                    serveraddress : new ObjectId(serveraddress),
                                    lastmodified : new Date(),
                                    is_active : is_active,
                                    plantcolumn : plantcolumn,
                                    isfooter : isfooter,
                                    isplantvisible : isplantvisible,
                                    isperorganization : isperorganization
                                };
                                dbo.collection("t_reports").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editReport : function(req,res) {
            var id = req.params.id;
            var reportname = req.body.reportname; 
            var functionname = req.body.functionname; 
            var reportcode = req.body.reportcode;
            var reportheader = req.body.reportheader;
            var reportfooter = req.body.reportfooter;
            var requestedby = req.body.requestedby;
            var is_active = req.body.is_active;
            var serveraddress = req.body.serveraddress;
            var plantcolumn = req.body.plantcolumn;
            var isfooter = req.body.isfooter;
            var isplantvisible = req.body.isplantvisible;
            var isperorganization = req.body.isperorganization;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            reportname: reportname, 
                            functionname: functionname, 
                            reportcode : reportcode,
                            reportheader : reportheader,
                            reportfooter : reportfooter,
                            requestedby : requestedby,
                            lastmodified : new Date(), 
                            serveraddress : new ObjectId(serveraddress),
                            is_active : is_active,
                            plantcolumn : plantcolumn,
                            isfooter : isfooter,
                            isplantvisible : isplantvisible,
                            isperorganization : isperorganization
                        } 
                    };
                    dbo.collection("t_reports").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        deleteReport : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getReports : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).sort({'reportcode' : 1}).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getReportByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //FIXME:
        getTopicsByReport : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).aggregate([
                        { $lookup:
                            {
                                from: 't_topic_reports',
                                localField: '_id',
                                foreignField: 'reportid',
                                as: 'report_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_topics',
                                    localField: 'report_details.topicid',
                                    foreignField: '_id',
                                    as: 'topic_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                }
            });
        },

    //TOPICS
	copyTopic : function(req,res) {
            const {
                sourcetopic_id,
                desttopic_code
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(sourcetopic_id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var myobj = { 
                                topicname : result[0].topicname,
                                topiccode : desttopic_code,
                                lastmodified : new Date(),
                                reports : result[0].reports,
                                emailroles : result[0].emailroles,
                                msg1roles : result[0].msg1roles,
                                startdate : result[0].startdate,
                                emaillastrun : new Date(),
                                msg1lastrun : new Date(),
                                emailsuccess : 'N',
                                msg1success : 'N',
                                is_active : 0,
                                emailscheduleselected : result[0].emailscheduleselected,
                                msg1scheduleselected : result[0].msg1scheduleselected,
                            };
                            dbo.collection("t_topics").insertOne(myobj, function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    return res.status(200).json({id : result.insertedId});
                                }
                            });
                        }
                    });
                }
            });
            
        },


        createTopic : function(req,res) {
            var topicname = req.body.topicname; 
            var topiccode = req.body.topiccode; 
            var reports = req.body.reports;
            var emailroles = req.body.emailroles;
            var msg1roles = req.body.msg1roles;
            var is_active = req.body.is_active;
            var emailscheduleselected = req.body.emailscheduleselected;
            var msg1scheduleselected = req.body.msg1scheduleselected;
            var startdate = req.body.startdate;
           
            var rpt = [];
            var em = [];
            var msg = [];
            var i = 0;
            for(i=0;i<reports.length;i++) {
                rpt.push({value : new ObjectId(reports[i])});
            }
            for(i=0;i<emailroles.length;i++) {
                em.push({value : new ObjectId(emailroles[i])});
            }
            for(i=0;i<msg1roles.length;i++) {
                msg.push({value : new ObjectId(msg1roles[i])});
            }

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { topiccode: topiccode };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Topic with the same code already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    topicname : topicname,
                                    topiccode : topiccode,
                                    lastmodified : new Date(),
                                    reports : rpt,
                                    emailroles : em,
                                    msg1roles : msg,
                                    startdate : new Date(startdate),
                                    emaillastrun : startdate!=null ? new Date(startdate) : new Date(),
                                    msg1lastrun : new Date(),
                                    emailsuccess : 'N',
                                    msg1success : 'N',
                                    is_active : is_active,
                                    emailscheduleselected : emailscheduleselected,
                                    msg1scheduleselected : msg1scheduleselected,
                                };
                                dbo.collection("t_topics").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editTopicSchedule : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find({is_active : 1}).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            async.eachSeries(result, function(obj,callback){
                                var tm = '07:00';
                                var dtnew = obj.emaillastrun;
                                var tmnew = obj.startdate;
                                //console.log(dtnew + ' ' + tmnew);
                                if (tmnew!=null && tmnew!=undefined) tm = tmnew.getHours().toString() + ':' + tmnew.getMinutes().toString();
                                //console.log('itu ' + tm);
                                var dt1 = dtnew.getFullYear().toString() + '-' + (dtnew.getMonth()+1).toString() + '-' + dtnew.getDate().toString() + ' ' + tm; 
                                var dt = new Date(dt1);
                                //console.log('ini ' + dt)
                                newvalues = { $set: 
                                    {
                                        emaillastrun : dt
                                    }
                                    
                                };
                                dbo.collection("t_topics").updateOne({_id : new ObjectId(obj._id)}, newvalues, function(err, result2) {
                                    if (err) {
                                        return callback(err);
                                    }
                                    else {
                                        return callback();
                                    }
                                });
                            }, function(err3){
                                if (err3) {
                                    writeErrorLog('Error update schedule',err3);
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    return res.status(200).json('success');
                                }
                            });
                        }
                    });
                }
            });
        },

        editTopicWA : function(req,res) {
            var id = req.params.id;
            var msg1roles = req.body.msg1roles;
            var msg1scheduleselected = req.body.msg1scheduleselected;
            var startdate2 = req.body.startdate2;
            var is_active2 = req.body.is_active2;

            //console.log(startdate);
            var msg = [];
            for(i=0;i<msg1roles.length;i++) {
                msg.push({value : new ObjectId(msg1roles[i])});
            }

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id : new ObjectId(id)};
                    var newvalues =  {};
                    if (startdate2==null) {
                    newvalues = { $set: 
                            {
                                lastmodified2 : new Date(),
                                msg1roles : msg,
                                msg1success : 'N',
                                //emaillastrun : new Date(),
                                //msg1lastrun : new Date(),
                                is_active2 : is_active,
                                msg1scheduleselected : msg1scheduleselected
                            } 
                        };
                    }
                    else {
                        newvalues = { $set: 
                            {
                                lastmodified2 : new Date(),
                                msg1roles : msg,
                                msg1success : 'N',
                                startdate2 : new Date(startdate2),
                                msg1lastrun : new Date(),
                                is_active2 : is_active2,
                                msg1scheduleselected : msg1scheduleselected
                            } 
                        };
                    }
                    dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        editTopic : function(req,res) {
            var id = req.params.id;
            var topicname = req.body.topicname; 
            var topiccode = req.body.topiccode; 
            var reports = req.body.reports;
            var emailroles = req.body.emailroles;
            var msg1roles = req.body.msg1roles;
            var is_active = req.body.is_active;
            var emailscheduleselected = req.body.emailscheduleselected;
            var msg1scheduleselected = req.body.msg1scheduleselected;
            var startdate = req.body.startdate;
            var emaillastrun = req.body.emaillastrun;

            //console.log(startdate);

            var rpt = [];
            var em = [];
            var msg = [];
            var i = 0;
            for(i=0;i<reports.length;i++) {
                rpt.push({value : new ObjectId(reports[i])});
            }
            for(i=0;i<emailroles.length;i++) {
                em.push({value : new ObjectId(emailroles[i])});
            }
            for(i=0;i<msg1roles.length;i++) {
                msg.push({value : new ObjectId(msg1roles[i])});
            }

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id : new ObjectId(id)};
                    var newvalues =  {};
                    if (startdate==null) {
                    newvalues = { $set: 
                            {
                                topicname : topicname,
                                topiccode : topiccode,
                                lastmodified : new Date(),
                                reports : rpt,
                                emailroles : em,
                                msg1roles : msg,
                                emailsuccess : 'N',
                                //emaillastrun : new Date(),
                                //msg1lastrun : new Date(),
                                is_active : is_active,
                                emailscheduleselected : emailscheduleselected,
                                msg1scheduleselected : msg1scheduleselected
                            } 
                        };
                    }
                    else {
                        var dt = null;
                        if (new Date(startdate)>=new Date(emaillastrun))
                            dt = new Date(startdate);
                        else 
                            dt = new Date(emaillastrun);
                        newvalues = { $set: 
                            {
                                topicname : topicname,
                                topiccode : topiccode,
                                lastmodified : new Date(),
                                reports : rpt,
                                emailroles : em,
                                msg1roles : msg,
                                emailsuccess : 'N',
                                startdate : new Date(startdate),
                                emaillastrun : dt,
                                //msg1lastrun : new Date(),
                                is_active : is_active,
                                emailscheduleselected : emailscheduleselected,
                                msg1scheduleselected : msg1scheduleselected
                            } 
                        };
                    }
                    dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        deleteTopic : async function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            const client = new MongoClient(url.getDbString());
            await client.connect();
            const session = client.startSession();
            const transactionOptions = {
                readPreference: 'primary',
                readConcern: { level: 'local' },
                writeConcern: { w: 'majority' }
            };
            try {
                await session.withTransaction(async () => {
                const coll1 = client.db(dbname.getDbName()).collection('t_topics');
                const coll2 = client.db(dbname.getDbName()).collection('t_topic_reports');
                const coll3 = client.db(dbname.getDbName()).collection('t_topic_message_types_to_roles');
                var dt = [];
                async.eachSeries(col3x,function(obj,callback){
                    dt.push(ObjectId(obj._id));
                    return callback();
                }, async function(err){
                    if (err) {
                        await session.endSession();
                        await client.close();
                        auth.setResponse(res,400,err);
                        return;
                    }
                    else {
                        await coll1.deleteOne({  _id: new ObjectId(id) }, { session });
                        await coll2.deleteMany({ topicid : new ObjectId(id) }, { session });
                        await coll3.deleteMany({ topicid : new ObjectId(id) }, { session });
                    }
                });
                }, transactionOptions);
                /*
                const coll3 = client.db(dbname.getDbName()).collection('t_topic_message_types');
                const col3x = client.db(dbname.getDbName()).collection('t_topic_message_types').find({topicid :id});
                const coll4 = client.db(dbname.getDbName()).collection('t_topic_message_types_to_roles');
                var dt = [];
                async.eachSeries(col3x,function(obj,callback){
                    dt.push(ObjectId(obj._id));
                    return callback();
                }, function(err){
                    if (err) {
                        await session.endSession();
                        await client.close();
                        auth.setResponse(res,400,err);
                        return;
                    }
                    else {
                        await coll1.deleteOne({ "_id" : ObjectId(id) }, { session });
                        await coll2.deleteMany({ topicid : id }, { session });
                        await coll3.deleteMany({ topicid : id }, { session });
                        await coll4.deleteMany({  _id: { $in: dt } }, { session });
                    }
                });
                }, transactionOptions);
                */
            } finally {
                await session.endSession();
                await client.close();
                return res.status(200).json('success');
            }
        },

        getTopics : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = { $or: [{is_active: 1}, {is_active: 0}, {is_active: ''}]}
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getTopicsError : function(req,res) {

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = { $or: [ {emailsuccess : 'F'}, { emailsuccess : 'Z'} ] }
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).sort({emaillastrun : -1}).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getTopicByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        alignSchedule : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    writeErrorLog('Step align schedule 1', err)
                    //auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find({is_active : 1}).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            writeErrorLog('Step align schedule 2', err)
                            //auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            async.eachSeries(result, function(obj,callback){
                                var tm = '07:00';
                                var dtnew = obj.emaillastrun;
                                var tmnew = obj.startdate;
                                
                                //console.log(dtnew + ' ' + tmnew);
                                var hr = tmnew.getHours();
                                if (obj.emailscheduleselected==4) {
                                    hr = hr-4;
                                    dtnew = new Date();
                                }
                                if (obj.emailscheduleselected==8) {
                                    hr = hr-8;
                                    dtnew = new Date();
                                } 
                                if (tmnew!=null && tmnew!=undefined) tm = hr.toString() + ':' + tmnew.getMinutes().toString();
                                //console.log('itu ' + tm);
                                var dt1 = dtnew.getFullYear().toString() + '-' + (dtnew.getMonth()+1).toString() + '-' + dtnew.getDate().toString() + ' ' + tm; 
                                var dt = new Date(dt1);
                                //console.log('ini ' + dt)
                                
                                newvalues = { $set: 
                                    {
                                        emaillastrun : dt
                                    }
                                    
                                };
                                dbo.collection("t_topics").updateOne({_id : new ObjectId(obj._id)}, newvalues, function(err, result2) {
                                    if (err) {
                                        return callback(err);
                                    }
                                    else {
                                        return callback();
                                    }
                                });
                            }, function(err3){
                                if (err3) {
                                    writeErrorLog('Error update schedule',err3);
                                    db.close();
                                    //auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    return res.status(200).json('success');
                                }
                            });
                        }
                    });
                }
            });
        },

        getError : function(req,res) {

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_error_logs").find().sort({lastmodified : -1}).limit(1000).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },



        //FIXME:
        getTopicByIDTemp : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).aggregate([
                        { $lookup:
                            {
                                from: 't_topic_reports',
                                localField: '_id',
                                foreignField: 'topicid',
                                as: 'report_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'report_details.reportid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_topic_message_types_to_roles',
                                    localField: '_id',
                                    foreignField: 'topicid',
                                    as: 'type_details'
                                }
                            }
                            
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'type_details.roleid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                    /*
                            dbo.collection("t_topic_reports").find({topicid : id}).toArray(function(err, result2) {
                              
                                    dbo.collection("t_topic_message_types").find({topicid : id}).toArray(function(err, result3) {
                                        if (err) {
                                            auth.setResponse(res,400,err);
                                            db.close();
                                            return;
                                        }
                                        else {

                                        }
                                    });
                                }
                            });  
                            var jsonArr = [];
                            for(var i=0;i<result.length;i++) {
                               
                                dbo.collection("t_topics").find(query).toArray(function(err, result) {
                                jsonArr.push({
                                    _id : result[i]._id,
                                    topiccode : result[i].topiccode,
                                    topicname : result[i].topicname,
                                    lastmodified : helper.formatDate(result[i].lastmodified),
                                });
                            
                            }
                            db.close();
                            return res.status(200).json(jsonArr);
                        }
                    });
                }
                */
            }
            });
        },

        addReportToTopic : function(req,res) {
            var reportid = req.body.reportid; 
            var topicid = req.body.topicid; 
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { $and: [ {reportid: reportid}, {topicid: topicid} ]};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Topic and report with the same code already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    topicid : topicid,
                                    reportid : reportid,
                                    lastmodified : new Date(),
                                };
                                dbo.collection("t_topic_reports").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        removeReportFromTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_reports").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //FIXME:
        getReportsByTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).aggregate([
                        { $lookup:
                            {
                                from: 't_topic_reports',
                                localField: '_id',
                                foreignField: 'topicid',
                                as: 'report_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'report_details.reportid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                }
            });
        },

        addMessageTypeRoleToTopic : function(req,res) {
            var messagetypeid = req.body.messagetypeid; 
            var roleid = req.body.roleid; 
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { $and: [ {messagetypeid: messagetypeid}, {roleid: roleid} ]};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_message_types_to_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Same combination already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    messagetypeid : messagetypeid,
                                    roleid : roleid,
                                    lastmodified : new Date(),
                                };
                                dbo.collection("t_topic_message_types_to_roles").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        removeMessageTypeRoleToTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_message_types_to_roles").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //FIXME:
        getRolesByTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query)
                        .aggregate([
                        { $lookup:
                            {
                                from: 't_topic_message_types_to_roles',
                                localField: '_id',
                                foreignField: 'topicid',
                                as: 'type_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'type_details.roleid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                }
            });
        },

        testAPI : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    writeErrorLog('Step 1',err);
                    //auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var jsonTopFinal = [];
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics")
                        .aggregate([
                            { 
                                $match:
                                {
                                    is_active : 1,
                                    topiccode : 'TFOP-034'
                                },
                            },
                            {
                                $project: 
                                {
                                    _id : true,
                                    topicname : true,
                                    topiccode : true,
                                    reports : true,
                                    emailroles : true,
                                    emailscheduleselected : true,
                                    emailsuccess : true,
                                    emaillastrun : true,
                                    todaydate : { $dayOfMonth: new Date() },
                                    todayhour : { $hour : { date : new Date(), timezone: "+07"}},
                                    todayhour2 : { $hour : { date : '$startdate', timezone: "+07"}},
                                    todayhour3 : '$startdate',
                                    lasteighthours : {
                                    
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 8
                                            ]        
                                    
                                    },
                                    lastfivehours : {
                                            
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 5
                                            ]        
                                    
                                    },
                                    lastfourhours : {
                                    
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 4
                                            ]        
                                    
                                    },
                                    lastrunday : {
                                    
                                                $divide: 
                                                    [    
                                                        { 
                                                            $subtract: [new Date(), '$emaillastrun'] 
                                                        },  1000 * 60 * 60 * 24
                                                    ]        
                                            
                                    },
                                    lastrunweek : {
                                        
                                                $divide: 
                                                    [
                                                        {$divide: 
                                                            [
                                                                { 
                                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                                },  1000 * 60 * 60 * 24
                                                            ]
                                                        }, 7
                                                    ]            
                                        
                                    },
                                    lastrunmonth : {
                                        
                                                $divide:
                                                [
                                                    {$divide: 
                                                        [
                                                            { 
                                                                $subtract: [new Date(), '$emaillastrun'] 
                                                            },  (1000 * 60 * 60 * 24)
                                                        ]
                                                    }, 30
                                                ]    
                                            
                                    },
                                    lastrunmonthfeb : {
                                        
                                        $divide:
                                        [
                                            {$divide: 
                                                [
                                                    { 
                                                        $subtract: [new Date(), '$emaillastrun'] 
                                                    },  (1000 * 60 * 60 * 24)
                                                ]
                                            }, 28
                                        ]    
                                    
                                    },
                                },
                                
                            },
                            
                            {
                                $match:
                                { 
                                    $or : 
                                        [ 
                                            {
                                                $and:
                                                    [ 
                                                        {lastfivehours : { $gt : 1}, emailscheduleselected :5 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastfourhours : { $gt : 1}, emailscheduleselected :4 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lasteighthours : { $gt : 1}, emailscheduleselected :8 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunday : { $gt : 1}, emailscheduleselected :0 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunweek : { $gt : 1}, emailscheduleselected :1 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunmonth : { $gt : 1}, emailscheduleselected :2 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [
                                                        {
                                                            todaydate : { $eq : 1 },
                                                            emailscheduleselected : {$eq : 99},
                                                            lastrunmonthfeb : { $gt : 1},
                                                            $expr:{$eq:["$todayhour", "$todayhour2"]}, 
                                                        }
                                                    ] 
                                            },
                                        ]
                                },
                            },
                            
                            {   
                                
                                $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'reports.value',
                                    foreignField: '_id',
                                    as: 'reportdetails'
                                }
                                    
                                    
                            },
                            //{
                            //    $sort: {"reportdetails.reportcode": 1}
                            //},
                            {
                                $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'emailroles.value',
                                    foreignField: '_id',
                                    as: 'roledetails'
                                },
                            },
                        ])
                        .toArray(function(errB,result) { 
                            if (errB) {
                                db.close();
                                writeErrorLog('Step 2',errB);
                                return res.status(200).json(errB);
                            }
                            else {
                                return res.status(200).json(result);
                            }
                        }
                        
                    );
                }
            });
        },

    }

    module.exports = mainOps;

    function writeErrorLog(step,err) {
        var MongoClient = mongo.MongoClient;
        MongoClient.connect(url.getDbString(), function(errx, db) {
            if (errx) {
                db.close();
                return;
            }
            else {
                var lastrun = new Date();
                var myobj = { 
                    step : step,
                    error_desc : JSON.stringify(err),
                    lastmodified : lastrun,
                };
                var dbo = db.db(dbname.getDbName());
                dbo.collection("t_error_logs").insertOne(myobj, function(err, result) {
                    if (err) {
                        db.close();
                        return;
                    }
                });
            }
        });
    }

    function GetSortOrder(prop) {    
        return function(a, b) {    
            if (a[prop] > b[prop]) {    
                return 1;    
            } else if (a[prop] < b[prop]) {    
                return -1;    
            }    
            return 0;    
        }    
    }    

    function sendEmail(jsonRes,forceRun) {
        var transport = nodemailer.createTransport({
            pool: true,
            maxConnections: 1,
            host: "mail.kiranamegatara.com",
            port: 465,
            secure: true, // use TLS
            auth: {
              user: "no-reply@kiranamegatara.com",
              pass: "1234567890"
            }
        });
        var filterEmp = [];
        //console.log(JSON.stringify(jsonRes[0].HR_recipients));
        //TOPIC ITERATION   
        var dtnowwtholiday = null;
        var dtnow = null;
        var d9days = null;
        var d3days = null;
        var dyes = null;
        var dyesd = null;
        var d7days = null;
        var d8days = null;
        var d14days = null;
        var d15days = null;
        helper.formatDateShortWorkingDay(new Date())
        .then(function(xtnow){
            dtnow = xtnow;
            return helper.formatDateShort(new Date());
        })
        .then(function(xtnow){
            
            dtnowwtholiday = xtnow;
            return helper.formatDateShortWorkingDay(new Date(),-9);
        })
        .then(function(x9days){
            d9days = x9days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-3));
        })
        .then(function(x3days){
            d3days = x3days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-1));
        })
        .then(function(xyes){
            dyes = xyes;
            return helper.formatDateShort(helper.addDays(new Date(),-1));
        })
        .then(function(xyesd){
            dyesd = xyesd;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-7));
        })
        .then(function(x7days){
            d7days = x7days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-8));
        })
        .then(function(x8days){
            d8days = x8days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-14));
        })
        .then(function(x14days){
            d14days = x14days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-15));
        })
        .then(function(x15days){
            d15days = x15days;
            for (var z=0;z<jsonRes.length;z++) {
                topicid =  jsonRes[z]._id;
                topiccode = jsonRes[z].topiccode;
                topicname = jsonRes[z].topicname;
    
                //NOTE: Special Recipient
                //try {
                    filterEmp = [];
                    var special_recipients = removeDuplicates(jsonRes[z].special_recipients);
                    for(var i=0;i<special_recipients.length;i++) {
                        filterEmp.push(special_recipients[i].nik);
                        var htm = '';
                        var oktosend = false;
                        htm = '<head>' +
                        '<style>' +
                        'table {' +
                            'font-family: arial, sans-serif;' +
                            'font-size:14px;' +
                            'border-collapse: collapse;' +
                            'width: 100%;' +
                        '}' +
                        
                        'td, th {' +
                            'border: 1px solid #dddddd;' +
                            'padding: 8px;' +
                        '}' +
                        
                        'tr:nth-child(even) {' +
                            'background-color: #dddddd;' +
                        '}' +
                        
                    
                        '</style>' +
                        '</head>'; 
                        htm = htm + '<b>Kepada Bapak/Ibu ' + special_recipients[i].nama + '</b><p/>';
                        htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                        jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                        for (var t=0;t<jsonRes[z].reports.length;t++) {            
                            if (special_recipients[i].email!=null && special_recipients[i].email!='' && special_recipients[i].email!=undefined) {
                            
                                var hdr = '';
                                if (jsonRes[z].reports[t].rows.length>0) {
                                    var strheader = '';
                                    
                                
                                    strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                    strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                    strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                    strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                    strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                    strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                    strheader = strheader.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                    strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                    strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                    strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                    strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                    strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                    strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                    strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                    
                                    strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                    strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                    strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                    strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");

                                    htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                    hdr = '<tr>'
                                    for (var key in jsonRes[z].reports[t].rows[0]) {
                                        var found = false;
                                        var count = 0;
                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {
                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                found = true;
                                                hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                            } 
                                            count++;
                                        }
                                        //sini plant 1
                                        found = false;
                                        count = 0;
                                        //console.log('sini ' + jsonRes[z].reports[t].isplantvisible)
                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {
                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                found = true;
                                                //console.log('sni ' + jsonRes[z].reports[t].isplantvisible)
                                                if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                {
                                                    if (jsonRes[z].reports[t].isplantvisible==0)
                                                        hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                }
                                                else
                                                    hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                            }
                                            count++;
                                        }
                                    }
                                    hdr = hdr + '</tr>'
                                }
                        
                                var plantSelected = [];
                                for(var c=0;c<special_recipients[i].plants.length;c++) {
                                    plantSelected.push(
                                        special_recipients[i].plants[c].value
                                    )
                                }
                                plantSelected.push('KMTR');
                                //console.log(JSON.stringify(plantSelected))
                                var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                var rows = jsonRes[z].reports[t].rows;
                                var rowsfiltered = rows;
                                
                                //TODO: sini plant modifikasi
                                if (jsonRes[z].reports[t].isperorganization==1) {
                                    var strpos = special_recipients[i].posisi + ' ' + special_recipients[i].divisi + ' ' + special_recipients[i].dept;
                                    console.log(strpos);
                                    rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                    console.log(JSON.stringify(rowsfiltered));
                                }
                                else
                                    rowsfiltered = rows.filter(row => plantSelected.includes(row[plantcolname]));
                                //console.log(JSON.stringify(rowsfiltered));
                                var ctnt = '';
                                var ftr = '';
                                var rw = '';
                                var isfooter = false;
        
                                var checkfooter = false;
                                if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                    checkfooter = true;
                                }
        
                                for (var h=0;h<rowsfiltered.length;h++) {
                                    oktosend = true;
                                    var bgcolor = "#F5F5F5";
                                    rw = '';
                                    if (checkfooter==true) {
                                        for (var key in rowsfiltered[h]) {
                                            var found = false;
                                            var count = 0;
                                            var val = rowsfiltered[h][key];
                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                if (key==jsonRes[z].reports[t].isfooter) {
                                                    if (val==1 || val=='1' || val==true) {
                                                        bgcolor = "#2b471d";
                                                        isfooter = true;
                                                        found = true;
                                                    }  
                                                }
                                                count++;
                                            }
                                        }
                                    }
                                    var targetfound = false;
                                    var targetfound2 = false;
                                    var targetfound3 = false;
                                    var target = 0;
                                    var target2 = 0;
                                    var target3 = 0;

                                    for (var key in rowsfiltered[h]) {
                                        var val = rowsfiltered[h][key];
                                        var valx = val;
                                        var count = 0;
                                        
        
                                        while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {
                                            
                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                targetfound = true;
                                                target = valx;
                                            }
                                            count++;
                                        } 

                                        var count = 0; 
        
                                        while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {
                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                targetfound2 = true;
                                                target2 = valx;
                                            }
                                            count++;
                                        } 

                                        var count = 0; 
        
                                        while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {
                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                targetfound3 = true;
                                                target3 = valx;
                                            }
                                            count++;
                                        } 

                                        var found = false;
                                        var count = 0;
        
                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {

                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                found = true;
                                                //console.log('val : ' + val + ' ,key : ' + key + ' ,columnname : ' + jsonRes[z].reports[t].columnnames[count].columnname + ' format : ' + jsonRes[z].reports[t].columnnames[count].formatcolumn)
                                                if (val!=null && val!=undefined) {
                                                    switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                        case 0:
                                                            val = val;
                                                            break;
                                                        case 1:

                                                            val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                            break;
                                                        case 2:
                                                            val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                            break;
                                                        case 3:
                                                            val = helper.formatDateShort(val)
                                                            break;
                                                        case 4:
                                                            val = helper.formatDate(val)
                                                            break;
                                                    }
                                                }
                                                var bgcolor2 = bgcolor;
                                                var fontcolor = "<font>";
                                                if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                        if (valx>target) {
                                                            fontcolor = "<font>"
                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                            //    fontcolor = "<font color='#009900'>";
                                                            //else
                                                            //    fontcolor = "<font color='#FF0000'>";
                                                        }
                                                    }
                                                    else {
                                                        if (valx<target) {
                                                            fontcolor = "<font>"
                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                            //    fontcolor = "<font color='#009900'>";
                                                            //else
                                                            //    fontcolor = "<font color='#FF0000'>";
                                                        }
                                                    }
                                                if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                        if (valx>target2) {
                                                            fontcolor = "<font>"
                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                            //    fontcolor = "<font color='#009900'>";
                                                            //else
                                                            //    fontcolor = "<font color='#FF0000'>";
                                                        }
                                                    }
                                                    else {
                                                        if (valx<target2) {
                                                            fontcolor = "<font>"
                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                            //    fontcolor = "<font color='#009900'>";
                                                            //else
                                                            //    fontcolor = "<font color='#FF0000'>";
                                                        }
                                                    }
                                                if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                        if (valx>target3) {
                                                            fontcolor = "<font>"
                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                            //    fontcolor = "<font color='#009900'>";
                                                            //else
                                                            //    fontcolor = "<font color='#FF0000'>";
                                                        }
                                                    }
                                                    else {
                                                        if (valx<target3) {
                                                            fontcolor = "<font>"
                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                            //    fontcolor = "<font color='#009900'>";
                                                            //else
                                                            //    fontcolor = "<font color='#FF0000'>";
                                                        }
                                                    }
                                                if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                    if (valx<0) fontcolor = "<font>";
                                                if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                    if (isfooter==false) 
                                                        bgcolor2 = "#b5ddc3";
                                                if (key!=jsonRes[z].reports[t].isfooter) {
                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                }
                                            }
                                            count++;
                                        } 
                                        //sini plant 2
                                        found = false;
                                        count = 0;
                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                        {
                                            if (key==jsonRes[z].reports[t].plantcolumn) {
                                                found = true;
                                                if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                    if (jsonRes[z].reports[t].isplantvisible==0) {
                                                        /*
                                                        if (val!=null && val!=undefined) {
                                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                case 0:
                                                                    val = val;
                                                                    break;
                                                                case 1:
                                                                    val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 2:
                                                                    console.log('val ' + jsonRes[z].reports[t].columnnames[count].formatcolumn + ' ' + val)
                                                                    val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 3:
                                                                    val = helper.formatDateShort(val)
                                                                    break;
                                                                case 4:
                                                                    val = helper.formatDate(val)
                                                                    break;
                                                            }
                                                        }
                                                        */
                                                        var bgcolor2 = bgcolor;
                                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                            if (isfooter==false) 
                                                                bgcolor2 = "#b5ddc3";
                                                        var fontcolor = "<font>";
                                                        if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                if (valx>target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                if (valx>target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                if (valx>target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                            if (valx<0) fontcolor = "<font>";
                                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                        }
                                                    }
                                                }
                                                else {
                                                    if (val!=null && val!=undefined) {
                                                        /*
                                                        switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                            case 0:
                                                                val = val;
                                                                break;
                                                            case 1:
                                                                val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                break;
                                                            case 2:
                                                                val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                break;
                                                            case 3:
                                                                val = helper.formatDateShort(val)
                                                                break;
                                                            case 4:
                                                                val = helper.formatDate(val)
                                                                break;
                                                        }
                                                        */
                                                    }
                                                    var bgcolor2 = bgcolor;
                                                    if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                        if (isfooter==false) 
                                                            bgcolor2 = "#b5ddc3";
                                                    var fontcolor = "<font>";
                                                    if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                            if (valx>target) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                        else {
                                                            if (valx<target) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                    if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                            if (valx>target2) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                        else {
                                                            if (valx<target2) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                    if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                            if (valx>target3) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                        else {
                                                            if (valx<target3) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                    if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                        if (valx<0) fontcolor = "<font>";
                                                    if (key!=jsonRes[z].reports[t].isfooter) {
                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                    }
                                                }
                                            }
                                            count++;
                                        } 
        
                                    }
                                    if (isfooter==true) {
                                        isfooter = false;
                                        ftr = '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>';
                                    }
                                    else {
                                        ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                    }
                                }
                                var strfooter = '';
                                strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                strfooter = strfooter.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                if (ctnt!='')
                                    htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                //else 
                                //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                            }
                        }
        
                        htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
        
                        var mailOptions = {
                            from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                            to: special_recipients[i].email,//narendra.adinugraha@kiranamegatara.com
                            subject: jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname,
                            html: htm,
                        };
                        if (oktosend==true) {
                            console.log('SPECIAL : ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                            if (special_recipients[i].email!='' && special_recipients[i].email!=null && special_recipients[i].email!=undefined)
                            transport.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                console.log(error);
                                writeErrorLog('Error sending e-mail to ' + special_recipients[i].email,error);
                                }
                            });
                        }
                        else {
                            console.log('empty special : ' + special_recipients[i].nama + ' ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                        }
                    }
        
                    //NOTE: HR Recipient 
                    var rowsEmpFiltered =  [];
                    for(var i=0;i<jsonRes[z].ex_recipients.length;i++) {
                        filterEmp.push(jsonRes[z].ex_recipients[i].nik);
                    }
                    if (jsonRes[z].HR_recipients.length>0) {
                        var HR_emp  = [];

                        for(var i=0;i<jsonRes[z].HR_recipients.length;i++) {
                            for(var j=0;j<jsonRes[z].HR_recipients[i].length;j++) {
                                HR_emp.push(jsonRes[z].HR_recipients[i][j]);
                            }
                        }
                        if (filterEmp.length>0)
                            rowsEmpFiltered = HR_emp.filter(row => !filterEmp.includes(row.nik));
                        else 
                            rowsEmpFiltered = HR_emp;
                    
                        //console.log('sini ' + JSON.stringify(rowsEmpFiltered));
                        rowsEmpFiltered = removeDuplicates(rowsEmpFiltered);
                        for (var g=0;g<rowsEmpFiltered.length;g++) {
                            //for(var i=0;i<rowsEmpFiltered[g].length;i++) {
                                var htm = '';
                                var oktosend = false;
                                htm = '<head>' +
                                        '<style>' +
                                        'table {' +
                                            'font-family: arial, sans-serif;' +
                                            'font-size:14px;' +
                                            'border-collapse: collapse;' +
                                            'width: 100%;' +
                                        '}' +
                                        
                                        'td, th {' +
                                            'border: 1px solid #dddddd;' +
                                            'padding: 8px;' +
                                        '}' +
                                        
                                        'tr:nth-child(even) {' +
                                            'background-color: #dddddd;' +
                                        '}' +
                                        
                                    
                                        '</style>' +
                                        '</head>'; 
                                        htm = htm + '<b>Kepada Bapak/Ibu ' + rowsEmpFiltered[g].nama + '</b><p/>';
                                        htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                                jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                                for (var t=0;t<jsonRes[z].reports.length;t++) {
                                    if (rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=undefined) {
                                        
                                        var hdr = '';
                                        if (jsonRes[z].reports[t].rows.length>0) {
                                            var strheader = '';
                                            strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                            strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                            strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                            strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                            strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                            strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                            strheader = strheader.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                            strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                            strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                            strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                            strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                            strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                            strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                            strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                            
                                            strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                            strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                            strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                            strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");
                                            htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                        
                                            hdr = '<tr>'
                                            for (var key in jsonRes[z].reports[t].rows[0]) {
                                                var found = false;
                                                var count = 0;
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                    }
                                                    count++;
                                                }
                                                //sini plant 1
                                                found = false;
                                                count = 0;
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                        {
                                                            if (jsonRes[z].reports[t].isplantvisible==0)
                                                                hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                        }
                                                        else
                                                            hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                    }
                                                    count++;
                                                }
                                            }
                                            hdr = hdr + '</tr>'
                                        }
                            
                                        var rows = jsonRes[z].reports[t].rows;
                                        var rowsfiltered = rows;
                                        //TODO: sini plant modifikasi
                                        if (jsonRes[z].reports[t].isperorganization==1) {
                                            var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                            if (rowsEmpFiltered[g].ho!='n') {
                                                var strpos = rowsEmpFiltered[g].posst + ' ' + rowsEmpFiltered[g].divisi + ' ' + rowsEmpFiltered[g].dept;
                                                rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                            }
                                            else {
                                                rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                            }
                                        }
                                        else {
                                            if (rowsEmpFiltered[g].ho=='n') {
                                                var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                            }
                                        }
                                        plantSelected.push('KMTR');
                                        var ctnt = '';
                                        var ftr = '';
                                        var rw = '';
                                        var isfooter = false;
            
                                        var checkfooter = false;
                                        if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                            checkfooter = true;
                                        }
                                        for (var h=0;h<rowsfiltered.length;h++) {
                                            oktosend = true;
                                            var bgcolor = "#F5F5F5";
                                            rw = '';
                                            if (checkfooter==true) {
                                                for (var key in rowsfiltered[h]) {
                                                    var val = rowsfiltered[h][key];
                                                    var found = false;
                                                    var count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                        if (key==jsonRes[z].reports[t].isfooter) {
                                                            if (val==1 || val=='1' || val==true) {
                                                                bgcolor = "#2b471d";
                                                                isfooter = true;
                                                                found = true;
                                                            }  
                                                        }
                                                        count++;
                                                    }
                                                }
                                            }
                                            var targetfound = false;
                                            var targetfound2 = false;
                                            var target = 0;
                                            var target2 = 0;
                                            for (var key in rowsfiltered[h]) {
                                                var val = rowsfiltered[h][key];
                                                var valx = val;

                                                var count = 0;
                                            
                                                while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                        targetfound = true;
                                                        target = valx;
                                                    }
                                                    count++;
                                                } 

                                                var count = 0;
                                            
                                                while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                        targetfound2 = true;
                                                        target2 = valx;
                                                    }
                                                    count++;
                                                } 

                                                var count = 0; 
            
                                                while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                        targetfound3 = true;
                                                        target3 = valx;
                                                    }
                                                    count++;
                                                } 

                                                var found = false;
                                                var count = 0;
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        if (val!=null && val!=undefined) {
                                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                case 0:
                                                                    val = val;
                                                                    break;
                                                                case 1:
                                                                    val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 2:
                                                                    val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 3:
                                                                    val = helper.formatDateShort(val)
                                                                    break;
                                                                case 4:
                                                                    val = helper.formatDate(val)
                                                                    break;
                                                            }
                                                        }
                                                        var bgcolor2 = bgcolor;
                                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                            if (isfooter==false) 
                                                                bgcolor2 = "#b5ddc3";
                                                        var fontcolor = "<font>";
                                                        if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                if (valx>target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                if (valx>target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                if (valx>target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                            if (valx<0) fontcolor = "<font>";
                                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                        }
                                                    }
                                                    count++;
                                                }
                                                //sini plant 2
                                                found = false;
                                                count = 0;
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                            if (jsonRes[z].reports[t].isplantvisible==0) {
                                                                /*
                                                                if (val!=null && val!=undefined) {
                                                                    switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                        case 0:
                                                                            val = val;
                                                                            break;
                                                                        case 1:
                                                                            val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 2:
                                                                            val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 3:
                                                                            val = helper.formatDateShort(val)
                                                                            break;
                                                                        case 4:
                                                                            val = helper.formatDate(val)
                                                                            break;
                                                                    }
                                                                }
                                                                */
                                                                var bgcolor2 = bgcolor;
                                                                var fontcolor = "<font>";
                                                                if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                        if (valx>target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                        if (valx>target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                        if (valx>target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                    if (valx<0) fontcolor = "<font>";
                                                                if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                    if (isfooter==false) 
                                                                        bgcolor2 = "#b5ddc3";
                                                                if (key!=jsonRes[z].reports[t].isfooter) {
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            if (val!=null && val!=undefined) {
                                                                /*
                                                                switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                    case 0:
                                                                        val = val;
                                                                        break;
                                                                    case 1:
                                                                        val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 2:
                                                                        val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 3:
                                                                        val = helper.formatDateShort(val)
                                                                        break;
                                                                    case 4:
                                                                        val = helper.formatDate(val)
                                                                        break;
                                                                }
                                                                */
                                                            }
                                                            var bgcolor2 = bgcolor;
                                                            if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                if (isfooter==false) 
                                                                    bgcolor2 = "#b5ddc3";
                                                            var fontcolor = "<font>";
                                                            if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                    if (valx>target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                    if (valx>target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                    if (valx>target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                if (valx<0) fontcolor = "<font>";
                                                            if (key!=jsonRes[z].reports[t].isfooter) {
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            }
                                                        }
                                                    }
                                                    count++;
                                                } 
                                            }
                                            if (isfooter==true) {
                                                isfooter = false;
                                                ftr = '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>';
                                            }
                                            else {
                                                ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                            }
                                        }
                                        var strfooter = '';
                                        strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                        strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                        strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                        strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                        strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                        strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                        strfooter = strfooter.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                        strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                        strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                        strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                        strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                        strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                        strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                        strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                        if (ctnt!='')
                                            htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                        //else 
                                        //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                    }
                                }
                                htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                                var mailOptions = {
                                    from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                                    to: rowsEmpFiltered[g].email,//narendra.adinugraha@kiranamegatara.com
                                    subject: jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname,
                                    html: htm,
                                };
                                //console.log('ok todent ' + oktosend + ' ' + JSON.stringify(mailOptions))

                                if (oktosend==true) {
                                    console.log('HR : ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                    if (rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!=undefined)
                                        transport.sendMail(mailOptions, (error, info) => {
                                        if (error) {
                                            console.log(error)
                                            writeErrorLog('Error sending e-mail to ' + rowsEmpFiltered[g].email,error);
                                        }
                                    });
                                }
                                else {
                                    console.log('empty HR : ' + rowsEmpFiltered[g].nama + ' ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                }
                            //}
                        }
                    }
                //}
                //catch(err) {
                //    writeErrorLog('step try catch', topiccode + ' - ' + topicname + ' : ' + err);
                //}
                //var MongoClient = mongo.MongoClient;
                
                //LOGs
               
                
            }
            //transport.close();
        });
       
    }

    function sendEmail2(jsonRes,forceRun) {
        var MongoClient = mongo.MongoClient;
        MongoClient.connect(url.getDbString(), function(err, db) {
            if (err) {
                db.close();
                auth.setResponse(res,400,err);
                return;
            }
            else {
                var noreply = '';
                var query = { _id : new ObjectId(id)};
                var dbo = db.db(dbname.getDbName());
                var query = { servername : 'other' }
                dbo.collection("t_mail_servers").findOne(query).toArray(function(err, result) {
                    noreply = result[0].noreply;
                    var transport = nodemailer.createTransport({
                        pool: true,
                        maxConnections: 1,
                        host: result[0].serveraddress,
                        port: result[0].port,
                        secure: true, // use TLS
                        auth: {
                        user: result[0].username,
                        pass: result[0].pwd,
                        }
                    });
                    var filterEmp = [];
                    //console.log(JSON.stringify(jsonRes[0].HR_recipients));
                    //TOPIC ITERATION   
                    var dtnowwtholiday = null;
                    var dtnow = null;
                    var d9days = null;
                    var d3days = null;
                    var dyes = null;
                    var dyesd = null;
                    var d7days = null;
                    var d8days = null;
                    var d14days = null;
                    var d15days = null;
                    helper.formatDateShortWorkingDay(new Date())
                    .then(function(xtnow){
                        dtnow = xtnow;
                        return helper.formatDateShort(new Date());
                    })
                    .then(function(xtnow){
                        
                        dtnowwtholiday = xtnow;
                        return helper.formatDateShortWorkingDay(new Date(),-9);
                    })
                    .then(function(x9days){
                        d9days = x9days;
                        return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-3));
                    })
                    .then(function(x3days){
                        d3days = x3days;
                        return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-1));
                    })
                    .then(function(xyes){
                        dyes = xyes;
                        return helper.formatDateShort(helper.addDays(new Date(),-1));
                    })
                    .then(function(xyesd){
                        dyesd = xyesd;
                        return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-7));
                    })
                    .then(function(x7days){
                        d7days = x7days;
                        return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-8));
                    })
                    .then(function(x8days){
                        d8days = x8days;
                        return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-14));
                    })
                    .then(function(x14days){
                        d14days = x14days;
                        return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-15));
                    })
                    .then(function(x15days){
                        d15days = x15days;
                        for (var z=0;z<jsonRes.length;z++) {
                            topicid =  jsonRes[z]._id;
                            topiccode = jsonRes[z].topiccode;
                            topicname = jsonRes[z].topicname;
                
                            //NOTE: Special Recipient
                            //try {
                                filterEmp = [];
                                var special_recipients = removeDuplicates(jsonRes[z].special_recipients);
                                for(var i=0;i<special_recipients.length;i++) {
                                    filterEmp.push(special_recipients[i].nik);
                                    var htm = '';
                                    var oktosend = false;
                                    htm = '<head>' +
                                    '<style>' +
                                    'table {' +
                                        'font-family: arial, sans-serif;' +
                                        'font-size:14px;' +
                                        'border-collapse: collapse;' +
                                        'width: 100%;' +
                                    '}' +
                                    
                                    'td, th {' +
                                        'border: 1px solid #dddddd;' +
                                        'padding: 8px;' +
                                    '}' +
                                    
                                    'tr:nth-child(even) {' +
                                        'background-color: #dddddd;' +
                                    '}' +
                                    
                                
                                    '</style>' +
                                    '</head>'; 
                                    htm = htm + '<b>Kepada Bapak/Ibu ' + special_recipients[i].nama + '</b><p/>';
                                    //htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                                    jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                                    for (var t=0;t<jsonRes[z].reports.length;t++) {            
                                        if (special_recipients[i].email!=null && special_recipients[i].email!='' && special_recipients[i].email!=undefined) {
                                        
                                            var hdr = '';
                                            if (jsonRes[z].reports[t].rows.length>0) {
                                                var strheader = '';
                                                
                                            
                                                strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                                strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                                strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                                strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                                strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                                strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                                strheader = strheader.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                                strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                                strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                                strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                                strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                                strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                                strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                                strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                                
                                                strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                                strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                                strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                                strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");

                                                htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                                hdr = '<tr>'
                                                for (var key in jsonRes[z].reports[t].rows[0]) {
                                                    var found = false;
                                                    var count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                        } 
                                                        count++;
                                                    }
                                                    //sini plant 1
                                                    found = false;
                                                    count = 0;
                                                    //console.log('sini ' + jsonRes[z].reports[t].isplantvisible)
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            //console.log('sni ' + jsonRes[z].reports[t].isplantvisible)
                                                            if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                            {
                                                                if (jsonRes[z].reports[t].isplantvisible==0)
                                                                    hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                            }
                                                            else
                                                                hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                        }
                                                        count++;
                                                    }
                                                }
                                                hdr = hdr + '</tr>'
                                            }
                                    
                                            var plantSelected = [];
                                            //for(var c=0;c<special_recipients[i].plants.length;c++) {
                                            //    plantSelected.push(
                                            //        special_recipients[i].plants[c].value
                                            //    )
                                            //}
                                            //plantSelected.push('KMTR');
                                            //console.log(JSON.stringify(plantSelected))
                                            var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                            var rows = jsonRes[z].reports[t].rows;
                                            var rowsfiltered = rows;
                                            
                                            //TODO: sini plant modifikasi
                                            //if (jsonRes[z].reports[t].isperorganization==1) {
                                            //    var strpos = special_recipients[i].posisi + ' ' + special_recipients[i].divisi + ' ' + special_recipients[i].dept;
                                            //    console.log(strpos);
                                            //    rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                            //    console.log(JSON.stringify(rowsfiltered));
                                            //}
                                            //else
                                            //    rowsfiltered = rows.filter(row => plantSelected.includes(row[plantcolname]));
                                            //console.log(JSON.stringify(rowsfiltered));
                                            var ctnt = '';
                                            var ftr = '';
                                            var rw = '';
                                            var isfooter = false;
                    
                                            var checkfooter = false;
                                            if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                                checkfooter = true;
                                            }
                    
                                            for (var h=0;h<rowsfiltered.length;h++) {
                                                oktosend = true;
                                                var bgcolor = "#F5F5F5";
                                                rw = '';
                                                if (checkfooter==true) {
                                                    for (var key in rowsfiltered[h]) {
                                                        var found = false;
                                                        var count = 0;
                                                        var val = rowsfiltered[h][key];
                                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                            if (key==jsonRes[z].reports[t].isfooter) {
                                                                if (val==1 || val=='1' || val==true) {
                                                                    bgcolor = "#2b471d";
                                                                    isfooter = true;
                                                                    found = true;
                                                                }  
                                                            }
                                                            count++;
                                                        }
                                                    }
                                                }
                                                var targetfound = false;
                                                var targetfound2 = false;
                                                var targetfound3 = false;
                                                var target = 0;
                                                var target2 = 0;
                                                var target3 = 0;

                                                for (var key in rowsfiltered[h]) {
                                                    var val = rowsfiltered[h][key];
                                                    var valx = val;
                                                    var count = 0;
                                                    
                    
                                                    while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                            targetfound = true;
                                                            target = valx;
                                                        }
                                                        count++;
                                                    } 

                                                    var count = 0; 
                    
                                                    while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                            targetfound2 = true;
                                                            target2 = valx;
                                                        }
                                                        count++;
                                                    } 

                                                    var count = 0; 
                    
                                                    while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                            targetfound3 = true;
                                                            target3 = valx;
                                                        }
                                                        count++;
                                                    } 

                                                    var found = false;
                                                    var count = 0;
                    
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {

                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            //console.log('val : ' + val + ' ,key : ' + key + ' ,columnname : ' + jsonRes[z].reports[t].columnnames[count].columnname + ' format : ' + jsonRes[z].reports[t].columnnames[count].formatcolumn)
                                                            if (val!=null && val!=undefined) {
                                                                switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                    case 0:
                                                                        val = val;
                                                                        break;
                                                                    case 1:

                                                                        val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 2:
                                                                        val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 3:
                                                                        val = helper.formatDateShort(val)
                                                                        break;
                                                                    case 4:
                                                                        val = helper.formatDate(val)
                                                                        break;
                                                                }
                                                            }
                                                            var bgcolor2 = bgcolor;
                                                            var fontcolor = "<font>";
                                                            if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                    if (valx>target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                    if (valx>target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                    if (valx>target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                if (valx<0) fontcolor = "<font>";
                                                            if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                if (isfooter==false) 
                                                                    bgcolor2 = "#b5ddc3";
                                                            if (key!=jsonRes[z].reports[t].isfooter) {
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            }
                                                        }
                                                        count++;
                                                    } 
                                                    //sini plant 2
                                                    found = false;
                                                    count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                                if (jsonRes[z].reports[t].isplantvisible==0) {
                                                                    /*
                                                                    if (val!=null && val!=undefined) {
                                                                        switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                            case 0:
                                                                                val = val;
                                                                                break;
                                                                            case 1:
                                                                                val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 2:
                                                                                console.log('val ' + jsonRes[z].reports[t].columnnames[count].formatcolumn + ' ' + val)
                                                                                val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 3:
                                                                                val = helper.formatDateShort(val)
                                                                                break;
                                                                            case 4:
                                                                                val = helper.formatDate(val)
                                                                                break;
                                                                        }
                                                                    }
                                                                    */
                                                                    var bgcolor2 = bgcolor;
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                        if (isfooter==false) 
                                                                            bgcolor2 = "#b5ddc3";
                                                                    var fontcolor = "<font>";
                                                                    if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                            if (valx>target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                            if (valx>target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                            if (valx>target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                        if (valx<0) fontcolor = "<font>";
                                                                    if (key!=jsonRes[z].reports[t].isfooter) {
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                if (val!=null && val!=undefined) {
                                                                    /*
                                                                    switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                        case 0:
                                                                            val = val;
                                                                            break;
                                                                        case 1:
                                                                            val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 2:
                                                                            val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 3:
                                                                            val = helper.formatDateShort(val)
                                                                            break;
                                                                        case 4:
                                                                            val = helper.formatDate(val)
                                                                            break;
                                                                    }
                                                                    */
                                                                }
                                                                var bgcolor2 = bgcolor;
                                                                if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                    if (isfooter==false) 
                                                                        bgcolor2 = "#b5ddc3";
                                                                var fontcolor = "<font>";
                                                                if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                        if (valx>target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                        if (valx>target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                        if (valx>target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                    if (valx<0) fontcolor = "<font>";
                                                                if (key!=jsonRes[z].reports[t].isfooter) {
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                }
                                                            }
                                                        }
                                                        count++;
                                                    } 
                    
                                                }
                                                if (isfooter==true) {
                                                    isfooter = false;
                                                    ftr = '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>';
                                                }
                                                else {
                                                    ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                                }
                                            }
                                            var strfooter = '';
                                            strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                            strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                            strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                            strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                            strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                            strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                            strfooter = strfooter.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                            strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                            strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                            strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                            strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                            strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                            strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                            strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                            if (ctnt!='')
                                                htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                            //else 
                                            //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                        }
                                    }
                    
                                    //htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                    
                                    var mailOptions = {
                                        from: noreply,
                                        to: special_recipients[i].email,//narendra.adinugraha@kiranamegatara.com
                                        subject: jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname,
                                        html: htm,
                                    };
                                    if (oktosend==true) {
                                        console.log('SPECIAL : ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                        if (special_recipients[i].email!='' && special_recipients[i].email!=null && special_recipients[i].email!=undefined)
                                        transport.sendMail(mailOptions, (error, info) => {
                                            if (error) {
                                            console.log(error);
                                            writeErrorLog('Error sending e-mail to ' + special_recipients[i].email,error);
                                            }
                                        });
                                    }
                                    else {
                                        console.log('empty special : ' + special_recipients[i].nama + ' ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                    }
                                }
                    
                                //NOTE: HR Recipient 
                                var rowsEmpFiltered =  [];
                                for(var i=0;i<jsonRes[z].ex_recipients.length;i++) {
                                    filterEmp.push(jsonRes[z].ex_recipients[i].nik);
                                }
                                if (jsonRes[z].HR_recipients.length>0) {
                                    var HR_emp  = [];

                                    for(var i=0;i<jsonRes[z].HR_recipients.length;i++) {
                                        for(var j=0;j<jsonRes[z].HR_recipients[i].length;j++) {
                                            HR_emp.push(jsonRes[z].HR_recipients[i][j]);
                                        }
                                    }
                                    if (filterEmp.length>0)
                                        rowsEmpFiltered = HR_emp.filter(row => !filterEmp.includes(row.nik));
                                    else 
                                        rowsEmpFiltered = HR_emp;
                                
                                    //console.log('sini ' + JSON.stringify(rowsEmpFiltered));
                                    rowsEmpFiltered = removeDuplicates(rowsEmpFiltered);
                                    for (var g=0;g<rowsEmpFiltered.length;g++) {
                                        //for(var i=0;i<rowsEmpFiltered[g].length;i++) {
                                            var htm = '';
                                            var oktosend = false;
                                            htm = '<head>' +
                                                    '<style>' +
                                                    'table {' +
                                                        'font-family: arial, sans-serif;' +
                                                        'font-size:14px;' +
                                                        'border-collapse: collapse;' +
                                                        'width: 100%;' +
                                                    '}' +
                                                    
                                                    'td, th {' +
                                                        'border: 1px solid #dddddd;' +
                                                        'padding: 8px;' +
                                                    '}' +
                                                    
                                                    'tr:nth-child(even) {' +
                                                        'background-color: #dddddd;' +
                                                    '}' +
                                                    
                                                
                                                    '</style>' +
                                                    '</head>'; 
                                                    htm = htm + '<b>Kepada Bapak/Ibu ' + rowsEmpFiltered[g].nama + '</b><p/>';
                                                    //htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                                            jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                                            for (var t=0;t<jsonRes[z].reports.length;t++) {
                                                if (rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=undefined) {
                                                    
                                                    var hdr = '';
                                                    if (jsonRes[z].reports[t].rows.length>0) {
                                                        var strheader = '';
                                                        strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                                        strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                                        strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                                        strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                                        strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                                        strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                                        strheader = strheader.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                                        strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                                        strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                                        strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                                        strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                                        strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                                        strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                                        strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                                        
                                                        strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                                        strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                                        strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                                        strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");
                                                        htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                                    
                                                        hdr = '<tr>'
                                                        for (var key in jsonRes[z].reports[t].rows[0]) {
                                                            var found = false;
                                                            var count = 0;
                                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                                    found = true;
                                                                    hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                                }
                                                                count++;
                                                            }
                                                            //sini plant 1
                                                            found = false;
                                                            count = 0;
                                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                                    found = true;
                                                                    if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                                    {
                                                                        if (jsonRes[z].reports[t].isplantvisible==0)
                                                                            hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                                    }
                                                                    else
                                                                        hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                                }
                                                                count++;
                                                            }
                                                        }
                                                        hdr = hdr + '</tr>'
                                                    }
                                        
                                                    var rows = jsonRes[z].reports[t].rows;
                                                    var rowsfiltered = rows;
                                                    //TODO: sini plant modifikasi
                                                    /*
                                                    if (jsonRes[z].reports[t].isperorganization==1) {
                                                        var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                        if (rowsEmpFiltered[g].ho!='n') {
                                                            var strpos = rowsEmpFiltered[g].posst + ' ' + rowsEmpFiltered[g].divisi + ' ' + rowsEmpFiltered[g].dept;
                                                            rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                                        }
                                                        else {
                                                            rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                                        }
                                                    }
                                                    else {
                                                        if (rowsEmpFiltered[g].ho=='n') {
                                                            var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                            rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                                        }
                                                    }
                                                    plantSelected.push('KMTR');
                                                    */
                                                    var ctnt = '';
                                                    var ftr = '';
                                                    var rw = '';
                                                    var isfooter = false;
                        
                                                    var checkfooter = false;
                                                    if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                                        checkfooter = true;
                                                    }
                                                    for (var h=0;h<rowsfiltered.length;h++) {
                                                        oktosend = true;
                                                        var bgcolor = "#F5F5F5";
                                                        rw = '';
                                                        if (checkfooter==true) {
                                                            for (var key in rowsfiltered[h]) {
                                                                var val = rowsfiltered[h][key];
                                                                var found = false;
                                                                var count = 0;
                                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                                    if (key==jsonRes[z].reports[t].isfooter) {
                                                                        if (val==1 || val=='1' || val==true) {
                                                                            bgcolor = "#2b471d";
                                                                            isfooter = true;
                                                                            found = true;
                                                                        }  
                                                                    }
                                                                    count++;
                                                                }
                                                            }
                                                        }
                                                        var targetfound = false;
                                                        var targetfound2 = false;
                                                        var target = 0;
                                                        var target2 = 0;
                                                        for (var key in rowsfiltered[h]) {
                                                            var val = rowsfiltered[h][key];
                                                            var valx = val;

                                                            var count = 0;
                                                        
                                                            while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                                    targetfound = true;
                                                                    target = valx;
                                                                }
                                                                count++;
                                                            } 

                                                            var count = 0;
                                                        
                                                            while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                                    targetfound2 = true;
                                                                    target2 = valx;
                                                                }
                                                                count++;
                                                            } 

                                                            var count = 0; 
                        
                                                            while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                                    targetfound3 = true;
                                                                    target3 = valx;
                                                                }
                                                                count++;
                                                            } 

                                                            var found = false;
                                                            var count = 0;
                                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                                    found = true;
                                                                    if (val!=null && val!=undefined) {
                                                                        switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                            case 0:
                                                                                val = val;
                                                                                break;
                                                                            case 1:
                                                                                val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 2:
                                                                                val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 3:
                                                                                val = helper.formatDateShort(val)
                                                                                break;
                                                                            case 4:
                                                                                val = helper.formatDate(val)
                                                                                break;
                                                                        }
                                                                    }
                                                                    var bgcolor2 = bgcolor;
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                        if (isfooter==false) 
                                                                            bgcolor2 = "#b5ddc3";
                                                                    var fontcolor = "<font>";
                                                                    if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                            if (valx>target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                            if (valx>target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                            if (valx>target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                        if (valx<0) fontcolor = "<font>";
                                                                    if (key!=jsonRes[z].reports[t].isfooter) {
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    }
                                                                }
                                                                count++;
                                                            }
                                                            //sini plant 2
                                                            found = false;
                                                            count = 0;
                                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                            {
                                                                if (key==jsonRes[z].reports[t].plantcolumn) {
                                                                    found = true;
                                                                    if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                                        if (jsonRes[z].reports[t].isplantvisible==0) {
                                                                            /*
                                                                            if (val!=null && val!=undefined) {
                                                                                switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                                    case 0:
                                                                                        val = val;
                                                                                        break;
                                                                                    case 1:
                                                                                        val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                        break;
                                                                                    case 2:
                                                                                        val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                        break;
                                                                                    case 3:
                                                                                        val = helper.formatDateShort(val)
                                                                                        break;
                                                                                    case 4:
                                                                                        val = helper.formatDate(val)
                                                                                        break;
                                                                                }
                                                                            }
                                                                            */
                                                                            var bgcolor2 = bgcolor;
                                                                            var fontcolor = "<font>";
                                                                            if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                                    if (valx>target) {
                                                                                        fontcolor = "<font>"
                                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                        //    fontcolor = "<font color='#009900'>";
                                                                                        //else
                                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    if (valx<target) {
                                                                                        fontcolor = "<font>"
                                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                        //    fontcolor = "<font color='#009900'>";
                                                                                        //else
                                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                                    }
                                                                                }
                                                                            if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                                    if (valx>target2) {
                                                                                        fontcolor = "<font>"
                                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                        //    fontcolor = "<font color='#009900'>";
                                                                                        //else
                                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    if (valx<target2) {
                                                                                        fontcolor = "<font>"
                                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                        //    fontcolor = "<font color='#009900'>";
                                                                                        //else
                                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                                    }
                                                                                }
                                                                            if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                                    if (valx>target3) {
                                                                                        fontcolor = "<font>"
                                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                        //    fontcolor = "<font color='#009900'>";
                                                                                        //else
                                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    if (valx<target3) {
                                                                                        fontcolor = "<font>"
                                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                        //    fontcolor = "<font color='#009900'>";
                                                                                        //else
                                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                                    }
                                                                                }
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                                if (valx<0) fontcolor = "<font>";
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                                if (isfooter==false) 
                                                                                    bgcolor2 = "#b5ddc3";
                                                                            if (key!=jsonRes[z].reports[t].isfooter) {
                                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                            }
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (val!=null && val!=undefined) {
                                                                            /*
                                                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                                case 0:
                                                                                    val = val;
                                                                                    break;
                                                                                case 1:
                                                                                    val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                    break;
                                                                                case 2:
                                                                                    val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                    break;
                                                                                case 3:
                                                                                    val = helper.formatDateShort(val)
                                                                                    break;
                                                                                case 4:
                                                                                    val = helper.formatDate(val)
                                                                                    break;
                                                                            }
                                                                            */
                                                                        }
                                                                        var bgcolor2 = bgcolor;
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                            if (isfooter==false) 
                                                                                bgcolor2 = "#b5ddc3";
                                                                        var fontcolor = "<font>";
                                                                        if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                                if (valx>target) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                if (valx<target) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                        if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                                if (valx>target2) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                if (valx<target2) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                        if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                                if (valx>target3) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                if (valx<target3) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                            if (valx<0) fontcolor = "<font>";
                                                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                        }
                                                                    }
                                                                }
                                                                count++;
                                                            } 
                                                        }
                                                        if (isfooter==true) {
                                                            isfooter = false;
                                                            ftr = '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>';
                                                        }
                                                        else {
                                                            ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                                        }
                                                    }
                                                    var strfooter = '';
                                                    strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                                    strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                                    strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                                    strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                                    strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                                    strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                                    strfooter = strfooter.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                                    strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                                    strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                                    strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                                    strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                                    strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                                    strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                                    strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                                    if (ctnt!='')
                                                        htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                                    //else 
                                                    //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                                }
                                            }
                                            //htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                                            var mailOptions = {
                                                from: noreply,
                                                to: rowsEmpFiltered[g].email,//narendra.adinugraha@kiranamegatara.com
                                                subject: jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname,
                                                html: htm,
                                            };
                                            //console.log('ok todent ' + oktosend + ' ' + JSON.stringify(mailOptions))

                                            if (oktosend==true) {
                                                console.log('HR : ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                                if (rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!=undefined)
                                                    transport.sendMail(mailOptions, (error, info) => {
                                                    if (error) {
                                                        console.log(error)
                                                        writeErrorLog('Error sending e-mail to ' + rowsEmpFiltered[g].email,error);
                                                    }
                                                });
                                            }
                                            else {
                                                console.log('empty HR : ' + rowsEmpFiltered[g].nama + ' ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                            }
                                        //}
                                    }
                                }
                            //}
                            //catch(err) {
                            //    writeErrorLog('step try catch', topiccode + ' - ' + topicname + ' : ' + err);
                            //}
                            //var MongoClient = mongo.MongoClient;
                            
                            //LOGs
                        
                            
                        }
                        //transport.close();
                    });
                });
            }
        });
       
    }

    function filterArray(array, filters) {
        const filterKeys = Object.keys(filters);
        return array.filter(item => {
          // validates all filter criteria
          return filterKeys.every(key => {
            // ignores non-function predicates
            if (typeof filters[key] !== 'function') return true;
            return filters[key](item[key]);
          });
        });
    }

    function removeDuplicates(arr) {
       return [...new Set(
            arr.map(el => JSON.stringify(el))
          )].map(e => JSON.parse(e));
    }

    /*
    const filters = {
        size: size => size === 50 || size === 70,
        color: color => ['blue', 'black'].includes(color.toLowerCase()),
        locations: locations => locations.find(x => ['JAPAN', 'USA'].includes(x.toUpperCase())),
        details: details => details.length < 30 && details.width >= 70,
      };
    */