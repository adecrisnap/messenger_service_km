var mongo = require('mongodb');
var url = require('../config/mgConfig.js');
var dbname = require('../config/dbPool');
var auth = require('../middlewares/auth.js');
var helper = require('../middlewares/helper');
var async = require('async');
const sql = require('mssql');
const Queue = require('bull');
const { all } = require('./index.js');
var ObjectId = require('mongodb').ObjectId;


var kpiOps = {
        /*
        addChildrenKPIS : function(req,res,next) {
            const {parent_kpi, kpis, level } = req.body
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    if (level==2) {
                                        async.eachSeries(kpis,function(obj,callback){
                                            var request = new sql.Request(pool);
                                            request.input('parent', sql.Int, parent_kpi)
                                            request.input('id', sql.Int, obj.id)
                                            var sqlx = 'update rpt.T_KPI_GROUP_2 set parent=@parent where id=@id'
                                            request.query(sqlx,function (err, recordsetx){
                                                if (err) {
                                                   return callback(err)
                                                }
                                                else {
                                                   return callback();
                                                }
                                            });
                                        },function(err){
                                            if (err) {
                                                console.log(err)
                                                pool.close();
                                                auth.setResponse(res,400,err);
                                                return;
                                            }
                                            else {
                                                pool.close();
                                                return res.status(200).json({message : 'ok'});
                                            }
                                        });
                                    }
                                    else {
                                        async.eachSeries(kpis,function(obj,callback){
                                            var request = new sql.Request(pool);
                                            request.input('parent', sql.Int, parent_kpi)
                                            request.input('id', sql.Int, obj.id)
                                            var sqlx = 'update rpt.T_KPI_GROUP_3 set parent=@parent where id=@id'
                                            request.query(sqlx,function (err, recordsetx){
                                                if (err) {
                                                   return callback(err)
                                                }
                                                else {
                                                   return callback();
                                                }
                                            });
                                        },function(err){
                                            if (err) {
                                                console.log(err)
                                                pool.close();
                                                auth.setResponse(res,400,err);
                                                return;
                                            }
                                            else {
                                                pool.close();
                                                return res.status(200).json({message : 'ok'});
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    })
                }
            })
        },
        */

        //KPI DIV: list of struktur organisasi parent child --> dapatkan semua kpi children
        getChildrenKPIS : function(req,res,next) {
            const {level, roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('roleid', sql.Int, roleid)
                                    var sqlx = null
                                    if (level==2) {
                                        sqlx = 'SELECT t.id,t.group_name,t.role_id,t2.role_name FROM [rpt].[T_KPI_GROUP_2] t ' +
                                        'inner join [rpt].T_KPI_ROLES t2 ' +
                                        'on t.role_id=t2.id ' +
                                        'inner join [rpt].T_KPI_ROLES t3 ' +
                                        'on t2.role_parent=t3.id ' +
                                        'where t3.id=@roleid'
                                    }
                                    else {
                                        sqlx = 'SELECT t.id,t.group_name,t.role_id,t2.role_name FROM [rpt].[T_KPI_GROUP_3] t ' +
                                        'inner join [rpt].T_KPI_ROLES t2 ' +
                                        'on t.role_id=t2.id ' +
                                        'inner join [rpt].T_KPI_ROLES t3 ' +
                                        'on t2.role_parent=t3.id ' +
                                        'where t3.id=@roleid'
                                    }
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []

                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   id : obj.id,
                                                   group_name : obj.group_name,
                                                   role_name : obj.role_name,

                                                }
                                               //console.log(JSON.stringify(jsonRes))
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }


                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        createRole : function(req,res,next) {
            const { 
                role_name,
                role_parent,
                is_ho,
                lvl
            } = req.body
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress, 
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        } 
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('role_name', sql.VarChar, role_name)
                                    request.input('role_parent', sql.Int, role_parent)
                                    request.input('is_ho', sql.Int, is_ho)
                                    request.input('lvl', sql.Int, lvl)
                                    var sqlx = 'insert into rpt.t_kpi_roles (role_name,role_parent,is_active,is_ho,lvl) values (@role_name,@role_parent,1,@is_ho,@lvl)'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json({message:'ok'})
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        editRole : function(req,res,next) {
            const id = req.params
            const { 
                role_name,
                role_parent,
                is_ho,
                lvl,
                is_active
            } = req.body
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress, 
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        } 
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('role_name', sql.VarChar, role_name)
                                    request.input('role_parent', sql.Int, role_parent)
                                    request.input('is_ho', sql.Int, is_ho)
                                    request.input('lvl', sql.Int, lvl)
                                    request.input('id', sql.Int, id)
                                    request.input('is_active', sql.Int, is_active)
                                    var sqlx = 'update rpt.t_kpi_roles set role_name=@role_name,role_parent=@role_parent,is_active=@is_active,is_ho=@is_ho,lvl=@lvl where id=@id'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json({message:'ok'})
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        getRolebyID : function(req,res,next) {
            const id = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress, 
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        } 
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id)
                                    var sqlx = 'select * from rpt.t_kpi_roles where id=@id'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })

        },


        /*
        deleteConfirmedChildrenKPIS : function(req,res,next) {
            const {id, level } = req.body
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    if (level==2) {
                                        request.input('id', sql.Int, id)
                                        var sqlx = 'update rpt.T_KPI_GROUP_2 set parent=null where id=@id'
                                        request.query(sqlx,function (err, recordsetx){
                                            if (err) {
                                                pool.close();
                                                auth.setResponse(res,400,err);
                                            }
                                            else {
                                                pool.close();
                                                return res.status(200).json({message:'ok'})
                                            }
                                        });

                                    }
                                    else {
                                        request.input('id', sql.Int, id)
                                        var sqlx = 'update rpt.T_KPI_GROUP_3 set parent=null where id=@id'
                                        request.query(sqlx,function (err, recordsetx){
                                            if (err) {
                                                pool.close();
                                                auth.setResponse(res,400,err);
                                            }
                                            else {
                                                pool.close();
                                                return res.status(200).json({message:'ok'})
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    })
                }
            })
        },
        */

        getConfirmedChildrenKPIS : function(req,res,next) {
            const {level, kpi } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('kpi', sql.Int,kpi)
                                    var sqlx = null
                                    if (level==2) {
                                        sqlx = 'SELECT t.id,t.group_name,t.role_id,t2.role_name FROM [rpt].[T_KPI_GROUP_2] t ' +
                                        'where t.parent=@kpi'
                                    }
                                    else {
                                        sqlx = 'SELECT t.id,t.group_name,t.role_id,t2.role_name FROM [rpt].[T_KPI_GROUP_3] t ' +
                                        'where t.parent=@kpi'
                                    }
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   id : obj.id,
                                                   group_name : obj.group_name,
                                                   role_name : obj.role_name,
                                                }
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        getAllRoles : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = 'select t1.*,t2.role_name role_parent_name from rpt.T_KPI_ROLES t1 left outer join rpt.T_KPI_ROLES t2 on t1.role_parent=t2.id where t1.is_active=1 order by t1.role_name';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   id : obj.id,
                                                   role_name : obj.role_name,
                                                   role_parent : obj.role_parent,
                                                   role_parent_name : obj.role_parent_name,
                                                   is_ho : obj.is_ho
                                                }
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        getRolesBySelection : function(req,res) {
            const {valhoplant, nik } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('valhoplant', sql.Int, valhoplant);
                                    request.input('nik', sql.VarChar, nik)
                                    var sqlx = 'select t1.id,t1.role_id,t1.is_active,t1.is_admin,t1.is_it,t2.role_name from rpt.T_KPI_ROLE_NIK t1 inner join rpt.T_KPI_ROLES  t2 on t1.role_id=t2.id where t1.is_active=1 and t2.is_active=1 and t1.nik=@nik and t2.is_ho=@valhoplant order by t2.role_name';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []

                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   id : obj.id,
                                                   role_id : obj.role_id,
                                                   role_name : obj.role_name,
                                                   is_admin : obj.is_admin,
                                                   is_it : obj.is_it
                                                }
                                               //console.log(JSON.stringify(jsonRes))
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }


                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        getRolesBySelectionDivDept : function(req,res) {

            const {valhoplant, rolesfinal, isdiv, divid } = req.body
            console.log('roles final ' + JSON.stringify(rolesfinal))
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            console.log(JSON.stringify(config))
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('valhoplant', sql.Int, valhoplant);
                                    var str = '('
                                    console.log(JSON.stringify(rolesfinal))
                                    for(var i=0;i<rolesfinal.length;i++){
                                        str+='role_mg_id=@role_mg_id' + '_' + i + ' or '
                                        request.input('role_mg_id_' + i, sql.VarChar,rolesfinal[i].value)
                                    }
                                    str = str.substring(0,str.length-3) + ')'
                                    var sqlx = ''
                                    if (isdiv==1){
                                        sqlx = 'select distinct t1.role_id,t1.is_active,t1.is_admin,t1.is_it,t2.role_name from rpt.T_KPI_ROLE_NIK t1 inner join rpt.T_KPI_ROLES  t2 on t1.role_id=t2.id where t1.is_active=1 ' +
                                        'and t2.lvl=2 and t2.is_active=1 and t2.is_ho=@valhoplant and ' + str + ' order by t2.role_name';
                                    }
                                    else {
                                        request.input('divid', sql.Int, divid);
                                        sqlx = 'select distinct t1.role_id,t1.is_active,t1.is_admin,t1.is_it,t2.role_name from rpt.T_KPI_ROLE_NIK t1 inner join rpt.T_KPI_ROLES  t2 on t1.role_id=t2.id where t1.is_active=1 ' +
                                        'and t2.lvl=3 and t2.role_parent=@divid and t2.is_active=1 and t2.is_ho=@valhoplant and ' + str + ' order by t2.role_name';
                                    }
                                    console.log(sqlx)
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            console.log(err)
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   //id : obj.id,
                                                   role_id : obj.role_id,
                                                   role_name : obj.role_name,
                                                   is_admin : obj.is_admin,
                                                   is_it : obj.is_it
                                                }
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }


                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        getRolesBySelection2 : function(req,res) {
            const {valhoplant, rolesfinal } = req.body
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            console.log(JSON.stringify(config))
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('valhoplant', sql.Int, valhoplant);
                                    //request.input('nik', sql.VarChar, nik)
                                    var str = '('
                                    console.log(JSON.stringify(rolesfinal))
                                    for(var i=0;i<rolesfinal.length;i++){
                                        str+='role_mg_id=@role_mg_id' + '_' + i + ' or '
                                        request.input('role_mg_id_' + i, sql.VarChar,rolesfinal[i].value)
                                    }
                                    //console.log(valhoplant + ' and ' + rolesfinal[0].value)
                                    str = str.substring(0,str.length-3) + ')'
                                    console.log(str)
                                    var sqlx = 'select distinct t1.role_id,t1.is_active,t1.is_admin,t1.is_it,t2.role_name from rpt.T_KPI_ROLE_NIK t1 inner join rpt.T_KPI_ROLES  t2 on t1.role_id=t2.id where t1.is_active=1 ' +
                                    'and t2.is_active=1 and t2.is_ho=@valhoplant and ' + str + ' order by t2.role_name';
                                    console.log(sqlx)
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            console.log(err)
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   //id : obj.id,
                                                   role_id : obj.role_id,
                                                   role_name : obj.role_name,
                                                   is_admin : obj.is_admin,
                                                   is_it : obj.is_it
                                                }
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }


                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            })
        },

        deleteRolesRoles : function(req,res) {
            const { id } = req.body

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                pool: {
                                    max: 10,
                                    min: 0,
                                    idleTimeoutMillis: 600000
                                    },
                                    options: {
                                    encrypt: false, // for azure
                                    trustServerCertificate: true // change to true for local dev / self-signed certs
                                    }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    var sqlx = 'update rpt.T_KPI_ROLE_NIK set is_active=0 where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json({message:'ok'});
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getRolesRoles : function(req,res) {
            const { roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                pool: {
                                    max: 10,
                                    min: 0,
                                    idleTimeoutMillis: 600000
                                    },
                                    options: {
                                    encrypt: false, // for azure
                                    trustServerCertificate: true // change to true for local dev / self-signed certs
                                    }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('role_id', sql.Int, roleid);
                                    var sqlx = 'select id,role_id,role_mg_id,role_mg_name,is_active,is_it,is_admin from rpt.T_KPI_ROLE_NIK where is_active=1 and role_id=@role_id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   id : obj.id,
                                                   role_id : obj.id,
                                                   role_mg_id : obj.role_mg_id,
                                                   is_active : obj.is_active,
                                                   is_it : obj.is_it,
                                                   is_admin : obj.is_admin,
                                                   role_mg_name : obj.role_mg_name
                                                }
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getRolesKPI : function(req,res) {

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                pool: {
                                    max: 10,
                                    min: 0,
                                    idleTimeoutMillis: 600000
                                    },
                                    options: {
                                    encrypt: false, // for azure
                                    trustServerCertificate: true // change to true for local dev / self-signed certs
                                    }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = 'select id,role_name,lvl from rpt.T_KPI_ROLES where is_active=1';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                var jsonRes = {
                                                   id : obj.id,
                                                   role_name : obj.role_name,
                                                   lvl : obj.lvl
                                                }
                                                jsonArr.push(jsonRes);
                                                return callback();
                                            },function(err){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    pool.close();
                                                    return res.status(200).json(jsonArr);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        setRolesRoles : function(req,res) {
            const { role_id,role_mg_id,is_it,is_admin,role_mg_name } = req.body

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                pool: {
                                    max: 10,
                                    min: 0,
                                    idleTimeoutMillis: 600000
                                    },
                                    options: {
                                    encrypt: false, // for azure
                                    trustServerCertificate: true // change to true for local dev / self-signed certs
                                    }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('role_id', sql.Int, role_id);
                                    request.input('is_admin', sql.Int, is_admin);
                                    request.input('is_it', sql.Int, is_it);
                                    request.input('role_mg_id', sql.VarChar, role_mg_id);
                                    request.input('role_mg_name', sql.VarChar, role_mg_name);
                                    var sqlx = 'insert into rpt.T_KPI_ROLE_NIK (role_id,role_mg_id,role_mg_name,is_active,is_admin,is_it) values (@role_id,@role_mg_id,@role_mg_name,1,@is_admin,@is_it)';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json({message:'ok'});
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },


    //MASTER
        calculateKPI : function(req,res) {
            const {
                bulan,
                tahun,
                role_id
            } = req.body;
            //FIXME:
            console.log(tahun + ' ' + bulan)
            var tempbln = null
            if (bulan==12)
                tempbln = 1
            else
                tempbln = bulan+1
            var dt1 = new Date(tahun + '-' + bulan + '-1');
            var dt2 = addDays(new Date(tahun + '-' + tempbln + '-1'),-1)
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log(err)
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            console.log(err)
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                    pool: {
                                        max: 10,
                                        min: 0,
                                        idleTimeoutMillis: 600000
                                        },
                                        options: {
                                            encrypt: false, // for azure
                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                            connectTimeout : 600000
                                        }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = 'select * from rpt.T_KPI_GROUP_2 where is_active=1 order by sequence';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            console.log(err)
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var jsonArr = []
                                            async.eachSeries(recordsetx.recordsets[0], function(obj,callback){
                                                var jsonRes = {
                                                    config : {
                                                        user: username,
                                                        password: pwd,
                                                        server: serveraddress,
                                                        database: dbname,
                                                        port : 1433,
                                                        pool: {
                                                                max: 10,
                                                                min: 0,
                                                                idleTimeoutMillis: 600000
                                                            },
                                                            options: {
                                                                encrypt: false, // for azure
                                                                trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                                connectTimeout : 600000

                                                            }
                                                    },
                                                    level : 2,
                                                    kpiid : obj.id,
                                                    group_code : obj.group_code,
                                                    group_name : obj.group_name,
                                                    sp_name : obj.sp_name,
                                                    date1 : dt1,
                                                    date2 : dt2
                                                }
                                                if (obj.sp_name!=null && obj.sp_name!='') jsonArr.push(jsonRes)

                                                return callback();
                                            },function(err) {
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    jsonArr.push({
                                                        config : {
                                                            user: username,
                                                            password: pwd,
                                                            server: serveraddress,
                                                            database: dbname,
                                                            port : 1433,
                                                            pool: {
                                                                    max: 10,
                                                                    min: 0,
                                                                    idleTimeoutMillis: 600000
                                                                },
                                                                options: {
                                                                    encrypt: false, // for azure
                                                                    trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                                    connectTimeout : 600000

                                                                }
                                                        },
                                                        level : 2,
                                                        kpiid : '',
                                                        group_code : '',
                                                        group_name : '',
                                                        sp_name : 'rpt.sp_KPI_Global_2',
                                                        date1 : dt1,
                                                        date2 : dt2,
                                                        role_id : role_id
                                                    })
                                                    var request = new sql.Request(pool);
                                                    var sqlx = 'select * from rpt.T_KPI_GROUP_3 where is_active=1 order by sequence';
                                                    request.query(sqlx,function (err, recordsetx){
                                                        if (err) {
                                                            console.log(err)
                                                            pool.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                                var jsonRes = {
                                                                    config : {
                                                                        user: username,
                                                                        password: pwd,
                                                                        server: serveraddress,
                                                                        database: dbname,
                                                                        port : 1433,
                                                                        pool: {
                                                                                max: 10,
                                                                                min: 0,
                                                                                idleTimeoutMillis: 600000
                                                                            },
                                                                            options: {
                                                                                encrypt: false, // for azure
                                                                                trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                                                connectTimeout : 600000

                                                                            }
                                                                    },
                                                                    level : 3,
                                                                    kpiid : obj.id,
                                                                    group_code : obj.group_code,
                                                                    group_name : obj.group_name,
                                                                    sp_name : obj.sp_name,
                                                                    date1 : dt1,
                                                                    date2 : dt2
                                                                }
                                                                if (obj.sp_name!=null && obj.sp_name!='') jsonArr.push(jsonRes)

                                                                return callback();
                                                            },function(err){
                                                                if (err) {
                                                                    console.log(err)
                                                                    pool.close();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                }
                                                                else {
                                                                    jsonArr.push({
                                                                        config : {
                                                                            user: username,
                                                                            password: pwd,
                                                                            server: serveraddress,
                                                                            database: dbname,
                                                                            port : 1433,
                                                                            pool: {
                                                                                    max: 10,
                                                                                    min: 0,
                                                                                    idleTimeoutMillis: 600000
                                                                                },
                                                                                options: {
                                                                                    encrypt: false, // for azure
                                                                                    trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                                                    connectTimeout : 600000

                                                                                }
                                                                        },
                                                                        level : 3,
                                                                        kpiid : '',
                                                                        group_code : '',
                                                                        group_name : '',
                                                                        sp_name : 'rpt.sp_KPI_Global_3',
                                                                        date1 : dt1,
                                                                        date2 : dt2,
                                                                        role_id : role_id
                                                                    })
                                                                    async.eachSeries(jsonArr,function(obj,callback){
                                                                        executeKPIQueue.add(obj);
                                                                        return callback();
                                                                    },function(err){
                                                                        if (err) {
                                                                            console.log(err)
                                                                            pool.close();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        }
                                                                        else {
                                                                            pool.close();
                                                                            return res.status(200).json('success');
                                                                        }
                                                                    })
                                                                }
                                                            })

                                                        }
                                                    });
                                                }
                                            })

                                        }
                                    })
                                }
                            });

                        }
                    });
                }
            });
        },

        /*
        calculateKPI : function(req,res) {
            return res.status(200).json('success');
        },


        getKPICalculationStatus : function(req,res) {
            return res.status(200).json({message :'success'});
        },
        */

        setKPIServer : function(req,res) {
            var kpiserver = req.body.kpiserver
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { kpiserver: new ObjectId(kpiserver) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var myobj = {
                                lastmodified : new Date(),
                                kpiserver : new ObjectId(kpiserver),
                            };
                            if (result.length>0) {
                                dbo.collection("t_topics").updateOne({_id : new ObjectId(result[0]._id)}, myobj, function(err, result2) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json('success');
                                    }
                                });
                            }
                            else {
                                dbo.collection("t_kpi_server").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json('success');
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        getKPIServer : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //KPI GROUP 1
        createKPIGroup1 : function(req,res) {
            const {
                group_code,
                group_name,
                sequence,
                is_active,
                roleid
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('group_code', sql.VarChar, group_code);
                                    request.input('group_name', sql.VarChar, group_name);
                                    request.input('sequence', sql.Int, sequence);
                                    request.input('is_active', sql.Int, is_active);
                                    request.input('role_id', sql.Int, roleid);
                                    request.input('lastmodified', sql.DateTime, new Date())
                                    var sqlx = 'insert into rpt.T_KPI_GROUP_1 (role_id,group_code,group_name,sequence,is_active,lastmodified) values (@role_id,@group_code,@group_name,@sequence,@is_active,@lastmodified)';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json({message : 'ok'})
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        editKPIGroup1 : function(req,res) {
            const { id } = req.params;
            const {
                group_code,
                group_name,
                sequence,
                is_active,
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    request.input('group_code', sql.VarChar, group_code);
                                    request.input('group_name', sql.VarChar, group_name);
                                    request.input('sequence', sql.Int, sequence);
                                    request.input('is_active', sql.Int, is_active);
                                    request.input('lastmodified', sql.DateTime, new Date())
                                    var sqlx = 'update rpt.T_KPI_GROUP_1 set is_active=@is_active,lastmodified=@lastmodified,group_code=@group_code,group_name=@group_name,sequence=@sequence where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json({message : 'ok'})
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPIGroups1 : function(req,res) {
            const { is_active, roleid } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = ''
                                    if (is_active==-1) {
                                        request.input('role_id', sql.Int, roleid);
                                        sqlx = 'select * from rpt.T_KPI_GROUP_1 where role_id=@role_id order by sequence';
                                    }
                                    else {
                                        request.input('is_active', sql.Int, is_active);
                                        request.input('role_id', sql.Int, roleid);
                                        sqlx = 'select * from rpt.T_KPI_GROUP_1 where is_active=@is_active and role_id=@role_id order by sequence';
                                    }
                                    console.log('roele ' + roleid)
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            console.log(sqlx + ' ' + is_active)
                                            pool.close();
                                            console.log(JSON.stringify(recordsetx.recordsets[0]))
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPIGroup1byID : function(req,res) {
            const { id } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    var sqlx = 'select * from rpt.T_KPI_GROUP_1 where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        //KPI GROUP 2
        createKPIGroup2 : function(req,res) {
            const {
                group_code,
                group_name,
                sequence,
                id_kpi_group_1,
                is_active,
                sp_name,
                max_ach,
                kpi_type,
                ytd_measurement,
                weight,
                roleid,
                childrenkpis,
                kpi_desc,
                kpi_formula
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('group_code', sql.VarChar, group_code);
                                    request.input('group_name', sql.VarChar, group_name);
                                    request.input('sequence', sql.Int, sequence);
                                    request.input('is_active', sql.Int, is_active);
                                    request.input('sp_name', sql.VarChar, sp_name);
                                    request.input('id_kpi_group_1', sql.Int, id_kpi_group_1);
                                    request.input('lastmodified', sql.DateTime, new Date())
                                    request.input('max_ach', sql.Int,max_ach)
                                    request.input('kpi_type', sql.Int, kpi_type)
                                    request.input('ytd_measurement', sql.Int, ytd_measurement)
                                    request.input('weight', sql.Numeric(18,2), weight)
                                    request.input('role_id', sql.Int, roleid);
                                    request.input('childrenkpis', sql.VarChar, childrenkpis);
                                    request.input('kpi_desc', sql.VarChar, kpi_desc);
                                    request.input('kpi_formula', sql.VarChar, kpi_formula);
                                    var sqlx = 'insert into rpt.T_KPI_GROUP_2 (kpi_desc,kpi_formula,childrenkpis,role_id,weight,max_ach,kpi_type,ytd_measurement,is_active,sp_name,id_kpi_group_1,group_code,group_name,sequence,lastmodified) values (@kpi_desc,@kpi_formula,@childrenkpis,@role_id,@weight,@max_ach,@kpi_type,@ytd_measurement,@is_active,@sp_name,@id_kpi_group_1,@group_code,@group_name,@sequence,@lastmodified)';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request(pool);
                                            request.input('group_code', sql.VarChar, group_code);
                                            request.query('delete from rpt.T_KPI_GROUP_2_CHILDREN where parent_id=@group_code', function(err,recordsetx){
                                                if (err) {
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var spt = childrenkpis//.split(',')
                                                    async.eachSeries(spt,function(obj,callback){
                                                        var request = new sql.Request(pool);
                                                        request.input('group_code', sql.VarChar, group_code);
                                                        request.input('child_id', sql.VarChar, obj);
                                                        request.query('insert into rpt.T_KPI_GROUP_2_CHILDREN (parent_id,child_id) values(@group_code,@child_id)', function(err,recordsetx){
                                                            if (err) {
                                                                return callback(err)
                                                            }
                                                            else {
                                                                return callback()
                                                            }
                                                        })
                                                    },function(err){
                                                        if (err) {
                                                            pool.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            pool.close();
                                                            return res.status(200).json({message : 'ok'})
                                                        }
                                                    })
                                                }
                                            })

                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        editKPIGroup2 : function(req,res) {
            const { id } = req.params;
            const {
                group_code,
                group_name,
                sequence,
                is_active,
                id_kpi_group_1,
                sp_name,
                max_ach,
                kpi_type,
                ytd_measurement,
                weight,
                childrenkpis,
                kpi_desc,
                kpi_formula
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    request.input('group_code', sql.VarChar, group_code);
                                    request.input('group_name', sql.VarChar, group_name);
                                    request.input('sequence', sql.Int, sequence);
                                    request.input('is_active', sql.Int, is_active);
                                    request.input('id_kpi_group_1', sql.Int, id_kpi_group_1);
                                    request.input('sp_name', sql.VarChar, sp_name);
                                    request.input('lastmodified', sql.DateTime, new Date())
                                    request.input('max_ach', sql.Int,max_ach)
                                    request.input('kpi_type', sql.Int, kpi_type)
                                    request.input('ytd_measurement', sql.Int, ytd_measurement)
                                    request.input('weight', sql.Numeric(18,2), weight)
                                    request.input('childrenkpis', sql.VarChar, childrenkpis);
                                    request.input('kpi_desc', sql.VarChar, kpi_desc);
                                    request.input('kpi_formula', sql.VarChar, kpi_formula);
                                    var sqlx = 'update rpt.T_KPI_GROUP_2 set kpi_desc=@kpi_desc,kpi_formula=@kpi_formula,childrenkpis=@childrenkpis,weight=@weight,max_ach=@max_ach,kpi_type=@kpi_type,ytd_measurement=@ytd_measurement,lastmodified=@lastmodified,id_kpi_group_1=@id_kpi_group_1,sp_name=@sp_name,is_active=@is_active,group_code=@group_code,group_name=@group_name,sequence=@sequence where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request(pool);
                                            request.input('group_code', sql.VarChar, group_code);
                                            request.query('delete from rpt.T_KPI_GROUP_2_CHILDREN where parent_id=@group_code', function(err,recordsetx){
                                                if (err) {
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    console.log(childrenkpis)
                                                    var spt = childrenkpis//.split(',')
                                                    async.eachSeries(spt,function(obj,callback){
                                                        var request = new sql.Request(pool);
                                                        request.input('group_code', sql.VarChar, group_code);
                                                        request.input('child_id', sql.VarChar, obj);
                                                        request.query('insert into rpt.T_KPI_GROUP_2_CHILDREN (parent_id,child_id) values(@group_code,@child_id)', function(err,recordsetx){
                                                            if (err) {
                                                                return callback(err)
                                                            }
                                                            else {
                                                                return callback()
                                                            }
                                                        })
                                                    },function(err){
                                                        if (err) {
                                                            pool.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            pool.close();
                                                            return res.status(200).json({message : 'ok'})
                                                        }
                                                    })
                                                }
                                            })

                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPIGroups2 : function(req,res) {
            const { is_active,roleid } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = ''
                                    if (is_active==-1) {
                                        request.input('role_id', sql.Int, roleid);
                                        sqlx = 'select t1.*,t2.group_name kpi1,t2.sequence seq1,t1.sequence seq2 from rpt.T_KPI_GROUP_2 t1 inner join rpt.T_KPI_GROUP_1 t2 on t1.id_kpi_group_1=t2.id where t1.role_id=@role_id order by t2.sequence,t1.sequence';
                                    }
                                    else {
                                        request.input('is_active', sql.Int, is_active);
                                        request.input('role_id', sql.Int, roleid);
                                        sqlx = 'select t1.*,t2.group_name kpi1,t2.sequence seq1,t1.sequence seq2 from rpt.T_KPI_GROUP_2 t1 inner join rpt.T_KPI_GROUP_1 t2 on t1.id_kpi_group_1=t2.id where t1.is_active=@is_active and t1.role_id=@role_id order by t2.sequence,t1.sequence';
                                    }
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPIGroup2byID : function(req,res) {
            const { id } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    var sqlx = 'select * from rpt.T_KPI_GROUP_2 where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        //KPI GROUP 3
        createKPIGroup3 : function(req,res) {
            const {
                group_code,
                group_name,
                sequence,
                id_kpi_group_2,
                is_active,
                sp_name,
                max_ach,
                kpi_type,
                ytd_measurement,
                weight,
                roleid,
                childrenkpis,
                kpi_desc,
                kpi_formula
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            console.log(err)
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    console.log(err)
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('group_code', sql.VarChar, group_code);
                                    request.input('group_name', sql.VarChar, group_name);
                                    request.input('sequence', sql.Int, sequence);
                                    request.input('is_active', sql.Int, is_active);
                                    request.input('sp_name', sql.VarChar, sp_name);
                                    request.input('lastmodified', sql.DateTime, new Date())
                                    request.input('id_kpi_group_2', sql.Int, id_kpi_group_2);
                                    request.input('max_ach', sql.Int,max_ach)
                                    request.input('kpi_type', sql.Int, kpi_type)
                                    request.input('ytd_measurement', sql.Int, ytd_measurement)
                                    request.input('weight', sql.Numeric(18,2), weight)
                                    request.input('role_id', sql.Int, roleid);
                                    request.input('childrenkpis', sql.VarChar, childrenkpis);
                                    request.input('kpi_desc', sql.VarChar, kpi_desc);
                                    request.input('kpi_formula', sql.VarChar, kpi_formula);
                                    var sqlx = 'insert into rpt.T_KPI_GROUP_3 (kpi_desc,kpi_formula,childrenkpis,role_id,weight,max_ach,kpi_type,ytd_measurement,is_active,sp_name,id_kpi_group_2,group_code,group_name,sequence,lastmodified) values (@kpi_desc,@kpi_formula,@childrenkpis,@role_id,@weight,@max_ach,@kpi_type,@ytd_measurement,@is_active,@sp_name,@id_kpi_group_2,@group_code,@group_name,@sequence,@lastmodified)';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request(pool);
                                            request.input('group_code', sql.VarChar, group_code);
                                            request.query('delete from rpt.T_KPI_GROUP_3_CHILDREN where parent_id=@group_code', function(err,recordsetx){
                                                if (err) {
                                                    pool.close();
                                                    console.log(err)
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var spt = childrenkpis//.split(',')
                                                    async.eachSeries(spt,function(obj,callback){
                                                        var request = new sql.Request(pool);
                                                        request.input('group_code', sql.VarChar, group_code);
                                                        request.input('child_id', sql.VarChar, obj);
                                                        request.query('insert into rpt.T_KPI_GROUP_3_CHILDREN (parent_id,child_id) values(@group_code,@child_id)', function(err,recordsetx){
                                                            if (err) {
                                                                return callback(err)
                                                            }
                                                            else {
                                                                return callback()
                                                            }
                                                        })
                                                    },function(err){
                                                        if (err) {
                                                            console.log(err)
                                                            pool.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            pool.close();
                                                            return res.status(200).json({message : 'ok'})
                                                        }
                                                    })
                                                }
                                            })

                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        editKPIGroup3 : function(req,res) {
            const { id } = req.params;
            const {
                group_code,
                group_name,
                sequence,
                is_active,
                id_kpi_group_2,
                sp_name,
                max_ach,
                kpi_type,
                ytd_measurement,
                weight,
                childrenkpis,
                kpi_desc,
                kpi_formula
            } = req.body;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    request.input('group_code', sql.VarChar, group_code);
                                    request.input('group_name', sql.VarChar, group_name);
                                    request.input('sequence', sql.Int, sequence);
                                    request.input('is_active', sql.Int, is_active);
                                    request.input('sp_name', sql.VarChar, sp_name);
                                    request.input('lastmodified', sql.DateTime, new Date())
                                    request.input('id_kpi_group_2', sql.Int, id_kpi_group_2);
                                    request.input('max_ach', sql.Int,max_ach)
                                    request.input('kpi_type', sql.Int, kpi_type)
                                    request.input('ytd_measurement', sql.Int, ytd_measurement)
                                    request.input('weight', sql.Numeric(18,2), weight)
                                    request.input('childrenkpis', sql.VarChar, childrenkpis);
                                    request.input('kpi_desc', sql.VarChar, kpi_desc);
                                    request.input('kpi_formula', sql.VarChar, kpi_formula);
                                    var sqlx = 'update rpt.T_KPI_GROUP_3 set kpi_desc=@kpi_desc,kpi_formula=@kpi_formula,childrenkpis=@childrenkpis,weight=@weight,max_ach=@max_ach,kpi_type=@kpi_type,ytd_measurement=@ytd_measurement,lastmodified=@lastmodified,id_kpi_group_2=@id_kpi_group_2,sp_name=@sp_name,is_active=@is_active,group_code=@group_code,group_name=@group_name,sequence=@sequence where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request(pool);
                                            request.input('group_code', sql.VarChar, group_code);
                                            request.query('delete from rpt.T_KPI_GROUP_2_CHILDREN where parent_id=@group_code', function(err,recordsetx){
                                                if (err) {
                                                    pool.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    var spt = childrenkpis//.split(',')
                                                    async.eachSeries(spt,function(obj,callback){
                                                        var request = new sql.Request(pool);
                                                        request.input('group_code', sql.VarChar, group_code);
                                                        request.input('child_id', sql.VarChar, obj);
                                                        request.query('insert into rpt.T_KPI_GROUP_2_CHILDREN (parent_id,child_id) values(@group_code,@child_id)', function(err,recordsetx){
                                                            if (err) {
                                                                return callback(err)
                                                            }
                                                            else {
                                                                return callback()
                                                            }
                                                        })
                                                    },function(err){
                                                        if (err) {
                                                            pool.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            pool.close();
                                                            return res.status(200).json({message : 'ok'})
                                                        }
                                                    })
                                                }
                                            })

                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPIGroups3 : function(req,res) {
            const { is_active, roleid } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = ''
                                    if (is_active==-1) {
                                        request.input('role_id', sql.Int, roleid);
                                        sqlx = 'select t1.*,t2.group_name kpi2,t3.group_name kpi1 from rpt.T_KPI_GROUP_3 t1 inner join rpt.T_KPI_GROUP_2 t2 on t1.id_kpi_group_2=t2.id inner join rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id where t1.role_id=@role_id order by t3.sequence,t2.sequence,t1.sequence';
                                    }
                                    else {
                                        request.input('role_id', sql.Int, roleid);
                                        request.input('is_active', sql.Int, is_active);
                                        sqlx = 'select t1.*,t2.group_name kpi2,t3.group_name kpi1 from rpt.T_KPI_GROUP_3 t1 inner join rpt.T_KPI_GROUP_2 t2 on t1.id_kpi_group_2=t2.id inner join rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id where t1.is_active=@is_active and t1.role_id=@role_id order by t3.sequence,t2.sequence,t1.sequence';
                                    }
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPIGroup3byID : function(req,res) {
            const { id } = req.params;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('id', sql.Int, id);
                                    var sqlx = 'select * from rpt.T_KPI_GROUP_3 where id=@id';
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },


        //konfigurasi
        getConfigurationByID : function(req,res) {
            const { roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var sqlx = ''
                                        request.input('role_id', sql.Int, roleid);
                                        request.input('tahun1', sql.Int,new Date().getFullYear())
                                        request.input('tahun2', sql.Int,new Date().getFullYear()+1)
                                        sqlx = 'select t.* from (' +
                                        'select 2 level,t2.sequence,t3.group_name parent,t2.group_name,t1.* from rpt.T_KPI_GROUP_2 t2 left outer join rpt.T_KPI_GROUP_2_tahun t1 on t1.id_kpi_group_2=t2.id left outer join rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id where t2.is_active=1 and t2.role_id=@role_id and (t1.tahun=@tahun1 or t1.tahun=@tahun2) and t1.role_id=@role_id ' +
                                        'union all ' +
                                        'select 3 level,t2.sequence,t3.group_name parent,t2.group_name,t1.* from rpt.T_KPI_GROUP_3 t2 left outer join rpt.T_KPI_GROUP_3_tahun t1 on t1.id_kpi_group_3=t2.id left outer join rpt.T_KPI_GROUP_2 t3 on t2.id_kpi_group_2=t3.id where t2.is_active=1 and t2.role_id=@role_id and (t1.tahun=@tahun1 or t1.tahun=@tahun2) and t1.role_id=@role_id ' +
                                        ') t order by t.level,t.sequence'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getKPISInLevel : function(req,res) {
            var { level,roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('level', sql.Int, level);
                                    request.input('role_id', sql.Int, roleid);
                                    var sqlx = ''

                                        sqlx = 'select * from (' +
                                        'select 2 level,t2.sequence,t3.group_name parent,t2.group_name,t2.id from rpt.T_KPI_GROUP_2 t2 left outer join rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id where t2.is_active=1 and t2.role_id=@role_id ' +
                                        'union all ' +
                                        'select 3 level,t2.sequence,t3.group_name parent,t2.group_name,t2.id from rpt.T_KPI_GROUP_3 t2 left outer join rpt.T_KPI_GROUP_2 t3 on t2.id_kpi_group_2=t3.id where t2.is_active=1 and t2.role_id=@role_id ' +
                                        ') t where level=@level'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getConfigurationByTahun : function(req,res) {
            const { tahun,roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('role_id', sql.Int, roleid);
                                    var sqlx = ''

                                    sqlx =
                                        'select ' + tahun + ' as tahun,' +
                                        't2.id as id_kpi_group,t2.group_name,2 level,t2.max_ach,t1.weight,t2.kpi_type,t2.ytd_measurement from rpt.T_KPI_GROUP_2 t2 left outer join rpt.T_KPI_GROUP_2_TAHUN t1 on t2.id=t1.id_kpi_group_2 where t2.is_active=1 and t2.role_id=@role_id and t1.role_id=@role_id ' +
                                        'union all ' +
                                        'select ' + tahun + ' as tahun,' +
                                        't2.id as id_kpi_group,t2.group_name,3 level,t2.max_ach,t1.weight,t2.kpi_type,t2.ytd_measurement from rpt.T_KPI_GROUP_3 t2 left outer join rpt.T_KPI_GROUP_3_TAHUN t1 on t2.id=t1.id_kpi_group_3 where t2.is_active=1 and t2.role_id=@role_id and t1.role_id=@role_id '
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        copyConfigurationByTahun : function(req,res) {
            const { tahun,roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    console.log('halo ' + err)
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('tahun',sql.Int,tahun)
                                    request.input('role_id',sql.Int,roleid)
                                    console.log(tahun + ' ' + roleid)
                                    request.execute('rpt.sp_KPI_copyConfigurationByTahun', function (erry, recordset) {
                                        if (erry) {
                                            console.log('halo222 ' + err)
                                            pool.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json('success')
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        uploadConfiguration : function(req,res) {
            const { data } = req.body
            console.log(JSON.stringify(data))
            var bobot = 0;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    console.log(err)
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            console.log(err)
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    console.log(err)
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    async.eachSeries(data,function(obj,callback){
                                        if (obj.level==2) {
                                            var request = new sql.Request(pool);
                                            request.input('id_kpi_group_2', sql.Int, obj.id_kpi_group);
                                            request.input('tahun', sql.Int, obj.tahun);
                                            request.input('role_id', sql.Int, obj.roleid);
                                            var sqlx = 'select * from rpt.T_KPI_GROUP_2_TAHUN where tahun=@tahun and id_kpi_group_2=@id_kpi_group_2 and role_id=@role_id'
                                            request.query(sqlx, function (err, recordset) {
                                                if (err) {
                                                    pool.close();
                                                    console.log(err)
                                                    return callback(err)
                                                }
                                                else {
                                                    if (recordset.recordsets[0].length>0) {
                                                        var request = new sql.Request(pool);
                                                        bobot += obj.weight;
                                                        request.input('id_kpi_group_2',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('max_ach',sql.Numeric(18,2),obj.max_ach);
                                                        request.input('weight',sql.Numeric(18,2),obj.weight);
                                                        request.input('kpi_type',sql.Int,obj.kpi_type);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        var sqlx = 'update rpt.T_KPI_GROUP_2_TAHUN set role_id=@role_id,max_ach=@max_ach,weight=@weight,kpi_type=@kpi_type,lastmodified=@lastmodified where id_kpi_group_2=@id_kpi_group_2 and tahun=@tahun'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                    else {
                                                        bobot+=obj.weight;
                                                        var request = new sql.Request(pool);
                                                        request.input('id_kpi_group_2',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('max_ach',sql.Numeric(18,2),obj.max_ach);
                                                        request.input('weight',sql.Numeric(18,2),obj.weight);
                                                        request.input('kpi_type',sql.Int,obj.kpi_type);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        var sqlx = 'insert into rpt.T_KPI_GROUP_2_TAHUN (role_id,id_kpi_group_2,tahun,max_ach,weight,kpi_type,lastmodified) values (@role_id,@id_kpi_group_2,@tahun,@max_ach,@weight,@kpi_type,@lastmodified)'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            var request = new sql.Request(pool);
                                            request.input('id_kpi_group_3', sql.Int, obj.id_kpi_group);
                                            request.input('tahun', sql.Int, obj.tahun);
                                            request.input('role_id', sql.Int, obj.roleid);
                                            var sqlx = 'select * from rpt.T_KPI_GROUP_3_TAHUN where tahun=@tahun and id_kpi_group_3=@id_kpi_group_3 and role_id=@role_id'
                                            request.query(sqlx, function (err, recordset) {
                                                if (err) {
                                                    pool.close();
                                                    console.log(err)
                                                    return callback(err)
                                                }
                                                else {
                                                    if (recordset.recordsets[0].length>0) {
                                                        var request = new sql.Request(pool);
                                                        request.input('id_kpi_group_3',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('max_ach',sql.Numeric(18,2),obj.max_ach);
                                                        request.input('weight',sql.Numeric(18,2),obj.weight);
                                                        request.input('kpi_type',sql.Int,obj.kpi_type);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        var sqlx = 'update rpt.T_KPI_GROUP_3_TAHUN set role_id=@role_id,max_ach=@max_ach,weight=@weight,kpi_type=@kpi_type,lastmodified=@lastmodified where id_kpi_group_3=@id_kpi_group_3 and tahun=@tahun'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                    else {
                                                        var request = new sql.Request(pool);
                                                        request.input('id_kpi_group_3',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('max_ach',sql.Numeric(18,2),obj.max_ach);
                                                        request.input('weight',sql.Numeric(18,2),obj.weight);
                                                        request.input('kpi_type',sql.Int,obj.kpi_type);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        var sqlx = 'insert into rpt.T_KPI_GROUP_3_TAHUN (role_id,id_kpi_group_3,tahun,max_ach,weight,kpi_type,lastmodified) values (@role_id,@id_kpi_group_3,@tahun,@max_ach,@weight,@kpi_type,@lastmodified)'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        }
                                    },function(err){
                                        if (err) {
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            return res.status(200).json({hasil :'success', bobot : bobot});
                                        }
                                    })

                                }
                            });
                        }
                    });
                }
            });
        },

        //target
        getTargetByID : function(req,res) {
            const { roleid } = req.params
            console.log(roleid)
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    var tahun = new Date().getFullYear()
                                    request.input('tahun1', sql.Int, tahun)
                                    request.input('tahun2', sql.Int, tahun+1)
                                    request.input('role_id', sql.Int, roleid);
                                    var sqlx = ''

                                        sqlx = 'select * from ( ' +
                                            'select ' +
                                                '2 level,' +
                                                'v.name_coy,' +
                                                't2.sequence,' +
                                                't2.group_name,' +
                                                't3.group_name parent,t3.sequence seq,t1.* ' +
                                            'from ' +
                                                'rpt.T_KPI_GROUP_2 t2 left outer join ' +
                                                'rpt.T_KPI_GROUP_2_SCORE t1 on t2.id=t1.id_kpi_group_2 left outer join ' +
                                                'rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id full join ' +
                                                'rpt.vw_PlantRegion v on v.name_coy=t1.plant collate SQL_Latin1_General_CP1_CI_AS full join ' +
                                                'rpt.vw_bulan vx on vx.bulan=t1.bulan ' +
                                            'where t2.is_active=1 and (t1.tahun=@tahun1 or t1.tahun=@tahun2) ' +
                                            'and t2.role_id=@role_id ' +
                                            'union all ' +
                                            'select ' +
                                                '3 level,' +
                                                'v.name_coy,' +
                                                't2.sequence,' +
                                                't2.group_name,' +
                                                't3.group_name parent,' +
                                                't3.sequence seq,' +
                                                't1.* ' +
                                            'from ' +
                                                'rpt.T_KPI_GROUP_3 t2 left outer join ' +
                                                'rpt.T_KPI_GROUP_3_SCORE t1 on t2.id=t1.id_kpi_group_3 left outer join ' +
                                                'rpt.T_KPI_GROUP_2 t3 on t2.id_kpi_group_2=t3.id full join ' +
                                                'rpt.vw_PlantRegion v on v.name_coy=t1.plant collate SQL_Latin1_General_CP1_CI_AS full join ' +
                                                'rpt.vw_bulan vx on vx.bulan=t1.bulan ' +
                                            'where t2.is_active=1 and (t1.tahun=@tahun1 or t1.tahun=@tahun2) and t2.role_id=@role_id ' +
                                            ') t order by name_coy,tahun,bulan,level,seq,sequence'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getTargetByTahun : function(req,res) {
            const { tahun, plant, roleid } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('role_id', sql.Int, roleid);
                                    var sqlx = ''

                                        sqlx = 'select distinct * from ( ' +
                                            'select ' +
                                                tahun + ' tahun,' +
                                                'vx.bulan bln,' +
                                                '2 level,' +
                                                'v.name_coy plant,' +
                                                't2.sequence,' +
                                                't2.id id_kpi_group,' +
                                                't2.group_name,' +
                                                't3.sequence seq,' +
                                                't3.group_name parent,' +
                                                't1.target_mtd,' +
                                                't1.target_ytd,' +
                                                't1.uom ' +
                                            'from ' +
                                                'rpt.vw_PlantRegion v full join ' +
                                                'rpt.vw_bulan vx on 1=1 full join ' +
                                                'rpt.T_KPI_GROUP_2 t2 ON 1=1 left outer join ' +
                                                'rpt.T_KPI_GROUP_2_SCORE t1 on t2.id=t1.id_kpi_group_2 AND v.name_coy=t1.plant collate SQL_Latin1_General_CP1_CI_AS and vx.bulan=t1.bulan and t1.tahun=' + tahun + ' left outer join ' +
                                                'rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id ' +
                                            'where t2.is_active=1 ' +
                                            'and t2.role_id=@role_id ' +
                                            'union all ' +
                                            'select ' +
                                                tahun + ' tahun,' +
                                                'vx.bulan bln,' +
                                                '3 level,' +
                                                'v.name_coy plant,' +
                                                't2.sequence,' +
                                                't2.id id_kpi_group,' +
                                                't2.group_name,' +
                                                't3.sequence seq,' +
                                                't3.group_name parent,' +
                                                't1.target_mtd,' +
                                                't1.target_ytd,' +
                                                't1.uom ' +
                                            'from ' +
                                                'rpt.vw_PlantRegion v full join ' +
                                                'rpt.vw_bulan vx on 1=1 full join ' +
                                                'rpt.T_KPI_GROUP_3 t2 ON 1=1 left outer join ' +
                                                'rpt.T_KPI_GROUP_3_SCORE t1 on t2.id=t1.id_kpi_group_3 AND v.name_coy=t1.plant collate SQL_Latin1_General_CP1_CI_AS and vx.bulan=t1.bulan and t1.tahun=' + tahun + ' left outer join ' +
                                                'rpt.T_KPI_GROUP_2 t3 on t2.id_kpi_group_2=t3.id ' +
                                            'where t2.is_active=1 ' +
                                            'and t2.role_id=@role_id ' +
                                            ') t where plant=@plant order by plant,tahun,level,seq,sequence,bln'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getTargetByTahunLevelPlant : function(req,res) {
            const { tahun, plant, level } = req.params
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var request = new sql.Request(pool);
                                    request.input('plant', sql.VarChar, plant);
                                    request.input('level', sql.Int, level);
                                    var sqlx = ''

                                        sqlx = 'select * from ( ' +
                                            'select ' +
                                                tahun + ' tahun,' +
                                                'vx.bulan bln,' +
                                                '2 level,' +
                                                'v.name_coy plant,' +
                                                't2.sequence,' +
                                                't2.id id_kpi_group,' +
                                                't2.group_name,' +
                                                't3.sequence seq,' +
                                                't3.group_name parent,' +
                                                't1.target_mtd,' +
                                                't1.target_ytd ' +
                                            'from ' +
                                                'rpt.T_KPI_GROUP_2 t2 left outer join ' +
                                                'rpt.T_KPI_GROUP_2_SCORE t1 on t2.id=t1.id_kpi_group_2 left outer join ' +
                                                'rpt.T_KPI_GROUP_1 t3 on t2.id_kpi_group_1=t3.id full join ' +
                                                'rpt.vw_PlantRegion v on 1=1 full join ' +
                                                'rpt.vw_bulan vx on 1=1 ' +
                                            'where t2.is_active=1 ' +
                                            'union all ' +
                                            'select ' +
                                                tahun + ' tahun,' +
                                                'vx.bulan bln,' +
                                                '3 level,' +
                                                'v.name_coy plant,' +
                                                't2.sequence,' +
                                                't2.id id_kpi_group,' +
                                                't2.group_name,' +
                                                't3.sequence seq,' +
                                                't3.group_name parent,' +
                                                't1.target_mtd,' +
                                                't1.target_ytd ' +
                                            'from ' +
                                                'rpt.T_KPI_GROUP_3 t2 left outer join ' +
                                                'rpt.T_KPI_GROUP_3_SCORE t1 on t2.id=t1.id_kpi_group_3 left outer join ' +
                                                'rpt.T_KPI_GROUP_2 t3 on t2.id_kpi_group_2=t3.id full join ' +
                                                'rpt.vw_PlantRegion v on 1=1 full join ' +
                                                'rpt.vw_bulan vx on 1=1 ' +
                                            'where t2.is_active=1 ' +
                                            ') t where plant=@plant and level=@level order by plant,tahun,bln,level,seq,sequence'
                                    request.query(sqlx,function (err, recordsetx){
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        uploadTargetScore : function(req,res) {
            const { data } = req.body
            console.log(JSON.stringify(data))
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    console.log(err)
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    async.eachSeries(data,function(obj,callback){
                                        if (obj.level==2) {
                                            var request = new sql.Request(pool);
                                            request.input('id_kpi_group_2', sql.Int, obj.id_kpi_group);
                                            request.input('tahun', sql.Int, obj.tahun);
                                            request.input('bulan', sql.Int, obj.bulan);
                                            request.input('plant', sql.VarChar,obj.plant)
                                            request.input('role_id', sql.Int, obj.roleid);
                                            var sqlx = 'select * from rpt.T_KPI_GROUP_2_SCORE where role_id=@role_id and tahun=@tahun and id_kpi_group_2=@id_kpi_group_2 and bulan=@bulan and plant=@plant'
                                            request.query(sqlx, function (err, recordset) {
                                                if (err) {
                                                    pool.close();
                                                    console.log(err)
                                                    return callback(err)
                                                }
                                                else {
                                                    if (recordset.recordsets[0].length>0) {
                                                        var request = new sql.Request(pool);
                                                        //var ach_mtd = null;
                                                        //var score_mtd = null;
                                                        //var ach_ytd = null;
                                                        //var score_ytd = null;
                                                        //if (obj.target_mtd!=null && obj.target_mtd!=0 && obj.actual_mtd!=null) {
                                                        //    ach_mtd = obj.actual_mtd / obj.target_mtd;
                                                        //    score_mtd = calculateScore(obj.target_mtd,obj.actual_mtd)
                                                       // }
                                                       // if (obj.target_ytd!=null && obj.target_ytd!=0 && obj.actual_ytd!=null) {
                                                       //     ach_ytd = obj.actual_ytd / obj.target_ytd;
                                                       //     score_ytd = calculateScore(obj.target_ytd,obj.actual_ytd)
                                                       // }
                                                       console.log('here')
                                                        request.input('id_kpi_group_2',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('bulan',sql.Int,obj.bulan);
                                                        request.input('target_mtd', sql.Numeric(18,2), obj.target_mtd);
                                                        //request.input('actual_mtd', sql.Numeric(18,2), obj.actual_mtd);
                                                        request.input('target_ytd', sql.Numeric(18,2), obj.target_ytd);
                                                        //request.input('actual_ytd', sql.Numeric(18,2), obj.actual_ytd);
                                                        //request.input('ach_mtd', sql.Numeric(18,2), ach_mtd);
                                                        //request.input('score_mtd', sql.Numeric(18,2), score_mtd);
                                                        //request.input('ach_ytd', sql.Numeric(18,2), ach_ytd);
                                                        //request.input('score_ytd', sql.Numeric(18,2), score_ytd);
                                                        request.input('plant',sql.VarChar,obj.plant);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        request.input('uom',sql.VarChar,obj.uom);
                                                        var sqlx = 'update rpt.T_KPI_GROUP_2_SCORE set uom=@uom,role_id=@role_id,target_mtd=@target_mtd,target_ytd=@target_ytd,plant=@plant,lastmodified=@lastmodified where id_kpi_group_2=@id_kpi_group_2 and tahun=@tahun and bulan=@bulan'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                    else {
                                                        //var ach_mtd = null;
                                                        //var score_mtd = null;
                                                        //var ach_ytd = null;
                                                        //var score_ytd = null;
                                                        //if (obj.target_mtd!=null && obj.target_mtd!=0 && obj.actual_mtd!=null) {
                                                        //    ach_mtd = obj.actual_mtd / obj.target_mtd;
                                                        //    score_mtd = calculateScore(obj.target_mtd,obj.actual_mtd)
                                                        //}
                                                        //if (obj.target_ytd!=null && obj.target_ytd!=0 && obj.actual_ytd!=null) {
                                                        //    ach_ytd = obj.actual_ytd / obj.target_ytd;
                                                        //    score_ytd = calculateScore(obj.target_ytd,obj.actual_ytd)
                                                        //}
                                                        console.log('there')
                                                        var request = new sql.Request(pool);
                                                        request.input('id_kpi_group_2',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('bulan',sql.Int,obj.bulan);
                                                        request.input('target_mtd', sql.Numeric(18,2), obj.target_mtd);
                                                        //request.input('actual_mtd', sql.Numeric(18,2), obj.actual_mtd);
                                                        request.input('target_ytd', sql.Numeric(18,2), obj.target_ytd);
                                                        //request.input('actual_ytd', sql.Numeric(18,2), obj.actual_ytd);
                                                        //request.input('ach_mtd', sql.Numeric(18,2), ach_mtd);
                                                        //request.input('score_mtd', sql.Numeric(18,2), score_mtd);
                                                        //request.input('ach_ytd', sql.Numeric(18,2), ach_ytd);
                                                        //request.input('score_ytd', sql.Numeric(18,2), score_ytd);
                                                        request.input('plant',sql.VarChar,obj.plant);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        request.input('uom',sql.VarChar,obj.uom);
                                                        var sqlx = 'insert into rpt.T_KPI_GROUP_2_SCORE (uom,role_id,id_kpi_group_2,tahun,bulan,target_mtd,target_ytd,plant,lastmodified) values (@uom,@role_id,@id_kpi_group_2,@tahun,@bulan,@target_mtd,@target_ytd,@plant,@lastmodified)'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            var request = new sql.Request(pool);
                                            request.input('id_kpi_group_3', sql.Int, obj.id_kpi_group);
                                            request.input('tahun', sql.Int, obj.tahun);
                                            request.input('bulan', sql.Int, obj.bulan);
                                            request.input('plant', sql.VarChar,obj.plant)
                                            request.input('role_id', sql.Int, obj.roleid);
                                            var sqlx = 'select * from rpt.T_KPI_GROUP_3_SCORE where role_id=@role_id and tahun=@tahun and id_kpi_group_3=@id_kpi_group_3 and bulan=@bulan and plant=@plant'
                                            request.query(sqlx, function (err, recordset) {
                                                if (err) {
                                                    pool.close();
                                                    console.log(err)
                                                    return callback(err)
                                                }
                                                else {
                                                    if (recordset.recordsets[0].length>0) {
                                                        console.log('here 2')
                                                        var request = new sql.Request(pool);
                                                        //var ach_mtd = null;
                                                        //var score_mtd = null;
                                                        //var ach_ytd = null;
                                                        //var score_ytd = null;
                                                        //if (obj.target_mtd!=null && obj.target_mtd!=0 && obj.actual_mtd!=null) {
                                                        //    ach_mtd = obj.actual_mtd / obj.target_mtd;
                                                        //    score_mtd = calculateScore(obj.target_mtd,obj.actual_mtd)
                                                        //}
                                                        //if (obj.target_ytd!=null && obj.target_ytd!=0 && obj.actual_ytd!=null) {
                                                        //    ach_ytd = obj.actual_ytd / obj.target_ytd;
                                                        //    score_ytd = calculateScore(obj.target_ytd,obj.actual_ytd)
                                                        //}
                                                        request.input('id_kpi_group_3',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('bulan',sql.Int,obj.bulan);
                                                        request.input('target_mtd', sql.Numeric(18,2), obj.target_mtd);
                                                        //request.input('actual_mtd', sql.Numeric(18,2), obj.actual_mtd);
                                                        request.input('target_ytd', sql.Numeric(18,2), obj.target_ytd);
                                                        //request.input('actual_ytd', sql.Numeric(18,2), obj.actual_ytd);
                                                       // request.input('ach_mtd', sql.Numeric(18,2), ach_mtd);
                                                        //request.input('score_mtd', sql.Numeric(18,2), score_mtd);
                                                        //request.input('ach_ytd', sql.Numeric(18,2), ach_ytd);
                                                        //request.input('score_ytd', sql.Numeric(18,2), score_ytd);
                                                        request.input('plant',sql.VarChar,obj.plant);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        request.input('uom',sql.VarChar,obj.uom);
                                                        var sqlx = 'update rpt.T_KPI_GROUP_3_SCORE set uom=@uom,role_id=@role_id,target_mtd=@target_mtd,target_ytd=@target_ytd,plant=@plant,lastmodified=@lastmodified where id_kpi_group_3=@id_kpi_group_3 and tahun=@tahun and bulan=@bulan'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                    else {
                                                        //var ach_mtd = null;
                                                        //var score_mtd = null;
                                                        //var ach_ytd = null;
                                                        //var score_ytd = null;
                                                        //if (obj.target_mtd!=null && obj.target_mtd!=0 && obj.actual_mtd!=null) {
                                                        //    ach_mtd = obj.actual_mtd / obj.target_mtd;
                                                        //    score_mtd = calculateScore(obj.target_mtd,obj.actual_mtd)
                                                        //}
                                                        //if (obj.target_ytd!=null && obj.target_ytd!=0 && obj.actual_ytd!=null) {
                                                        //    ach_ytd = obj.actual_ytd / obj.target_ytd;
                                                        //    score_ytd = calculateScore(obj.target_ytd,obj.actual_ytd)
                                                       //}
                                                       console.log('there 2')
                                                        var request = new sql.Request(pool);
                                                        request.input('id_kpi_group_3',sql.Int,obj.id_kpi_group);
                                                        request.input('tahun',sql.Int,obj.tahun);
                                                        request.input('bulan',sql.Int,obj.bulan);
                                                        request.input('target_mtd', sql.Numeric(18,2), obj.target_mtd);
                                                        //request.input('actual_mtd', sql.Numeric(18,2), obj.actual_mtd);
                                                        request.input('target_ytd', sql.Numeric(18,2), obj.target_ytd);
                                                        //request.input('actual_ytd', sql.Numeric(18,2), obj.actual_ytd);
                                                        //request.input('ach_mtd', sql.Numeric(18,2), ach_mtd);
                                                        //request.input('score_mtd', sql.Numeric(18,2), score_mtd);
                                                        //request.input('ach_ytd', sql.Numeric(18,2), ach_ytd);
                                                        //request.input('score_ytd', sql.Numeric(18,2), score_ytd);
                                                        request.input('plant',sql.VarChar,obj.plant);
                                                        request.input('lastmodified',sql.DateTime,new Date());
                                                        request.input('role_id', sql.Int, obj.roleid);
                                                        request.input('uom',sql.VarChar,obj.uom);
                                                        var sqlx = 'insert into rpt.T_KPI_GROUP_3_SCORE (uom,role_id,id_kpi_group_3,tahun,bulan,target_mtd,target_ytd,plant,lastmodified) values (@uom,@role_id,@id_kpi_group_3,@tahun,@bulan,@target_mtd,@target_ytd,@plant,@lastmodified)'
                                                        request.query(sqlx,function(err,recordset){
                                                            if (err) {
                                                                //pool.close();
                                                                console.log(err)
                                                                return callback(err)
                                                            }
                                                            else {
                                                                //pool.close();
                                                                return callback();
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        }
                                    },function(err){
                                        pool.close();
                                        if (err) {
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            return res.status(200).json('success');
                                        }
                                    })

                                }
                            });
                        }
                    });
                }
            });
        },

        getCurrentScores : function(req,res) {
            var {
                tahun,
                bulan,
                plant,
                level,
                kpi
            } = req.body
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    var kpistr = '';
                                    console.log(JSON.stringify(kpi))
                                    for(var i=0;i<kpi.length;i++) {
                                        kpistr+=kpi[i] + ','
                                    }
                                    kpistr = kpistr.substring(0,kpistr.length-1)
                                    console.log(kpistr)
                                    var request = new sql.Request(pool);
                                    request.input('tahun',sql.Int,tahun)
                                    request.input('bulan',sql.Int,bulan)
                                    request.input('plant',sql.VarChar,plant)

                                    var sqlx = '';
                                    if (level==2) {
                                        sqlx = 'select t2.id as idkpi,t2.group_name,t2.ytd_measurement,t1.* from rpt.T_KPI_GROUP_2_SCORE t1 inner join rpt.T_KPI_GROUP_2 t2 on t1.id_kpi_group_2=t2.id where t1.tahun=@tahun and t1.bulan=@bulan and t1.plant=@plant and t1.id_kpi_group_2 in (' + kpistr + ')'
                                    }
                                    else {
                                        sqlx = 'select t2.id as idkpi,t2.group_name,t2.ytd_measurement,t1.* from rpt.T_KPI_GROUP_3_SCORE t1 inner join rpt.T_KPI_GROUP_3 t2 on t1.id_kpi_group_3=t2.id where t1.tahun=@tahun and t1.bulan=@bulan and t1.plant=@plant and t1.id_kpi_group_3 in (' + kpistr + ')'
                                    }
                                    console.log(sqlx)
                                    request.query(sqlx, function (err, recordsetx) {
                                        if (err) {
                                            pool.close();
                                            console.log(err)
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            pool.close();
                                            return res.status(200).json(recordsetx.recordsets[0])
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        editScoresKPIS : function(req,res) {
            var {
                plant,
                tahun,
                bulan,
                level,
                kpis
            } = req.body
            console.log(JSON.stringify(req.body))
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            var username = result[0].username;
                            var pwd = auth.decrypt(result[0].pwd);
                            var dbname = result[0].dbname;
                            var serveraddress = result[0].serveraddress;
                            var config = {
                                user: username,
                                password: pwd,
                                server: serveraddress,
                                database: dbname,
                                port : 1433,
                                            pool: {
                                                max: 10,
                                                min: 0,
                                                idleTimeoutMillis: 600000
                                              },
                                              options: {
                                                encrypt: false, // for azure
                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                              }
                            };
                            const pool = new sql.ConnectionPool(config, function (err) {
                                if (err) {
                                    pool.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    async.eachSeries(kpis,function(obj,callback){
                                        var request = new sql.Request(pool);
                                        request.input('plant',sql.VarChar,plant)
                                        request.input('tahun',sql.Int,tahun)
                                        request.input('bulan',sql.Int,bulan)
                                        var mtd = true
                                        if (obj.kpiid.substr(obj.kpiid.length-2,2)=='-1') {
                                            mtd = true
                                            request.input('actual_mtd', sql.Numeric(18,2),(obj.value) ? obj.value : null)
                                        }
                                        else {
                                            mtd = false
                                            request.input('actual_ytd', sql.Numeric(18,2),(obj.value) ? obj.value : null)
                                        }
                                        var sqlx = ''
                                        if (level==2) {
                                            var str = obj.kpiid.substr(0,obj.kpiid.length-2)
                                            console.log(str)
                                            request.input('id_kpi_group_2',sql.Int,str)
                                            if (mtd==true) {
                                                sqlx = 'update rpt.T_KPI_GROUP_2_SCORE set actual_mtd=@actual_mtd where plant=@plant and tahun=@tahun and bulan=@bulan and id_kpi_group_2=@id_kpi_group_2'
                                            }
                                            else {
                                                sqlx = 'update rpt.T_KPI_GROUP_2_SCORE set actual_ytd=@actual_ytd where plant=@plant and tahun=@tahun and bulan=@bulan and id_kpi_group_2=@id_kpi_group_2'
                                            }
                                        }
                                        else {
                                            var str = obj.kpiid.substr(0,obj.kpiid.length-2)
                                            console.log(str)
                                            request.input('id_kpi_group_3',sql.Int,str)
                                            if (mtd==true) {
                                                sqlx = 'update rpt.T_KPI_GROUP_3_SCORE set actual_mtd=@actual_mtd where plant=@plant and tahun=@tahun and bulan=@bulan and id_kpi_group_3=@id_kpi_group_3'
                                            }
                                            else {
                                                sqlx = 'update rpt.T_KPI_GROUP_3_SCORE set actual_ytd=@actual_ytd where plant=@plant and tahun=@tahun and bulan=@bulan and id_kpi_group_3=@id_kpi_group_3'
                                            }
                                        }
                                        request.query(sqlx,function(err,recordsetx){
                                            if (err)
                                                return callback(err)
                                            else
                                                return callback()
                                        })
                                    },function(err){
                                        if (err) {
                                            pool.close()
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request(pool);

                                            pool.close()
                                            return res.status(200).json('success')
                                        }
                                    })
                                }
                            });
                        }
                    })
                }
            });
        },
    }

module.exports = kpiOps;

const executeKPIQueue = new Queue('kpiqueue', {
    redis: {
      host: '127.0.0.1',
      port: 6379,
    }
});

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

function calculateScore(target,actual) {
    var score = 0;
    return score;
}

executeKPIQueue.process(async job => {
    return executeKPI(job.data).then(
        function(result) {
            /* handle a successful result */
            //console.log('done sending email to ' + job.data.to);
        },
        function(error) {
            /* handle an error */
            console.log('error ' + error);
            writeErrorLog('Error executing KPI ' + job.data.level + ' - ' + job.data.group_code + ' - ' + job.data.group_name ,error);
            var jsonRes = {
                config : job.data.config,
                level : job.data.level,
                kpiid : job.data.kpiid,
                group_code : job.data.group_code,
                group_name : job.data.group_name,
                sp_name : job.data.sp_name,
                date1 : job.data.date1,
                date2 : job.data.date2,
                role_id: (job.role_id) ? job.role_id : null
            };
            executeKPIQueue.add(jsonRes);

        }
    );
});

function executeKPI(jsonRes) {
    return new Promise((resolve, reject) => {
        console.log('NAME : ' + JSON.stringify(jsonRes.sp_name))
        if (jsonRes.sp_name!=null && jsonRes.sp_name!='') {
            const pool = new sql.ConnectionPool(jsonRes.config, function (errx) {
                if (errx) {
                    console.log('ERROR CONNECTION ' + jsonRes.sp_name + ' ' + errx)
                    pool.close();
                    reject(errx);
                }
                else {
                    var request = new sql.Request(pool);
                    var dtx1 = helper.formatDateShort(new Date(jsonRes.date1))
                    var dtx2 = helper.formatDateShort(new Date(jsonRes.date2))
                    console.log('tanggal ' + dtx1 + ' ' + dtx2)
                    request.input('FromDate', sql.Date, dtx1);
                    request.input('ToDate', sql.Date, dtx2);
                    if (jsonRes.role_id) request.input('role_id', sql.Int, jsonRes.role_id);
                    request.execute(jsonRes.sp_name, function (erry, recordset) {
                        if (erry) {
                            console.log('ERROR SP ' + erry)
                            pool.close();
                            reject(erry);
                        }
                        else {
                            console.log(jsonRes.sp_name + ' success')
                            pool.close()
                            resolve(true);
                            /*
                                if (jsonRes.level==2) {
                                    var request = new sql.Request(pool)
                                    var tahun = new Date(jsonRes.dt1).getFullYear() 
                                    var bulan = new Date(jsonRes.dt1).getMonth() + 1
                                    request.input('tahun',sql.Int,tahun)
                                    request.input('bulan',sql.Int,bulan)
                                    request.input('kpiid', sql.Int,jsonRes.kpiid)
                                    var sqlx = 'update rpt.T_KPI_GROUP_2_SCORE set ' +
                                    'actual_ytd=rpt.fn_KPI_ytd_value(id_kpi_group_2,2,@tahun,@bulan,actual_mtd,plant),' +
                                    'score_mtd=rpt.fn_KPI_score(id_kpi_group_2,2,@tahun,target_mtd,actual_mtd),' +
                                    'ach_mtd=rpt.fn_KPI_ach(id_kpi_group_2,2,@tahun,target_mtd,actual_mtd) ' +
                                    'where id_kpi_group_2=@kpiid and ' +
                                    'tahun=@tahun and ' +
                                    'bulan=@bulan' 
                        
                                    request.query(sqlx,function(err,recordsetx){
                                        if (err) {
                                            console.log('ERROR level 2 ' +  jsonRes.sp_name + ' ' + err)
                                            pool.close();
                                            reject(err);
                                        }
                                        else {
                                            var request = new sql.Request(pool)
                                            request.input('tahun',sql.Int,tahun)
                                            request.input('bulan',sql.Int,bulan)
                                            request.input('kpiid', sql.Int,jsonRes.kpiid)
                                            var sqlx = 'update rpt.T_KPI_GROUP_2_SCORE set ' +
                                            'ach_ytd=rpt.fn_KPI_ach(id_kpi_group_2,2,@tahun,target_ytd,actual_ytd),' +
                                            'score_ytd=rpt.fn_KPI_score(id_kpi_group_2,2,@tahun,target_ytd,actual_ytd) ' +
                                            'where id_kpi_group_2=@kpiid and ' +
                                            'tahun=@tahun and ' +
                                            'bulan=@bulan' 
                                            request.query(sqlx,function(err,recordsetx){
                                                if (err) {
                                                    console.log('ERROR level 3 ' +  jsonRes.sp_name + ' ' + err)
                                                    pool.close();
                                                    reject(err);
                                                }
                                                else {
                                                    console.log(jsonRes.sp_name + ' success')
                                                    pool.close()
                                                    resolve(true);
                                                }
                                            });
                                        }
                                    })
                                }
                                else {
                                    var request = new sql.Request(pool)
                                    var tahun = new Date(jsonRes.dt1).getFullYear() 
                                    var bulan = new Date(jsonRes.dt1).getMonth() + 1
                                    request.input('tahun',sql.Int,tahun)
                                    request.input('bulan',sql.Int,bulan)
                                    request.input('kpiid', sql.Int,jsonRes.kpiid)
                                    var sqlx = 'update rpt.T_KPI_GROUP_3_SCORE set ' +
                                    'actual_ytd=rpt.fn_KPI_ytd_value(id_kpi_group_3,3,@tahun,@bulan,actual_mtd,plant),' +
                                    'score_mtd=rpt.fn_KPI_score(id_kpi_group_3,3,@tahun,target_mtd,actual_mtd),' +
                                    'ach_mtd=rpt.fn_KPI_ach(id_kpi_group_3,3,@tahun,target_mtd,actual_mtd) ' +
                                    'where id_kpi_group_3=@kpiid and ' +
                                    'tahun=@tahun and ' +
                                    'bulan=@bulan' 
                        
                                    request.query(sqlx,function(err,recordsetx){
                                        if (err) {
                                            console.log(err)
                                            pool.close();
                                            reject(err);
                                        }
                                        else {
                                            var request = new sql.Request(pool)
                                            request.input('tahun',sql.Int,tahun)
                                            request.input('bulan',sql.Int,bulan)
                                            request.input('kpiid', sql.Int,jsonRes.kpiid)
                                            var sqlx = 'update rpt.T_KPI_GROUP_3_SCORE set ' +
                                            'ach_ytd=rpt.fn_KPI_ach(id_kpi_group_3,3,@tahun,target_ytd,actual_ytd),' +
                                            'score_ytd=rpt.fn_KPI_score(id_kpi_group_3,3,@tahun,target_ytd,actual_ytd) ' +
                                            'where id_kpi_group_3=@kpiid and ' +
                                            'tahun=@tahun and ' +
                                            'bulan=@bulan' 
                                            request.query(sqlx,function(err,recordsetx){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    reject(err);
                                                }
                                                else {
                                                    console.log(jsonRes.sp_name + ' success')
                                                    pool.close()
                                                    resolve(true);
                                                }
                                            });
                                        }
                                    });
                                }
                            */
                        }
                    });
                }
            });
        }
        else {

            resolve(true);
        }
    });
}

/*
function executeKPI(jsonRes) {
    return new Promise((resolve, reject) => {
        if (jsonRes.sp_name!=null && jsonRes.sp_name!='') {
            const pool = new sql.ConnectionPool(jsonRes.config, function (errx) {
                if (errx) {
                    console.log('ERORR ' + jsonRes.sp_name)
                   // executeKPIQueue.add(jsonRes);
                    pool.close();
                    reject(errx);
                }
                else {
                    var request = new sql.Request(pool);
                    console.log(jsonRes.date1 + ' ' + jsonRes.date2)
                    request.input('FromDate', sql.Date, helper.formatDateShort(new Date(jsonRes.date1)));
                    request.input('ToDate', sql.Date, helper.formatDateShort(new Date(jsonRes.date2)));
                    request.execute(jsonRes.sp_name, function (erry, recordset) {
                        if (erry) {
                          //  executeKPIQueue.add(jsonRes);
                            console.log(erry)
                            pool.close();
                            reject(erry);
                        }
                        else {
                            console.log(jsonRes.sp_name + ' success')
                            pool.close()
                            resolve(true);
                        }
                    });
                }
            });
        }
        else {
            resolve(true);
        }
    });
}
*/
