var mongo = require('mongodb');
var url = require('../config/mgConfig.js');
var dbname = require('../config/dbPool');
var auth = require('../middlewares/auth.js');
var helper = require('../middlewares/helper');
var async = require('async');
var ObjectId = require('mongodb').ObjectId;
const sql = require('mssql');
var nodemailer = require('nodemailer');
var topicname = '';
var topiccode = '';
var topicid = '';


    var mainOps = {

        emailForceRun : function(req,res) {
            var id = req.params.id;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    writeErrorLog('Step 1 - Connect mongodb',err);
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var jsonTopFinal = [];

                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics")
                        .aggregate([
                            { 
                                $match:
                                {
                                    _id : new ObjectId(id)
                                },
                            },
                            {
                                $project: 
                                {
                                    _id : true,
                                    topicname : true,
                                    topiccode : true,
                                    reports : true,
                                    emailroles : true,
                                    emailscheduleselected : true,
                                    emailsuccess : true,
                                    emaillastrun : true,
                                    lastrunday : {
                                    
                                                $divide: 
                                                    [    
                                                        { 
                                                            $subtract: [new Date(), '$emaillastrun'] 
                                                        },  1000 * 60 * 60 * 24
                                                    ]        
                                         
                                    },
                                    lastrunweek : {
                                       
                                                $divide: 
                                                    [
                                                        {$divide: 
                                                            [
                                                                { 
                                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                                },  1000 * 60 * 60 * 24
                                                            ]
                                                        }, 7
                                                    ]            
                                       
                                    },
                                    lastrunmonth : {
                                       
                                                $divide:
                                                [
                                                    {$divide: 
                                                        [
                                                            { 
                                                                $subtract: [new Date(), '$emaillastrun'] 
                                                            },  (1000 * 60 * 60 * 24)
                                                        ]
                                                    }, 30
                                                ]    
                                         
                                    },
                                },
                               
                            },
                            /*
                            {
                                $match:
                                { 
                                    $or : 
                                        [ 
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunday : { $gt : 0.5}, emailscheduleselected :0 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunweek : { $gt : 0.5}, emailscheduleselected :1 }
                                                    ]
                                            },
                                            {
                                                $and:
                                                    [ 
                                                        {lastrunmonth : { $gt : 0.5}, emailscheduleselected :2 }
                                                    ]
                                            },
                                        ]
                                },
                            },
                            */
                            {   
                                
                                $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'reports.value',
                                    foreignField: '_id',
                                    as: 'reportdetails'
                                }
                                    
                                   
                            },
                            {
                                $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'emailroles.value',
                                    foreignField: '_id',
                                    as: 'roledetails'
                                },
                            },
                        ])
                        .toArray(function(errB,result) { 
                            if (errB) {
                                db.close();
                                //console.log('1 ' + err);
                                writeErrorLog('Step 2 - to Array',errB);
                                auth.setResponse(res,400,errB);
                                return;
                            }
                            else {

                                async.eachSeries(result, function(objT, callback00) {
                                    var topics = {
                                        _id : objT._id,
                                        topicname : objT.topicname,
                                        topiccode : objT.topiccode,
                                        reports : [],
                                        special_recipients : [],
                                        HR_recipients : []
                                    }
                                    async.eachSeries(objT.reportdetails, function(obj1,callback1){
                                        if (obj1.is_active==1) {
                                            dbo.collection("t_servers").find({_id : new ObjectId(obj1.serveraddress)}).toArray(function(err0,servernames){
                                                if (err0) {
                                                    writeErrorLog('Step 3 - t_servers',err0);
                                                    return callback1(err0);
                                                }
                                                else {
                                                    //exec sp
                                                    var config = null;
                                                    if (servernames[0].servertype==0) { 
                                                        config = {
                                                            user: servernames[0].username,
                                                            password: auth.decrypt(servernames[0].pwd),
                                                            server: servernames[0].serveraddress, 
                                                            database: servernames[0].dbname,
                                                            port : 1433,
                                                            requestTimeout : 360000,
                                                            connectionTimeout: 15000
                                                        }
                                                        //console.log('data ' + JSON.stringify(config))
                                                    }
                                                    sql.connect(config, function (errx) {
                                                        if (errx) {
                                                            sql.close();
                                                            writeErrorLog('Step 4 - SQL Connect',errx);
                                                            //console.log('sini ' + errx)
                                                            return callback1(errx); 
                                                        }
                                                        else {
                                                            //console.log('ok')
                                                            var request = new sql.Request();
                                                            request.execute(obj1.functionname, function (erry, recordset) {
                                                                if (erry) {
                                                                    sql.close();
                                                                    writeErrorLog('Step 5 - SQL Execute',obj1.functionname + ' ' + erry);
                                                                    return callback1(erry);
                                                                }
                                                                else {
                                                                    sql.close();
                                                                    var query2 = { id_report : obj1._id };
                                                                    dbo.collection("t_columns").find(query2).toArray(function(err1, colnames) {
                                                                        if (err1) {
                                                                            writeErrorLog('Step 6 - Column Execute',err1);
                                                                            return callback1(err1);
                                                                        }
                                                                        else {
                                                                            topics.reports.push({
                                                                                reportname : obj1.reportname,
                                                                                reportcode : obj1.reportcode,
                                                                                reportheader : obj1.reportheader,
                                                                                reportfooter : obj1.reportfooter,
                                                                                requestedby : obj1.requestedby,
                                                                                plantcolumn : obj1.plantcolumn,
                                                                                isfooter : obj1.isfooter,
                                                                                columnnames : colnames,
                                                                                rows : recordset.recordsets[0]
                                                                            });
                                                                            return callback1();
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                        else {
                                            return callback1();
                                        }
                                    }, function(err0){
                                        if (err0) {
                                            writeErrorLog('Step 7 - Summary',err0);
                                            return callback00(err0);
                                        }
                                        else {
                                            async.eachSeries(objT.roledetails, function(obj2,callback0z){
                                                if (obj2.is_active==1) {
                                                    dbo.collection("t_servers").find({_id : new ObjectId(obj2.dbserver)}).toArray(function(err4,servernames2){
                                                        if (err4) {
                                                            writeErrorLog('Step 8 - t_servers',err4);
                                                            return callback0z(err4);
                                                        }
                                                        else {
                                                            var config2 = null;
                                                            //console.log('server type ' + servernames2[0].servertype==0)
                                                            if (servernames2[0].servertype==0) { 
                                                                config2 = {
                                                                    user: servernames2[0].username,
                                                                    password: auth.decrypt(servernames2[0].pwd),
                                                                    server: servernames2[0].serveraddress, 
                                                                    database: servernames2[0].dbname,
                                                                    port : 1433,
                                                                    requestTimeout : 360000,
                                                                    connectionTimeout: 15000 

                                                                }
                                                                //console.log(JSON.stringify(config2))
                                                            }
                                                            sql.connect(config2, function (errx) {
                                                                if (errx) {
                                                                    sql.close();
                                                                    writeErrorLog('Step 9 - SQL Execute',errx);
                                                                    return callback0z(errx); 
                                                                }
                                                                else {
                                                                    //SINI
                                                                    var keyword = obj2.keyword;
                                                                    if (keyword=='') {
                                                                        sql.close();
                                                                        jsonEmp = [];
                                                                        var query1 = { roleid : obj2._id };
                                                                        dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                            if (err2) {
                                                                                writeErrorLog('Step 10 - Role members',err2);
                                                                                return callback0z(err2);
                                                                            }
                                                                            else {
                                                                                async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                   
                                                                                    topics.special_recipients.push({ 
                                                                                        nik : obj3.nik,
                                                                                        nama : obj3.nama,
                                                                                        email : obj3.email,
                                                                                        plants : obj3.plantselected
                                                                                    });
                                                                                    //send email
                                                                                    return callback3();
                                                                                }, function(err3){
                                                                                    if (err3) {
                                                                                        writeErrorLog('Step 11 - Summary',err3);
                                                                                        return callback0z(err3);
                                                                                    }
                                                                                    else {
                                                                                        return callback0z();
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                    else {
                                                                        var request = new sql.Request();
                                                                        var arrKeyword = keyword.split(';');
                                                                        var strFilter = '';
                                                                        for(var i=0;i<arrKeyword.length;i++) {
                                                                            request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                            strFilter+="posst like @" + i + "keyword OR ";
                                                                        }
                                                                        strFilter = strFilter.substring(0,strFilter.length-4);
                                                                    
                                                                        request.query("SELECT * FROM tbl_karyawan WHERE " + strFilter, function (err, recordsetx) {
                                                                            if (err) {
                                                                                sql.close();
                                                                                writeErrorLog('Step 12 - SQL Execute',err);
                                                                                return callback0z(err); 
                                                                            }
                                                                            else {
                                                                                sql.close();
                                                                               
                                                                                topics.HR_recipients.push(recordsetx.recordsets[0]);
                                                                                var query1 = { roleid : obj2._id };
                                                                                dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                    if (err2) {
                                                                                        writeErrorLog('Step 13 - Role members',err2);
                                                                                        return callback0z(err2);
                                                                                    }
                                                                                    else {
                                                                                        async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                            topics.special_recipients.push({ 
                                                                                                nik : obj3.nik,
                                                                                                nama : obj3.nama,
                                                                                                email : obj3.email,
                                                                                                plants : obj3.plantselected
                                                                                            });
                                                                                            //send email
                                                                                            return callback3();
                                                                                        }, function(err3){
                                                                                            if (err3) {
                                                                                                writeErrorLog('Step 14 - Summary',err3);
                                                                                                return callback0z(err3);
                                                                                            }
                                                                                            else {
                                                                                                return callback0z();
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                    
                                                }
                                                else {
                                                    return callback0z();
                                                }
                                            }, function(err2){
                                                if (err2) {
                                                    writeErrorLog('Step 15 - Summary',err2);
                                                    return callback00(err2);
                                                }
                                                else {
                                                    jsonTopFinal.push(topics);
                                                    return callback00();
                                                }
                                            });
                                        }
                                    });
                                }, function(errG){
                                    //console.log('halooo')
                                    if (errG) {
                                        db.close();
                                        //console.log('2 ' + errG);
                                        writeErrorLog('Step 16 - Summary',errG);
                                        auth.setResponse(res,400,errG);
                                        return;
                                    }
                                    else {
                                        sendEmail(jsonTopFinal,true);
                                        return res.status(200).json(jsonTopFinal);
                                    }
                                });
                            }
                        }
                       
                    );
                   
                }
            });
        },

        getLogs : function(req,res) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);        
                    return;
                }
                else {
                    var query;
                    query = {};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_logs").find(query).limit(200).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

    //DAFTAR PABRIK
        getPlants : function(req,res) {
            var config = {
                user: 'report',
                password: 'KiranaMegatara21',
                server: '10.0.0.32', 
                database: 'portal',
                port : 1433 
            };
            sql.connect(config, function (err) {
                if (err) {
                    sql.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var request = new sql.Request();
                    request.query("SELECT * FROM ReportTemplate.rpt.vw_PlantRegion", function (err, recordset) {
                        if (err) {
                            sql.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            res.status(200).json(recordset.recordsets[0]);
                        }
                    });
                }
            });
        },
    //MAIL SERVER
        createMailServer : function(req,res) {
            var servername = req.body.servername;
            var serveraddress = req.body.serveraddress;
            var username = req.body.username;
            var pwd = auth.encrypt(req.body.pwd);
            var token = auth.encrypt(req.body.token);
            var is_active = req.body.is_active;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { reportcode: reportcode };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Report with the same code already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    servername : servername,
                                    username : username,
                                    pwd : pwd,
                                    serveraddress : serveraddress,
                                    token : token,
                                    lastmodified : new Date(),
                                    is_active : is_active
                                };
                                dbo.collection("t_mail_servers").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editMailServer : function(req,res) {
            var id = req.params.id;
            var servername = req.body.servername;
            var serveraddress = req.body.serveraddress;
            var username = req.body.username;
            var pwd = auth.encrypt(req.body.pwd);
            var token = auth.encrypt(req.body.token);
            var is_active = req.body.is_active;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            servername : servername,
                            servertype : servertype,
                            username : username,
                            pwd : pwd,
                            token : token,
                            serveraddress : serveraddress,
                            lastmodified : new Date(),
                            is_active : is_active
                        } 
                    };
                    dbo.collection("t_mail_servers").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        getMailServers : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                   
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getMailServerByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

    //DATABASE SERVER
        createServer : function(req,res) {
            var servername = req.body.servername;
            var serveraddress = req.body.serveraddress;
            var dbname2 = req.body.dbname;
            var servertype = req.body.servertype;
            var username = req.body.username;
            var pwd = auth.encrypt(req.body.pwd);
            var is_active = req.body.is_active;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { servername : servername };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
  
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Report with the same code already exists');

                                return;
                            }
                            else {
                                var myobj = { 
                                    servername : servername,
                                    servertype : servertype,
                                    dbname : dbname2,
                                    username : username,
                                    pwd : pwd,
                                    serveraddress : serveraddress,
                                    lastmodified : new Date(),
                                    is_active : is_active
                                };
                                dbo.collection("t_servers").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);  
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editServer : function(req,res) {
            var id = req.params.id;
            var servername = req.body.servername;
            var dbname2 = req.body.dbname;
            var serveraddress = req.body.serveraddress;
            var servertype = req.body.servertype;
            var username = req.body.username;
            var pwd = req.body.pwd;
            var is_active = req.body.is_active;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            servername : servername,
                            servertype : servertype,
                            dbname : dbname2,
                            username : username,
                            pwd : pwd,
                            serveraddress : serveraddress,
                            lastmodified : new Date(),
                            is_active : is_active
                        } 
                    };
                    dbo.collection("t_servers").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        getServers : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);

                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getServerByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);

                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {

                            db.close();
                            return res.status(200).json(result);
                        
                        }
                    });
                }
            });
        },

    //ROLES
        createRole : function(req,res) {
            var rolename = req.body.rolename;
            var keyword = req.body.keyword;
            var is_active = req.body.is_active;
            var dbserver = req.body.dbserver;
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);

                    return;
                }
                else {
                    var query = { rolename: rolename };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);

                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Role with the same name already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    rolename: rolename, 
                                    keyword: keyword,
                                    is_active : is_active,
                                    lastmodified : new Date(),
                                    dbserver : new ObjectId(dbserver)
                                };
                                dbo.collection("t_roles").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
 
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editRole : function(req,res) {
            var id = req.params.id;
            var rolename = req.body.rolename;
            var keyword = req.body.keyword;
            var is_active = req.body.is_active;
            var dbserver = req.body.dbserver;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: {
                        rolename: rolename, 
                        keyword: keyword,
                        is_active : is_active,
                        lastmodified : new Date(),
                        dbserver : new ObjectId(dbserver)
                    } };
                    dbo.collection("t_roles").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        deleteRole : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRoles : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRoleByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                   
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //TODO:
        getEmployeesByRole : async function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var keyword = result[0].keyword;
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    //if (result[0].servertype==0) {
                                        var username = result[0].username;
                                        var pwd = auth.decrypt(result[0].pwd);
                                        var dbname = result[0].dbname;
                                        var serveraddress = result[0].serveraddress;
                                        var config = {
                                            user: username,
                                            password: pwd,
                                            server: serveraddress, 
                                            database: dbname 
                                        };
                                        sql.connect(config, function (err) {
                                            if (err) {
                                                sql.close();
                                                auth.setResponse(res,400,err);
                                                return;
                                            }
                                            else {
                                                var request = new sql.Request();
                                                if (keyword=='') {
                                                    sql.close();
                                                    res.status(200).json([]);
                                                }
                                                else {
                                                    var arrKeyword = keyword.split(';');
                                                    var strFilter = '';
                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                        request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                        strFilter+="posst like @" + i + "keyword OR ";
                                                    }
                                                    //request.input('keyword', sql.VarChar, '%' + keyword + '%');
                                                    strFilter = strFilter.substring(0,strFilter.length-4);
        
                                                    request.query("SELECT * FROM tbl_karyawan WHERE " + strFilter, function (err, recordset) {
                                                        if (err) {
                                                            sql.close();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        }
                                                        else {
                                                            res.status(200).json(recordset.recordsets[0]);
                                                        }
                                                    });
                                                }
                                            }
                                            
                                        });
                                    //}
                                    //else {

                                    //}
                                }
                            });
                        }
                    });
                }
            });
            
        },

        //TODO:
        getTopicMessageTypesByRole : function(req,res) {

        },
    
    //COLUMNS
        createColumn : function(req,res) {
            var id_report = req.body.id_report;
            var columnname = req.body.columnname;
            var displayname = req.body.displayname;
            var textalignment = req.body.textalignment;
            var formatcolumn = req.body.formatcolumn;
            var columnhighlight = req.body.columnhighlight;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { id_report : new ObjectId(id_report), columnname : columnname };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Data already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    id_report : new ObjectId(id_report),
                                    columnname : columnname,
                                    displayname : displayname,
                                    textalignment : textalignment,
                                    formatcolumn : formatcolumn,
                                    columnhighlight : columnhighlight,    
                                    lastmodified : new Date(),
                                };
                                dbo.collection("t_columns").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        deleteColumn : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getColumns : function(req,res) {
            var id_report = req.params.id_report;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    query = { id_report : new ObjectId(id_report) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_columns").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },


    //ROLE MEMBER
        createRoleMember : function(req,res) {
            var roleid = req.body.roleid;
            var id_karyawan = req.body.id_karyawan;
            var nik = req.body.nik;
            var pabrik = req.body.pabrik;
            var nama = req.body.nama;
            var posisi = req.body.posisi;
            var ho = req.body.ho;
            var email = req.body.email;
            var plantselected = req.body.plantselected;
        
            var pl = [];
          
            for(i=0;i<plantselected.length;i++) {
                pl.push({value : plantselected[i]});
            }
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { id_karyawan : id_karyawan, roleid : new ObjectId(roleid) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Role member with the same ID already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    roleid : new ObjectId(roleid),
                                    id_karyawan : id_karyawan,
                                    nik : nik,
                                    gsber : pabrik,
                                    nama : nama,
                                    posisi : posisi,
                                    ho : ho,
                                    email : email,
                                    lastmodified : new Date(),
                                    plantselected : pl
                                };
                                dbo.collection("t_role_members").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        deleteRoleMember : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRoleMembers : function(req,res) {
            var roleid = req.params.roleid;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    query = { roleid : new ObjectId(roleid) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getRolePlants : function(req,res) {
            var id = req.params.id;
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query;
                    query = { _id : new ObjectId(id) };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_role_members").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getEmployees :  function(req,res) {
            var roleid = req.params.roleid;
            
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(roleid)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    var username = result[0].username;
                                    var pwd = auth.decrypt(result[0].pwd);
                                    var dbname = result[0].dbname;
                                    var serveraddress = result[0].serveraddress;
                                    var config = {
                                        user: username,
                                        password: pwd,
                                        server: serveraddress, 
                                        database: dbname 
                                    };
                                    //console.log(JSON.stringify(config))
                                    sql.connect(config, function (err) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.query("SELECT * FROM tbl_karyawan order by nama", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    res.status(200).json(recordset.recordsets[0]);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },

        getEmployeeByID : function(req,res) {
            var roleid = req.body.roleid;
            
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    auth.setResponse(res,400,err);
                    db.close();
                    return;
                }
                else {
                    var query = { _id : new ObjectId(roleid)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            var dbserver = result[0].dbserver;
                            var query = { _id : new ObjectId(dbserver)};
                            var dbo = db.db(dbname.getDbName());
                            dbo.collection("t_servers").find(query).toArray(function(err, result) {
                                if (err) {
                                    db.close();
                                    auth.setResponse(res,400,err);
                                    return;
                                }
                                else {
                                    db.close();
                                    var username = result[0].username;
                                    var pwd = auth.decrypt(result[0].pwd);
                                    var dbname = result[0].dbname;
                                    var serveraddress = result[0].serveraddress;
                                    var config = {
                                        user: username,
                                        password: pwd,
                                        server: serveraddress, 
                                        database: dbname 
                                    };
                                    sql.connect(config, function (err) {
                                        if (err) {
                                            sql.close();
                                            auth.setResponse(res,400,err);
                                            return;
                                        }
                                        else {
                                            var request = new sql.Request();
                                            request.input('nik', sql.VarChar, req.body.id);
                                            request.query("SELECT * FROM tbl_karyawan where nik=@nik", function (err, recordset) {
                                                if (err) {
                                                    sql.close();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                }
                                                else {
                                                    res.status(200).json(recordset.recordsets[0]);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },


    //REPORTS
        createReport : function(req,res) {
            var reportname = req.body.reportname; 
            var functionname = req.body.functionname; 
            var reportcode = req.body.reportcode;
            var reportheader = req.body.reportheader;
            var reportfooter = req.body.reportfooter;
            var requestedby = req.body.requestedby;
            var serveraddress = req.body.serveraddress;
            var is_active = req.body.is_active;
            var plantcolumn = req.body.plantcolumn;
            var isfooter = req.body.isfooter;
        
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { reportcode: reportcode };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Report with the same code already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    reportname: reportname, 
                                    functionname: functionname, 
                                    reportcode : reportcode,
                                    reportheader : reportheader,
                                    reportfooter : reportfooter,
                                    requestedby : requestedby,
                                    serveraddress : new ObjectId(serveraddress),
                                    lastmodified : new Date(),
                                    is_active : is_active,
                                    plantcolumn : plantcolumn,
                                    isfooter : isfooter
                                };
                                dbo.collection("t_reports").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editReport : function(req,res) {
            var id = req.params.id;
            var reportname = req.body.reportname; 
            var functionname = req.body.functionname; 
            var reportcode = req.body.reportcode;
            var reportheader = req.body.reportheader;
            var reportfooter = req.body.reportfooter;
            var requestedby = req.body.requestedby;
            var is_active = req.body.is_active;
            var serveraddress = req.body.serveraddress;
            var plantcolumn = req.body.plantcolumn;
            var isfooter = req.body.isfooter;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id: new ObjectId(id) };
                    var newvalues = { $set: 
                        {
                            reportname: reportname, 
                            functionname: functionname, 
                            reportcode : reportcode,
                            reportheader : reportheader,
                            reportfooter : reportfooter,
                            requestedby : requestedby,
                            lastmodified : new Date(), 
                            serveraddress : new ObjectId(serveraddress),
                            is_active : is_active,
                            plantcolumn : plantcolumn,
                            isfooter : isfooter
                        } 
                    };
                    dbo.collection("t_reports").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        deleteReport : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = {  _id: new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getReports : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getReportByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //FIXME:
        getTopicsByReport : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_reports").find(query).aggregate([
                        { $lookup:
                            {
                                from: 't_topic_reports',
                                localField: '_id',
                                foreignField: 'reportid',
                                as: 'report_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_topics',
                                    localField: 'report_details.topicid',
                                    foreignField: '_id',
                                    as: 'topic_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                }
            });
        },

    //TOPICS
        createTopic : function(req,res) {
            var topicname = req.body.topicname; 
            var topiccode = req.body.topiccode; 
            var reports = req.body.reports;
            var emailroles = req.body.emailroles;
            var msg1roles = req.body.msg1roles;
            var is_active = req.body.is_active;
            var emailscheduleselected = req.body.emailscheduleselected;
            var msg1scheduleselected = req.body.msg1scheduleselected;
            var startdate = req.body.startdate;
           
            var rpt = [];
            var em = [];
            var msg = [];
            var i = 0;
            for(i=0;i<reports.length;i++) {
                rpt.push({value : new ObjectId(reports[i])});
            }
            for(i=0;i<emailroles.length;i++) {
                em.push({value : new ObjectId(emailroles[i])});
            }
            for(i=0;i<msg1roles.length;i++) {
                msg.push({value : new ObjectId(msg1roles[i])});
            }

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { topiccode: topiccode };
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Topic with the same code already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    topicname : topicname,
                                    topiccode : topiccode,
                                    lastmodified : new Date(),
                                    reports : rpt,
                                    emailroles : em,
                                    msg1roles : msg,
                                    emaillastrun : startdate!=null ? new Date(startdate) : new Date(),
                                    msg1lastrun : new Date(),
                                    emailsuccess : 'N',
                                    msg1success : 'N',
                                    is_active : is_active,
                                    emailscheduleselected : emailscheduleselected,
                                    msg1scheduleselected : msg1scheduleselected,
                                };
                                dbo.collection("t_topics").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        editTopic : function(req,res) {
            var id = req.params.id;
            var topicname = req.body.topicname; 
            var topiccode = req.body.topiccode; 
            var reports = req.body.reports;
            var emailroles = req.body.emailroles;
            var msg1roles = req.body.msg1roles;
            var is_active = req.body.is_active;
            var emailscheduleselected = req.body.emailscheduleselected;
            var msg1scheduleselected = req.body.msg1scheduleselected;
            var startdate = req.body.startdate;

            console.log(startdate);

            var rpt = [];
            var em = [];
            var msg = [];
            var i = 0;
            for(i=0;i<reports.length;i++) {
                rpt.push({value : new ObjectId(reports[i])});
            }
            for(i=0;i<emailroles.length;i++) {
                em.push({value : new ObjectId(emailroles[i])});
            }
            for(i=0;i<msg1roles.length;i++) {
                msg.push({value : new ObjectId(msg1roles[i])});
            }

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var myquery = { _id : new ObjectId(id)};
                    var newvalues =  {};
                    if (startdate==null) {
                    newvalues = { $set: 
                            {
                                topicname : topicname,
                                topiccode : topiccode,
                                lastmodified : new Date(),
                                reports : rpt,
                                emailroles : em,
                                msg1roles : msg,
                                //emaillastrun : new Date(),
                                //msg1lastrun : new Date(),
                                is_active : is_active,
                                emailscheduleselected : emailscheduleselected,
                                msg1scheduleselected : msg1scheduleselected
                            } 
                        };
                    }
                    else {
                        newvalues = { $set: 
                            {
                                topicname : topicname,
                                topiccode : topiccode,
                                lastmodified : new Date(),
                                reports : rpt,
                                emailroles : em,
                                msg1roles : msg,
                                emaillastrun : new Date(startdate),
                                //msg1lastrun : new Date(),
                                is_active : is_active,
                                emailscheduleselected : emailscheduleselected,
                                msg1scheduleselected : msg1scheduleselected
                            } 
                        };
                    }
                    dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json('success');
                        }
                    });
                }
            });
        },

        deleteTopic : async function(req,res) {
            var id = req.params.id;

            const client = new MongoClient(url);
            await client.connect();
            const session = client.startSession();
            const transactionOptions = {
                readPreference: 'primary',
                readConcern: { level: 'local' },
                writeConcern: { w: 'majority' }
            };
            try {
                await session.withTransaction(async () => {
                const coll1 = client.db(dbname.getDbName()).collection('t_topics');
                const coll2 = client.db(dbname.getDbName()).collection('t_topic_reports');
                const coll3 = client.db(dbname.getDbName()).collection('t_topic_message_types_to_roles');
                var dt = [];
                async.eachSeries(col3x,function(obj,callback){
                    dt.push(ObjectId(obj._id));
                    return callback();
                }, async function(err){
                    if (err) {
                        await session.endSession();
                        await client.close();
                        auth.setResponse(res,400,err);
                        return;
                    }
                    else {
                        await coll1.deleteOne({  _id: new ObjectId(id) }, { session });
                        await coll2.deleteMany({ topicid : new ObjectId(id) }, { session });
                        await coll3.deleteMany({ topicid : new ObjectId(id) }, { session });
                    }
                });
                }, transactionOptions);
                /*
                const coll3 = client.db(dbname.getDbName()).collection('t_topic_message_types');
                const col3x = client.db(dbname.getDbName()).collection('t_topic_message_types').find({topicid :id});
                const coll4 = client.db(dbname.getDbName()).collection('t_topic_message_types_to_roles');
                var dt = [];
                async.eachSeries(col3x,function(obj,callback){
                    dt.push(ObjectId(obj._id));
                    return callback();
                }, function(err){
                    if (err) {
                        await session.endSession();
                        await client.close();
                        auth.setResponse(res,400,err);
                        return;
                    }
                    else {
                        await coll1.deleteOne({ "_id" : ObjectId(id) }, { session });
                        await coll2.deleteMany({ topicid : id }, { session });
                        await coll3.deleteMany({ topicid : id }, { session });
                        await coll4.deleteMany({  _id: { $in: dt } }, { session });
                    }
                });
                }, transactionOptions);
                */
            } finally {
                await session.endSession();
                await client.close();
                return res.status(200).json('success');
            }
        },

        getTopics : function(req,res) {
            var is_active = Number(req.params.is_active);

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query;
                    if (is_active==null || is_active==undefined || is_active===-1)
                        query = {};
                    else 
                        query = { is_active : is_active}
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        getTopicByID : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },



        //FIXME:
        getTopicByIDTemp : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).aggregate([
                        { $lookup:
                            {
                                from: 't_topic_reports',
                                localField: '_id',
                                foreignField: 'topicid',
                                as: 'report_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'report_details.reportid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_topic_message_types_to_roles',
                                    localField: '_id',
                                    foreignField: 'topicid',
                                    as: 'type_details'
                                }
                            }
                            
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'type_details.roleid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                    /*
                            dbo.collection("t_topic_reports").find({topicid : id}).toArray(function(err, result2) {
                              
                                    dbo.collection("t_topic_message_types").find({topicid : id}).toArray(function(err, result3) {
                                        if (err) {
                                            auth.setResponse(res,400,err);
                                            db.close();
                                            return;
                                        }
                                        else {

                                        }
                                    });
                                }
                            });  
                            var jsonArr = [];
                            for(var i=0;i<result.length;i++) {
                               
                                dbo.collection("t_topics").find(query).toArray(function(err, result) {
                                jsonArr.push({
                                    _id : result[i]._id,
                                    topiccode : result[i].topiccode,
                                    topicname : result[i].topicname,
                                    lastmodified : helper.formatDate(result[i].lastmodified),
                                });
                            
                            }
                            db.close();
                            return res.status(200).json(jsonArr);
                        }
                    });
                }
                */
            }
            });
        },

        addReportToTopic : function(req,res) {
            var reportid = req.body.reportid; 
            var topicid = req.body.topicid; 
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { $and: [ {reportid: reportid}, {topicid: topicid} ]};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_reports").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Topic and report with the same code already exists');
                                
                                return;
                            }
                            else {
                                var myobj = { 
                                    topicid : topicid,
                                    reportid : reportid,
                                    lastmodified : new Date(),
                                };
                                dbo.collection("t_topic_reports").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        removeReportFromTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_reports").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //FIXME:
        getReportsByTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query).aggregate([
                        { $lookup:
                            {
                                from: 't_topic_reports',
                                localField: '_id',
                                foreignField: 'topicid',
                                as: 'report_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_reports',
                                    localField: 'report_details.reportid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                }
            });
        },

        addMessageTypeRoleToTopic : function(req,res) {
            var messagetypeid = req.body.messagetypeid; 
            var roleid = req.body.roleid; 
           
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    
                    return;
                }
                else {
                    var query = { $and: [ {messagetypeid: messagetypeid}, {roleid: roleid} ]};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_message_types_to_roles").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            if (result.length>0) {
                                db.close();
                                auth.setResponse(res,400,'Same combination already exists');
                                return;
                            }
                            else {
                                var myobj = { 
                                    messagetypeid : messagetypeid,
                                    roleid : roleid,
                                    lastmodified : new Date(),
                                };
                                dbo.collection("t_topic_message_types_to_roles").insertOne(myobj, function(err, result) {
                                    if (err) {
                                        db.close();
                                        auth.setResponse(res,400,err);
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return res.status(200).json({id : result.insertedId});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        removeMessageTypeRoleToTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topic_message_types_to_roles").deleteOne(query, function(err, result) {
                        if (err) {
                            db.close();
                            auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return res.status(200).json(result);
                        }
                    });
                }
            });
        },

        //FIXME:
        getRolesByTopic : function(req,res) {
            var id = req.params.id;

            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    db.close();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    var query = { _id : new ObjectId(id)};
                    var dbo = db.db(dbname.getDbName());
                    dbo.collection("t_topics").find(query)
                        .aggregate([
                        { $lookup:
                            {
                                from: 't_topic_message_types_to_roles',
                                localField: '_id',
                                foreignField: 'topicid',
                                as: 'type_details'
                            }
                        }
                        ]).aggregate([
                            { $lookup:
                                {
                                    from: 't_roles',
                                    localField: 'type_details.roleid',
                                    foreignField: '_id',
                                    as: 'role_details'
                                }
                            }
                        ])
                        .toArray(function(err, result) {
                            if (err) {
                                db.close();
                                auth.setResponse(res,400,err);
                                return;
                            }
                            else {
                                db.close();
                                return res.status(200).json(result);
                            }
                        })                    
                }
            });
        },

    }

    module.exports = mainOps;

    function writeErrorLog(step,err) {
        var MongoClient = mongo.MongoClient;
        MongoClient.connect(url.getDbString(), function(err, db) {
            if (err) {
                db.close();
                return;
            }
            else {
                var lastrun = new Date();
                var myobj = { 
                    step : step,
                    error_desc : err,
                    lastmodified : lastrun,
                };
                var dbo = db.db(dbname.getDbName());
                dbo.collection("t_error_logs").insertOne(myobj, function(err, result) {
                    if (err) {
                        db.close();
                        return;
                    }
                });
            }
        });
    }

    function sendEmail(jsonRes,forceRun) {
        var transport = nodemailer.createTransport({
            pool: true,
            host: "mail.kiranamegatara.com",
            port: 465,
            secure: true, // use TLS
            auth: {
              user: "no-reply@kiranamegatara.com",
              pass: "1234567890"
            }
        });
        var filterEmp = [];
       
        //TOPIC ITERATION
        for (var z=0;z<jsonRes.length;z++) {
            topicid =  jsonRes[z]._id;
            topiccode = jsonRes[z].topiccode;
            topicname = jsonRes[z].topicname;

            //NOTE: Special Recipient
            for(var i=0;i<jsonRes[z].special_recipients.length;i++) {
                filterEmp.push(jsonRes[z].special_recipients[i].nik);
                var htm = '';
                var oktosend = false;
                htm = '<head>' +
                '<style>' +
                'table {' +
                    'font-family: arial, sans-serif;' +
                    'border-collapse: collapse;' +
                    'width: 100%;' +
                '}' +
                  
                'td, th {' +
                    'border: 1px solid #dddddd;' +
                    'padding: 8px;' +
                '}' +
                  
                'tr:nth-child(even) {' +
                    'background-color: #dddddd;' +
                '}' +
                
               
                '</style>' +
                '</head>'; 
                htm = htm + '<b>Kepada Bapak/Ibu ' + jsonRes[z].special_recipients[i].nama + '</b><p/>';
                htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                for (var t=0;t<jsonRes[z].reports.length;t++) {            
                    if (jsonRes[z].special_recipients[i].email!=null && jsonRes[z].special_recipients[i].email!='' && jsonRes[z].special_recipients[i].email!=undefined) {
                      
                        var hdr = '';
                        if (jsonRes[z].reports[t].rows.length>0) {
                            htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + jsonRes[z].reports[t].reportheader + '<br/>'
                            hdr = '<tr>'
                            for (var key in jsonRes[z].reports[t].rows[0]) {
                                var found = false;
                                var count = 0;
                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                {
                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter) {
                                        found = true;
                                        hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                    }
                                    count++;
                                }
                            }
                            hdr = hdr + '</tr>'
                        }
                 
                        var plantSelected = [];
                        for(var c=0;c<jsonRes[z].special_recipients[i].plants.length;c++) {
                            plantSelected.push(
                                jsonRes[z].special_recipients[i].plants[c].value
                            )
                        }
                        plantSelected.push('KMTR');
                        var plantcolname = jsonRes[z].reports[t].plantcolumn;
                        var rows = jsonRes[z].reports[t].rows;
                        var rowsfiltered = rows;
                        
                        rowsfiltered = rows.filter(row => plantSelected.includes(row[plantcolname]));

                        var ctnt = '';
                        var ftr = '';
                        var rw = '';
                        var isfooter = false;

                        var checkfooter = false;
                        if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                            checkfooter = true;
                        }

                        for (var h=0;h<rowsfiltered.length;h++) {
                            oktosend = true;
                            var bgcolor = "#F5F5F5";
                            rw = '';
                            if (checkfooter==true) {
                                for (var key in rowsfiltered[h]) {
                                    var found = false;
                                    var count = 0;
                                    var val = rowsfiltered[h][key];
                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                        if (key==jsonRes[z].reports[t].isfooter) {
                                            if (val==1 || val=='1' || val==true) {
                                                bgcolor = "#2b471d";
                                                isfooter = true;
                                                found = true;
                                            }  
                                        }
                                        count++;
                                    }
                                }
                            }
                            for (var key in rowsfiltered[h]) {
                                var val = rowsfiltered[h][key];
                                var found = false;
                                var count = 0;

                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                {
                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname) {
                                        found = true;
                                        if (val!=null && val!=undefined) {
                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                case 0:
                                                    val = val;
                                                    break;
                                                case 1:
                                                    val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                    break;
                                                case 2:
                                                    val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                    break;
                                                case 3:
                                                    val = helper.formatDateShort(val)
                                                    break;
                                                case 4:
                                                    val = helper.formatDate(val)
                                                    break;
                                            }
                                        }
                                        var bgcolor2 = bgcolor;
                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                            if (isfooter==false) 
                                                bgcolor2 = "#b5ddc3";
                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + (val==null || val=='null' ? '' : val) + '</td>'
                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + (val==null || val=='null' ? '' : val) + '</td>'
                                        }
                                        
                                    }
                                    count++;
                                } 
                            }
                            if (isfooter==true) {
                                isfooter = false;
                                ftr = '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>';
                            }
                            else {
                                ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                            }
                        }
                        htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + jsonRes[z].reports[t].reportfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                    }
                }

                var mailOptions = {
                    from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                    to: jsonRes[z].special_recipients[i].email,
                    subject: jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname,
                    html: htm,
                };
                if (oktosend==true) {
                    transport.sendMail(mailOptions, (error, info) => {
                        if (error) {
                           console.log(error);
                        }
                    });
                }
                else {
                    console.log('empty special');
                }
            }

            //NOTE: HR Recipient 
            var rowsEmpFiltered =  jsonRes[z].HR_recipients.filter(row => !filterEmp.includes(row.nik));
            for(var i=0;i<rowsEmpFiltered.length;i++) {
                var htm = '';
                var oktosend = false;
                htm = '<head>' +
                        '<style>' +
                        'table {' +
                            'font-family: arial, sans-serif;' +
                            'border-collapse: collapse;' +
                            'width: 100%;' +
                        '}' +
                          
                        'td, th {' +
                            'border: 1px solid #dddddd;' +
                            'padding: 8px;' +
                        '}' +
                          
                        'tr:nth-child(even) {' +
                            'background-color: #dddddd;' +
                        '}' +
                        
                       
                        '</style>' +
                        '</head>'; 
                        htm = htm + '<b>Kepada Bapak/Ibu ' + jsonRes[z].special_recipients[i].nama + '</b><p/>';
                        htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                for (var t=0;t<jsonRes[z].reports.length;t++) {
                    if (rowsEmpFiltered[i].email!=null && rowsEmpFiltered[i].email!='' && rowsEmpFiltered[i].email!=undefined) {
                        
                        var hdr = '';
                        if (jsonRes[z].reports[t].rows.length>0) {
                            htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + jsonRes[z].reports[t].reportheader + '<br/>'
                            hdr = '<tr>'
                            for (var key in jsonRes[z].reports[t].rows[0]) {
                                var found = false;
                                var count = 0;
                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                {
                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter) {
                                        found = true;
                                        hdr = hdr + '<th bgcolor="#2b471d">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                    }
                                    count++;
                                }
                            }
                            hdr = hdr + '</tr>'
                        }
              
                        var rows = jsonRes[z].reports[t].rows;
                        var rowsfiltered = rows;
                        if (rowsEmpFiltered[i].ho=='n') {
                            var plantcolname = jsonRes[z].reports[t].plantcolumn;
                            rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[i].id_gedung);
                        }
                        plantSelected.push('KMTR');
                        var ctnt = '';
                        var ftr = '';
                        var rw = '';
                        var isfooter = false;

                        var checkfooter = false;
                        if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                            checkfooter = true;
                        }

                        for (var h=0;h<rowsfiltered.length;h++) {
                            oktosend = true;
                            var bgcolor = "#F5F5F5";
                            rw = '';
                            if (checkfooter==true) {
                                for (var key in rowsfiltered[h]) {
                                    var val = rowsfiltered[h][key];
                                    var found = false;
                                    var count = 0;
                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                        if (key==jsonRes[z].reports[t].isfooter) {
                                            if (val==1 || val=='1' || val==true) {
                                                bgcolor = "#2b471d";
                                                isfooter = true;
                                                found = true;
                                            }  
                                        }
                                        count++;
                                    }
                                }
                            }
                            for (var key in rowsfiltered[h]) {
                                var val = rowsfiltered[h][key];
                                var found = false;
                                var count = 0;
                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                {
                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname) {
                                        found = true;
                                        if (val!=null && val!=undefined) {
                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                case 0:
                                                    val = val;
                                                    break;
                                                case 1:
                                                    val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                    break;
                                                case 2:
                                                    val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                    break;
                                                case 3:
                                                    val = helper.formatDateShort(val)
                                                    break;
                                                case 4:
                                                    val = helper.formatDate(val)
                                                    break;
                                            }
                                        }
                                        var bgcolor2 = bgcolor;
                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                            if (isfooter==false) 
                                                bgcolor2 = "#b5ddc3";
                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + (val==null || val=='null' ? '' : val) + '</td>'
                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + (val==null || val=='null' ? '' : val) + '</td>'
                                        }
                                    }
                                    count++;
                                }
                            }
                            if (isfooter==true) {
                                isfooter = false;
                                ftr = '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>';
                            }
                            else {
                                ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                            }
                        }
                        htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + jsonRes[z].reports[t].reportfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                    }
                }
                var mailOptions = {
                    from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                    to: rowsEmpFiltered[i].email,
                    subject: jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname,
                    html: htm,
                };
                if (oktosend==true) {
                    transport.sendMail(mailOptions, (error, info) => {
                        if (error) {
                           console.log(error)
                        }
                    });
                }
                else {
                    console.log('empty HR');
                }
            }
            
            var MongoClient = mongo.MongoClient;
            
            //LOGs
            if (forceRun==true) {
                MongoClient.connect(url.getDbString(), function(err, db) {
                    if (err) {
                        db.close();
                        return;
                    }
                    else {
                        var lastrun = new Date();
                        var myobj = { 
                            topiccode : topiccode,
                            topicname : topicname,
                            emaillastrun : null,
                            statusemail : null,
                            emailforcerun : lastrun,
                            statusemailforcerun : 'Success',
                            msg1lastrun : null,
                            statusmsg1 : null,
                            msg1forcerun : null,
                            statusmsg1forcerun : null,    
                            lastmodified : lastrun,
                        };
                        var dbo = db.db(dbname.getDbName());
                        dbo.collection("t_logs").insertOne(myobj, function(err, result) {
                            if (err) {
                                db.close();
                                return;
                            }
                        });
                    }
                });
            }
            else {
                MongoClient.connect(url.getDbString(), function(err, db) {
                    if (err) {
                        db.close();
                        return;
                    }
                    else {
                        var lastrun = new Date();
                        var myobj = { 
                            topiccode : topiccode,
                            topicname : topicname,
                            emaillastrun : lastrun,
                            statusemail : 'Success',
                            emailforcerun : null,
                            statusemailforcerun : null,
                            msg1lastrun : null,
                            statusmsg1 : null,
                            msg1forcerun : null,
                            statusmsg1forcerun : null,    
                            lastmodified : lastrun,
                        };
                        var dbo = db.db(dbname.getDbName());
                        dbo.collection("t_logs").insertOne(myobj, function(err, result) {
                            if (err) {
                                db.close();
                                return;
                            }
                            else {
                                var myquery = { _id : topicid};
                                var newvalues = { $set: 
                                    {
                                        emaillastrun : lastrun,
                                        emailsuccess : 'Y'
                                    } 
                                };
                                dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                                    if (err) {
                                        db.close();
                                        return;
                                    }
                                    else {
                                        db.close();
                                        return;
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }

    function filterArray(array, filters) {
        const filterKeys = Object.keys(filters);
        return array.filter(item => {
          // validates all filter criteria
          return filterKeys.every(key => {
            // ignores non-function predicates
            if (typeof filters[key] !== 'function') return true;
            return filters[key](item[key]);
          });
        });
    }

    /*
    const filters = {
        size: size => size === 50 || size === 70,
        color: color => ['blue', 'black'].includes(color.toLowerCase()),
        locations: locations => locations.find(x => ['JAPAN', 'USA'].includes(x.toUpperCase())),
        details: details => details.length < 30 && details.width >= 70,
      };
    */