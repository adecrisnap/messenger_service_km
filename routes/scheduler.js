//batas transfer bill scheduler 500rb rupiah
var conf2 = require('../config/dbPool.js');
var auth = require('../middlewares/auth.js');
var async = require('async');
var cCode = require('../middlewares/challengeCode.js');
var pool = conf2.createPool();
var axios = require('axios');
var schedule = require('node-schedule');

/*
//var j = nschedule.scheduleJob( '/5 * * * *', function(){
    pool.getConnection(function (err,connection){
        if (err) {
            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
            auth.setResponse(res,400,err);
            return;
        }
        else {
            connection.query('select tox.id from t_orders tox inner join ' +
                't_customers tc on tox.customer_id=tc.id and tc.customer_type=1 ' +
                'where tox.is_paid=0 and tox.statusid not in (19)',function(err,rows){
                    if (err) {
                        if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                        auth.setResponse(res,400,err);
                        return;
                    }
                    else {

                    }
            })
        }
    });
});
*/

//FIXME: Belum bisa dipakai
var rule = new schedule.RecurrenceRule();
rule.hour = 7;
rule.dayOfWeek = 5;

var j = schedule.scheduleJob(rule, function(){
    //MULTIPLE LOGISTIK BROOO.....

    var invoiceheaders = [];
    pool.getConnection(function (err,connection){
        if (err) {
            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
            auth.setResponse(res,400,err);
            return;
        }
        else {
            //console.log('o ' + ordersx)
            connection.beginTransaction(function(err){
                if (err) {
                     if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                    auth.setResponse(res,400,err);
                    return;
                }
                else {
                    //get yg is_paid=1 dan is_billed=0 and orders in order by customer_id
                    var sql =
                'select partner_logistics_id,id,orderid,customer_id,customer_name,actual_shipment_date,partner_logistics_actual_fare from (' +
                    'select ' +
                    'tdl.partner_logistics_id,' +
                    'tdj.order_id as id,' +
                    'tdl.request_id as orderid,' +
                    'tg.id as customer_id,' +
                    'tg.partner_group_name as customer_name,' +
                    'tdl.request_date as actual_shipment_date,' +
                    'tdl.actual_fare as partner_logistics_actual_fare,' +
                    '0 as actual_weight ' +
                'from ' +
                    't_orders tox inner join ' +
                    't_order_direct_to_logistics tdl on tox.id=tdl.order_id inner join ' +
                    't_driver_jobs tdj on tdl.job_id=tdj.id and tdj.order_id=tox.id and tdj.status_id in (9) and tdl.status_id in (9) inner join ' +
                    't_partner_logistics tl on tl.id=tdl.partner_logistics_id inner join ' +
                    't_partner_groups tg on tg.id=tl.partner_group_id ' +
                'where ' +
                    'tox.is_paid=1 and ' +
                    'tox.is_billed=0 ' +
                'union all ' +

                'select ' +
                    'tox.partner_logistics_id,' +
                    'tox.id,' +
                    'tox.orderid,' +
                    'tox.customer_id,' +
                    'tc.customer_name,' +
                    'tox.actual_shipment_date,' +
                    'tox.partner_logistics_actual_fare,' +
                    'tox.actual_weight ' +
                'from ' +
                    't_orders tox inner join ' +
                    't_partner_logistics tpw on tpw.id=tox.partner_logistics_id inner join ' +
                    't_partner_groups tg on tg.id=tpw.partner_group_id inner join ' +
                    't_customers tc on tc.id=tox.customer_id ' +
                'where ' +
                    'tox.is_paid=1 and ' +
                    '(tox.order_type=1 or tox.order_type=4) and tc.customer_type=2 and tox.is_billed=0' +
                ') as tbl order by partner_logistics_id,customer_id';


                    connection.query(sql, function(err,rows){
                        if (err) {
                            connection.rollback(function() {
                                if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                auth.setResponse(res,400,err);
                                return;
                            });
                        }
                        else {
                            total_invoice = 0;
                            var newcustomer = 0;
                            var oldcustomer = 0;

                            if (rows.length>0) {
                                oldcustomer = rows[0].customer_id;
                                newcustomer = rows[0].customer_id;
                                var dt = new Date();
                                var inv = 'INV-' + dt.getFullYear().toString() + '-' + dt.getMonth().toString() + '-' + dt.getDate().toString() + '-' + cCode.makeid().toUpperCase();
                                var sql = 'insert into t_order_invoice_headers ' +
                                    '(from_partner_logistics_id,invoice_type,invoice_id,invoice_date,customer_id,invoice_to,invoice_to_address,total_amount,tax_amount,' +
                                    'discount_amount,created_by,created_date,status) values ' +
                                    '(?,1,?,Now(),?,?,\'\',0,0,0,1,Now(),1)';
                                //Now()
                                connection.query(sql, [rows[0].partner_logistics_id,inv,oldcustomer,rows[0].customer_name], function(err,rowsx){
                                    if (err) {
                                        connection.rollback(function() {
                                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                            auth.setResponse(res,400,err);
                                            return;
                                        });
                                    }
                                    else {
                                        idx = rowsx.insertId;
                                        var customer_name = '';
                                        async.eachSeries(rows, function(obj,callback){
                                            newcustomer = obj.customer_id;
                                            customer_name = obj.customer_name;
                                            if (oldcustomer===newcustomer) {
                                                //insert detail

                                                var sqlx = 'insert into t_order_invoice_details (invoice_id,order_id,' +
                                                    'order_amount,partner_logistics_id,created_by,created_date) ' +
                                                    'values (?,?,?,?,1,Now())';
                                                connection.query(sqlx, [idx,obj.id,obj.partner_logistics_actual_fare,obj.partner_logistics_id], function(err,rowsy){
                                                    if (err) {
                                                        return callback(err);
                                                    }
                                                    else {
                                                        total_invoice+=obj.partner_logistics_actual_fare;
                                                        var sqly = 'update t_orders set is_billed=1 where id=?';
                                                        connection.query(sqly, [obj.id], function(err,rowsz){
                                                            if (err) {
                                                                return callback(err);
                                                            }
                                                            else
                                                                return callback();
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                oldcustomer = newcustomer;
                                                var sqlz = 'update t_order_invoice_header set total_amount=? where id=?';
                                                connection.query(sqlz, [total_invoice,idx], function(err,rowsz1){
                                                    if (err) {
                                                        return callback(err);
                                                    }
                                                    else {

                                                        var sales_invoice = { 'sales_invoice' :  {
                                                                "transaction_date": new Date(),
                                                                "transaction_lines_attributes": [
                                                                {
                                                                    "quantity": 1,
                                                                    "rate": total_invoice,
                                                                    "discount": null,
                                                                    "product_name": "Penjualan",
                                                                    "line_tax_id": null,
                                                                    "line_tax_name": "ppn"
                                                                }
                                                                ],
                                                                "is_shipped": true,
                                                                "person_name": customer_name,
                                                                "transaction_no": inv,
                                                                "custom_id": idx
                                                            }
                                                        }
                                                        invoiceheaders.push(sales_invoice);

                                                        total_invoice = 0;
                                                        inv = 'INV-' + dt.getFullYear().toString() + '-' + dt.getMonth().toString() + '-' + dt.getDate().toString() + '-' + cCode.makeid();
                                                        var sqlt = 'insert into t_order_invoice_headers ' +
                                                            '(from_partner_logistics_id,invoice_type,invoice_id,invoice_date,customer_id,invoice_to,invoice_to_address,total_amount,tax_amount,' +
                                                            'discount_amount,created_by,created_date,status) values ' +
                                                            '(?,1,?,Now(),?,?,\'\',0,0,0,1,Now(),1)';
                                                        //Now()


                                                        connection.query(sqlt, [obj.partner_logistics_id,inv,oldcustomer,obj.customer_name], function(err,rowsz2){
                                                            if (err) {
                                                                return callback(err);
                                                            }
                                                            else {
                                                                idx = rowsz2.insertId;
                                                                var sqlxx = 'insert into t_order_invoice_details (invoice_id,order_id,' +
                                                                'order_amount,partner_logistics_id,created_by,created_date) ' +
                                                                'values (?,?,?,?,1,Now())';
                                                                connection.query(sqlxx, [idx,obj.id,obj.partner_logistics_actual_fare,obj.partner_logistics_id], function(err,rowsy){
                                                                    if (err) {
                                                                        return callback(err);
                                                                    }
                                                                    else {
                                                                        total_invoice+=obj.partner_logistics_actual_fare;
                                                                        var sqly = 'update t_orders set is_billed=1 where id=?';
                                                                        connection.query(sqly, [obj.id], function(err,rowsz){
                                                                            if (err) {
                                                                                return callback(err);
                                                                            }
                                                                            else
                                                                                return callback();
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                })
                                            }
                                        }, function(err){
                                            if (err) {
                                                connection.rollback(function() {
                                                    if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                                    auth.setResponse(res,400,err);
                                                    return;
                                                });
                                            }
                                            else {
                                                var sqlz = 'update t_order_invoice_headers set total_amount=? where id=?';
                                                connection.query(sqlz, [total_invoice,idx], function(err,rowsz1){
                                                    if (err) {
                                                        connection.rollback(function() {
                                                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                                            auth.setResponse(res,400,err);
                                                            return;
                                                        });
                                                    }
                                                    else {

                                                        var sales_invoice = { 'sales_invoice' :  {
                                                                "transaction_date": new Date(),
                                                                "transaction_lines_attributes": [
                                                                {
                                                                    "quantity": 1,
                                                                    "rate": total_invoice,
                                                                    "discount": null,
                                                                    "product_name": "Penjualan",
                                                                    "line_tax_id": null,
                                                                    "line_tax_name": "ppn"
                                                                }
                                                                ],
                                                                "is_shipped": true,
                                                                "person_name": customer_name,
                                                                "transaction_no": inv,
                                                                "custom_id": idx
                                                            }
                                                        }
                                                        invoiceheaders.push(sales_invoice);
                                                        var data = {
                                                            "sales_invoices": invoiceheaders
                                                        }
                                                        if (par)
                                                        cCode.createSalesInvoiceToJournalBulk2(data).then(function(result){
                                                            if (result.status==200 || result.status==201) {
                                                                connection.commit(function(err) {
                                                                    if (err) {
                                                                        connection.rollback(function() {
                                                                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                                                            auth.setResponse(res,400,err);
                                                                            return;
                                                                        });
                                                                    }
                                                                    else {
                                                                        if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                                                        return res.status(200).json({message : 'Success'});
                                                                    };
                                                                });
                                                            }
                                                            else {
                                                                connection.rollback(function() {
                                                                    if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                                                    auth.setResponse(res,400,err);
                                                                    return;
                                                                });
                                                            }
                                                        })

                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                connection.commit(function(err) {
                                    if (err) {
                                        connection.rollback(function() {
                                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                            auth.setResponse(res,400,err);
                                            return;
                                        });
                                    }
                                    else {
                                        //no invoice is created
                                        if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                        return res.status(200).json({message : 'Success'});
                                    };
                                });
                            }
                        }
                    });
                }
            });
        }
    });


});
