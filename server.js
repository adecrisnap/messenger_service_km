﻿var mongo = require('mongodb');
var url = require('./config/mgConfig.js');
var auth = require('./middlewares/auth.js');
var helper = require('./middlewares/helper');
var ObjectId = require('mongodb').ObjectId;
var dbname = require('./config/dbPool');
var express = require('express');
var logger = require('morgan');
var helmet = require('helmet');
var RateLimit = require('express-rate-limit');
var bodyParser = require('body-parser');
var compression = require('compression');
const sql = require('mssql');
const Queue = require('bull');
//var myClient = require('./routes/onesignallib');
const fs = require('fs');
const https = require('https');
var axios = require('axios');
var apikey = 'acb22b95e5ec8f0f14ab2d39a01a4320'; //sandbox
//var apikey = 'c1d7d6822ff1fb0fbaca92a07b954fb8'; //production
//var url = 'https://api.jurnal.id/core/api/v1'; //production
var async = require('async');
var app = express();
const nodemailer = require("nodemailer");
var trsprt = null
var rstMail1 = null;
/*
var transport = nodemailer.createTransport({
    pool: true,
    //maxConnections: 1,
    host: "mail.kiranamegatara.com",
    port: 465,
    secure: true, // use TLS
    auth: {
      user: "no-reply@kiranamegatara.com",
      pass: "1234567890"
    }
});
*/
var transport2 = null;
app.use(compression());
//var cors = require('cors');
//app.use(cors());

//server production
/*
const privateKey = fs.readFileSync('/home/adminpdx/pdxengine/nodesvrekasir/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/home/adminpdx/pdxengine/nodesvrekasir/cert.pem', 'utf8');
const ca = fs.readFileSync('/home/adminpdx/pdxengine/nodesvrekasir/chain.pem', 'utf8');
const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};
*/

var path = require('path');
app.use('/customerprofile', express.static(path.join(__dirname, 'customerprofile')));

var limiter = new RateLimit({
    windowMs: 60*1000,//15*60*1000, // 15 minutes 
    max: 1200, // limit each IP to 100 requests per windowMs 
    delayMs: 0 // disable delaying - full speed until the max limit is reached 
});

//var pool2 = conf3.createPool();
//var session = require('express-session');
//var MySQLStore = require('express-mysql-session')(session);
/*
var options = {
    checkExpirationInterval: 1000 * 60 * 15,// 15 min // How frequently expired sessions will be cleared; milliseconds.
    expiration: 1000 * 60 * 60 * 24 * 7,// 1 week // The maximum age of a valid session; milliseconds.
    createDatabaseTable: true,// Whether or not to create the sessions database table, if one does not already exist.
    //endConnectionOnClose: !connection,
    charset: 'utf8mb4_bin',
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
};
var sessionStore = new MySQLStore(options,pool2); 
app.use(session({
    key: 'session_cookie_name',
    secret: 'session_cookie_secret',
    store: sessionStore,
    resave: true,
    saveUninitialized: true,
    cookie: {  httpOnly: true,  secure: false  }  //secure TRUE kalau pakai https
}));
*/
/*
app.use(cors({
    origin:['http://localhost:1338'],
    methods:['GET','POST'],
    credentials: true // enable set cookie
}));
*/

var scheduler = require('node-schedule');

var j = scheduler.scheduleJob('*/10 * * * *', function(){

    var MongoClient = mongo.MongoClient;
    MongoClient.connect(url.getDbString(), function(err, db) {
        if (err) {
            //db.close();
            writeErrorLog('Step 1',err);
            //auth.setResponse(res,400,err);        
            return;
        }
        else {
            var jsonTopFinal = [];
            var dbo = db.db(dbname.getDbName());
            dbo.collection("t_topics")
                .aggregate([
                    { 
                        $match:
                        {
                            is_active : 1
                        },
                    },
                    {
                        $project: 
                        {
                            _id : true,
                            topicname : true,
                            topiccode : true,
                            reports : true,
                            emailroles : true,
                            emailscheduleselected : true,
                            emailsuccess : true,
                            emaillastrun : true,
                            todaydate : { $dayOfMonth: new Date() },
                            todayhour : { $hour : { date : new Date(), timezone: "+07"}},
                            todayhour2 : { $hour : { date : '$startdate', timezone: "+07"}},
                            todayminute : { $minute : { date : new Date(), timezone: "+07"}},
                            todayminute2 : { $add: [ {$minute : { date : '$startdate', timezone: "+07"}}, 5]},
                            todayminute3 : { $add: [ {$minute : { date : '$startdate', timezone: "+07"}}, -5]},
                            daymonth : {
                                 $dayOfMonth:  '$emaillastrun'

                            },
                            lastonehours : {
                            
                                $divide: 
                                    [    
                                        { 
                                            $subtract: [new Date(), '$emaillastrun'] 
                                        },  1000 * 60 * 60 * 1
                                    ]        
                            
                            },
                            lasteighthours : {
                            
                                $divide: 
                                    [    
                                        { 
                                            $subtract: [new Date(), '$emaillastrun'] 
                                        },  1000 * 60 * 60 * 8
                                    ]        
                            
                            },
                            lastfivehours : {
                                    
                                $divide: 
                                    [    
                                        { 
                                            $subtract: [new Date(), '$emaillastrun'] 
                                        },  1000 * 60 * 60 * 5
                                    ]        
                            
                            },
                            lastfourhours : {
                            
                                $divide: 
                                    [    
                                        { 
                                            $subtract: [new Date(), '$emaillastrun'] 
                                        },  1000 * 60 * 60 * 4
                                    ]        
                            
                            },
                            lastrunday : {
                            
                                        $divide: 
                                            [    
                                                { 
                                                    $subtract: [new Date(), '$emaillastrun'] 
                                                },  1000 * 60 * 60 * 24
                                            ]        
                                    
                            },
                            lastrunweek : {
                                
                                        $divide: 
                                            [
                                                {$divide: 
                                                    [
                                                        { 
                                                            $subtract: [new Date(), '$emaillastrun'] 
                                                        },  1000 * 60 * 60 * 24
                                                    ]
                                                }, 7
                                            ]            
                                
                            },
                            lastrunmonth : {
                                
                                        $divide:
                                        [
                                            {$divide: 
                                                [
                                                    { 
                                                        $subtract: [new Date(), '$emaillastrun'] 
                                                    },  (1000 * 60 * 60 * 24)
                                                ]
                                            }, 30
                                        ]    
                                    
                            },
                            lastrunmonthfeb : {
                                
                                $divide:
                                [
                                    {$divide: 
                                        [
                                            { 
                                                $subtract: [new Date(), '$emaillastrun'] 
                                            },  (1000 * 60 * 60 * 24)
                                        ]
                                    }, 28
                                ]    
                            
                            },
                        },
                        
                    },
                    {
                        $match:
                        { 
                            $or : 
                                [ 
                                    {
                                        $and:
                                            [ 
                                                {lastfivehours : { $gt : 1}, emailscheduleselected :5 }
                                            ]
                                    },
                                    {
                                        $and:
                                            [ 
                                                {lastfourhours : { $gt : 1}, emailscheduleselected :4 }
                                            ]
                                    },
                                    {
                                        $and:
                                            [ 
                                                {lasteighthours : { $gt : 1}, emailscheduleselected :8 }
                                            ]
                                    },
                                    {
                                        $and:
                                            [ 
                                                {lastrunday : { $gt : 1}, emailscheduleselected :0 }
                                            ]
                                    },
                                    {
                                        $and:
                                            [ 
                                                {lastrunweek : { $gt : 1}, emailscheduleselected :1 }
                                            ]
                                    },
                                    {
                                        $and:
                                            [ 
                                                {lastonehours : { $gt : 1}, emailscheduleselected :100 }
                                            ]
                                    },
                                    //{
                                    //    $and:
                                    //        [ 
                                    //            {lastrunmonth : { $gt : 1}, emailscheduleselected :2 }
                                    //        ]
                                    //},
                                    {
                                        $and:
                                            [ 
                                                {
                                                    daymonth : { $eq :  {$dayOfMonth:  new Date()} }, 
                                                    lastrunday : { $gt : 1},
                                                    emailscheduleselected :2,
                                                    $expr:{$eq:["$todayhour", "$todayhour2"]},
                                                    todayminute:{$lte : "$todayminute2"},
                                                    todayminute:{$gte : "$todayminute3"},
                                                }
                                            ]
                                    },
                                    {
                                        $and:
                                            [
                                                {
                                                    todaydate : { $eq : 1}, 
                                                    emailscheduleselected : 99, 
                                                    $expr:{$eq:["$todayhour", "$todayhour2"]},
                                                    todayminute:{$lte : "$todayminute2"},
                                                    todayminute:{$gte : "$todayminute3"},
                                                    //lastrunmonthfeb : { $gt : 1}
                                                }
                                            ] 
                                    },
                                ]
                        },
                    },
                    {   
                        
                        $lookup:
                        {
                            from: 't_reports',
                            localField: 'reports.value',
                            foreignField: '_id',
                            as: 'reportdetails'
                        }
                            
                            
                    },
                    //{
                    //    $sort: {"reportdetails.reportcode": 1}
                    //},
                    {
                        $lookup:
                        {
                            from: 't_roles',
                            localField: 'emailroles.value',
                            foreignField: '_id',
                            as: 'roledetails'
                        },
                    },
                ])
                .toArray(function(errB,result) { 
                    if (errB) {
                        db.close();
                        //console.log('1 ' + err);
                        writeErrorLog('Step 2',errB);
                        //auth.setResponse(res,400,errB);
                        return;
                    }
                    else {
                        console.log(JSON.stringify(result))
                        async.eachSeries(result, function(objT, callback00) {
                            var topics = {
                                _id : new ObjectId(objT._id),
                                topicname : objT.topicname,
                                topiccode : objT.topiccode,
                                reports : [],
                                special_recipients : [],
                                HR_recipients : [],
                                ex_recipients : []
                            }
                            //LOGs
                            var lastrun = new Date();
                                    var myquery = { _id : new ObjectId(objT._id)};
                                    var newvalues = { $set: 
                                        {
                                            emaillastrun : lastrun,
                                            emailsuccess : 'Y'
                                        } 
                                    };
                                    dbo.collection("t_topics").updateOne(myquery, newvalues, function(err, result2) {
                                        if (err) {
                                            writeErrorLog('Step 2b',err);
                                            return callback00(err);
                                        }
                                        else {
                                            async.eachSeries(objT.reportdetails, function(obj1,callback1){
                                                if (obj1.is_active==1) {
                                                    dbo.collection("t_servers").find({_id : new ObjectId(obj1.serveraddress)}).toArray(function(err0,servernames){
                                                        if (err0) {
                                                            writeErrorLog('Step 3',err0);
                                                            return callback1(err0);
                                                        }
                                                        else {
                                                            //exec sp
                                                            var config = null;
                                                            if (servernames[0].servertype==0) { 
                                                                config = {
                                                                    user: servernames[0].username,
                                                                    password: auth.decrypt(servernames[0].pwd),
                                                                    server: servernames[0].serveraddress, 
                                                                    database: servernames[0].dbname,
                                                                    port : 1433,
                                                                    requestTimeout : 360000,
                                                                    connectionTimeout: 15000,
                                                                    pool: {
                                                                        max: 10,
                                                                        min: 0,
                                                                        idleTimeoutMillis: 30000
                                                                      },
                                                                      options: {
                                                                        cryptoCredentialsDetails: {
                                                                            minVersion: 'TLSv1'
                                                                        },
                                                                        encrypt: false, // for azure
                                                                        trustServerCertificate: true // change to true for local dev / self-signed certs
                                                                      }
                                                                }
                                                                //console.log('data ' + JSON.stringify(config))
                                                            }
                                                            sql.connect(config, function (errx) {
                                                                if (errx) {
                                                                    sql.close();
                                                                    var myquery2 = { _id : new ObjectId(objT._id)};
                                                                    var newvalues2 = { $set: 
                                                                        {
                                                                            is_active : 0,
                                                                            emaillastrun : new Date(),
                                                                            emailsuccess : 'F',
                                                                            errorlog : 'Connection error : ' + obj1.functionname,
                                                                            errortime : new Date()
                                                                        } 
                                                                    };
                                                                    dbo.collection("t_topics").updateOne(myquery2, newvalues2, function(errH, result2H) {
                                                                        writeErrorLog('Step 4 - ' + obj1.functionname,'Connection error : ' + obj1.functionname);
                                                                        return callback1(errx);
                                                                    });         
                                                                }
                                                                else {
                                                                    //console.log('ok')
                                                                    var request = new sql.Request();
                                                                    request.execute(obj1.functionname, function (erry, recordset) {
                                                                        if (erry) {
                                                                            sql.close();
                                                                            var myquery2 = { _id : new ObjectId(objT._id)};
                                                                            var newvalues2 = { $set: 
                                                                                {
                                                                                    is_active : 0,
                                                                                    emaillastrun : new Date(),
                                                                                    emailsuccess : 'F',
                                                                                    errorlog : 'Execution error : ' + obj1.functionname,
                                                                                    errortime : new Date()
                                                                                } 
                                                                            };
                                                                            dbo.collection("t_topics").updateOne(myquery2, newvalues2, function(errH, result2H) {
                                                                                writeErrorLog('Step 5 - ' + obj1.functionname,'Execution error : ' + obj1.functionname);
                                                                                return callback1(erry);
                                                                
                                                                            });                                                                                    }
                                                                        else {
                                                                            if (recordset.recordsets[0].length<=0) {
                                                                                var myquery2 = { _id : new ObjectId(objT._id)};
                                                                                var newvalues2 = { $set: 
                                                                                    {
                                                                                        emailsuccess : 'Z',
                                                                                        errorlog : 'Zero row : ' + obj1.functionname
                                                                                    } 
                                                                                };
                                                                                dbo.collection("t_topics").updateOne(myquery2, newvalues2, function(errH, result2H) {
                                                                    
                                                                                });              
                                                                            }
                                                                            sql.close();
                                                                            var query2 = { id_report : obj1._id };
                                                                            dbo.collection("t_columns").find(query2).toArray(function(err1, colnames) {
                                                                                if (err1) {
                                                                                    writeErrorLog('Step 6',err1);
                                                                                    return callback1(err1);
                                                                                }
                                                                                else {
                                                                                    topics.reports.push({
                                                                                        reportname : obj1.reportname,
                                                                                        reportcode : obj1.reportcode,
                                                                                        reportheader : obj1.reportheader,
                                                                                        reportfooter : obj1.reportfooter,
                                                                                        requestedby : obj1.requestedby,
                                                                                        plantcolumn : obj1.plantcolumn,
                                                                                        isfooter : obj1.isfooter,
                                                                                        columnnames : colnames,
                                                                                        isperorganization : (obj1.isperorganization!=null && obj1.isperorganization!=undefined) ? obj1.isperorganization : 0,
                                                                                        isplantvisible : (obj1.isplantvisible!=null && obj1.isplantvisible!=undefined) ? obj1.isplantvisible : 0,
                                                                                        additionalsplitcolumn : (obj1.additionalsplitcolumn!=null && obj1.additionalsplitcolumn!=undefined) ? obj1.additionalsplitcolumn : 0,
                                                                                        additionalcolumn : (obj1.additionalcolumn!=null && obj1.additionalcolumn!=undefined) ? obj1.additionalcolumn : 0,
                                                                                        rows : recordset.recordsets[0]
                                                                                    });
                                                                                    return callback1();
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                else {
                                                    return callback1();
                                                }
                                            }, function(err0){
                                                if (err0) {
                                                    writeErrorLog('Step 7',err0);
                                                    return callback00(err0);
                                                }
                                                else {
                                                    async.eachSeries(objT.roledetails, function(obj2,callback0z){
                                                        if (obj2.is_active==1) {
                                                            dbo.collection("t_servers").find({_id : new ObjectId(obj2.dbserver)}).toArray(function(err4,servernames2){
                                                                if (err4) {
                                                                    writeErrorLog('Step 8',err4);
                                                                    return callback0z(err4);
                                                                }
                                                                else {
                                                                    var config2 = null;
                                                                    //console.log('server type ' + servernames2[0].servertype==0)
                                                                    //if (servernames2[0].servertype==0) { 
                                                                        config2 = {
                                                                            user: servernames2[0].username,
                                                                            password: auth.decrypt(servernames2[0].pwd),
                                                                            server: servernames2[0].serveraddress, 
                                                                            database: servernames2[0].dbname,
                                                                            port : 1433,
                                                                            requestTimeout : 360000,
                                                                            connectionTimeout: 15000,
                                                                            pool: {
                                                                                max: 10,
                                                                                min: 0,
                                                                                idleTimeoutMillis: 30000
                                                                              },
                                                                              options: {
                                                                                cryptoCredentialsDetails: {
                                                                                    minVersion: 'TLSv1'
                                                                                },
                                                                                encrypt: false, // for azure
                                                                                trustServerCertificate: true // change to true for local dev / self-signed certs
                                                                              }
                
                                                                        }
                                                                        //console.log(JSON.stringify(config2))
                                                                    //}
                                                                    sql.connect(config2, function (errx) {
                                                                        if (errx) {
                                                                            sql.close();
                                                                            
                                                                            var myquery2 = { _id : new ObjectId(objT._id)};
                                                                            var newvalues2 = { $set: 
                                                                                {
                                                                                    is_active : 0,
                                                                                    emaillastrun : new Date(),
                                                                                    emailsuccess : 'F',
                                                                                    errorlog : 'Connection error : ' + objT.topicname,
                                                                                    errortime : new Date()
                                                                                } 
                                                                            };
                                                                            dbo.collection("t_topics").updateOne(myquery2, newvalues2, function(errH, result2H) {
                                                                                writeErrorLog('Step 9',errx + ' - Error getting roles. Topic ' + objT.topicname);
                                                                                return callback0z(errx);
                                                                            });
                                                                           
                                                                        }
                                                                        else {
                                                                            //SINI
                                                                            var keyword = obj2.keyword;
                                                                            if (keyword=='') {
                                                                                sql.close();
                                                                                jsonEmp = [];
                                                                                var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                    if (err2) {
                                                                                        writeErrorLog('Step 10',err2);
                                                                                        return callback0z(err2);
                                                                                    }
                                                                                    else {
                                                                                        async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                            topics.special_recipients.push({ 
                                                                                                nik : obj3.nik,
                                                                                                nama : obj3.nama,
                                                                                                email : obj3.email,
                                                                                                plants : obj3.plantselected,
                                                                                                divisi : obj3.divisi,
                                                                                                dept : obj3.dept
                                                                                            });
                                                                                            //send email
                                                                                            return callback3();
                                                                                        }, function(err3){
                                                                                            if (err3) {
                                                                                                writeErrorLog('Step 11',err3);
                                                                                                return callback0z(err3);
                                                                                            }
                                                                                            else {
                                                                                                var activateExclude = 1;
                                                                                                if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                if (activateExclude==0) {
                                                                                                    return callback0z();
                                                                                                }
                                                                                                else {
                                                                                                    var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                    //console.log(JSON.stringify(obj2));
                                                                                                    dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                        if (err2) {
                                                                                                            writeErrorLog('Step 11a',err2);
                                                                                                            return callback0z(err2);
                                                                                                        }
                                                                                                        else {
                                                                                                            async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                topics.ex_recipients.push({ 
                                                                                                                    nik : obj3.nik,
                                                                                                                    nama : obj3.nama,
                                                                                                                    email : obj3.email,
                                                                                                                    plants : obj3.plantselected
                                                                                                                });
                                                                                                                //send email
                                                                                                                return callback3();
                                                                                                            }, function(err3) {
                                                                                                                if (err3) {
                                                                                                                    writeErrorLog('Step 11a',err3);
                                                                                                                    return callback0z(err3);
                                                                                                                }
                                                                                                                else {
                                                                                                                    return callback0z();
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                if (obj2.nonkmdb==null || obj2.nonkmdb==0) {
                                                                                    var request = new sql.Request();
                                                                                    var arrKeyword = keyword.split(';');
                                                                                    var strFilter = '';
                                                                                    var strFilter2 = '';
                                                                                    var strFilter3 = '';
                                                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                                                        request.input(i+"keyword", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                        strFilter+="t1.posst like @" + i + "keyword OR ";
                                                                                    }
                                                                                    //strFilter = strFilter.substring(0,strFilter.length-4);
                                                                                    
                                                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                                                        request.input(i+"keyword1", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                        strFilter2+="t3.nama like @" + i + "keyword1 OR ";
                                                                                    }
                                                                                    //strFilter2 = strFilter2.substring(0,strFilter2.length-4);
                    
                                                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                                                        request.input(i+"keyword2", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                        strFilter3+="t4.nama like @" + i + "keyword2 OR ";
                                                                                    }
                                                                                    strFilter3 = strFilter3.substring(0,strFilter3.length-4);
                                                                                
                                                                                    //var sqlx = 
                                                                                    //console.log('auto ' + sql);
                                                                                    request.query("SELECT t1.*,isnull(t3.nama,'') divisi,isnull(t4.nama,'') dept FROM tbl_karyawan t1 left outer join tbl_user t2 on t1.id_karyawan=t2.id_karyawan left outer join tbl_divisi t3 on t2.id_divisi=t3.id_divisi left outer join tbl_departemen t4 on t2.id_departemen=t4.id_departemen  WHERE t1.na='n' and t1.email<>'' and (" + strFilter + ' ' + strFilter2 + ' ' + strFilter3 + ')', function (err, recordsetx) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            var myquery2 = { _id : new ObjectId(objT._id)};
                                                                                            var newvalues2 = { $set: 
                                                                                                {
                                                                                                    is_active : 0,
                                                                                                    emaillastrun : new Date(),
                                                                                                    emailsuccess : 'F',
                                                                                                    errorlog : 'Connection error : ' + objT.topicname,
                                                                                                    errortime : new Date()
                                                                                                } 
                                                                                            };
                                                                                            dbo.collection("t_topics").updateOne(myquery2, newvalues2, function(errH, result2H) {
                                                                                                writeErrorLog('Step 12',err + ' - Error getting employees. Topic ' + objT.topicname);
                                                                                                return callback0z(err); 
                                                                                            });
                                                                                        }
                                                                                        else {
                                                                                            sql.close();
                                                                                            topics.HR_recipients.push(recordsetx.recordsets[0]);
                                                                                            var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                            dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                if (err2) {
                                                                                                    writeErrorLog('Step 13',err2);
                                                                                                    return callback0z(err2);
                                                                                                }
                                                                                                else {
                                                                                                    async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                        topics.special_recipients.push({ 
                                                                                                            nik : obj3.nik,
                                                                                                            nama : obj3.nama,
                                                                                                            email : obj3.email,
                                                                                                            plants : obj3.plantselected,
                                                                                                            divisi : obj3.divisi,
                                                                                                            dept : obj3.dept
                                                                                                        });
                                                                                                        //send email
                                                                                                        return callback3();
                                                                                                    }, function(err3){
                                                                                                        if (err3) {
                                                                                                            writeErrorLog('Step 14',err3);
                                                                                                            return callback0z(err3);
                                                                                                        }
                                                                                                        else {
                                                                                                            var activateExclude = 1;
                                                                                                            if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                            if (activateExclude==0) {
                                                                                                                return callback0z();
                                                                                                            }
                                                                                                            else {
                                                                                                                var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                                //console.log(JSON.stringify(obj2));
                                                                                                                dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                                    if (err2) {
                                                                                                                        writeErrorLog('Step 11a',err2);
                                                                                                                        return callback0z(err2);
                                                                                                                    }
                                                                                                                    else {
                                                                                                                        async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                            topics.ex_recipients.push({ 
                                                                                                                                nik : obj3.nik,
                                                                                                                                nama : obj3.nama,
                                                                                                                                email : obj3.email,
                                                                                                                                plants : obj3.plantselected
                                                                                                                            });
                                                                                                                            //send email
                                                                                                                            return callback3();
                                                                                                                        }, function(err3) {
                                                                                                                            if (err3) {
                                                                                                                                writeErrorLog('Step 11a',err3);
                                                                                                                                return callback0z(err3);
                                                                                                                            }
                                                                                                                            else {
                                                                                                                                return callback0z();
                                                                                                                            }
                                                                                                                        });
                                                                                                                    }
                                                                                                                });
                                                                                                            }
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                                else {
                                                                                    var arrKeyword = keyword.split(';');
                                                                                    var strFilter1 = '';
                                                                                    var strFilter2 = '';
                                                                                
                                                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                                                        request.input(i+"keyword1", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                        strFilter1+="t1.posisi like @" + i + "keyword1 OR ";
                                                                                    }
                                                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                                                        request.input(i+"keyword2", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                        strFilter2+="t1.divisi like @" + i + "keyword2 OR ";
                                                                                    }
                                                                                    //strFilter2 = strFilter2.substring(0,strFilter2.length-4);
                                                                                    for(var i=0;i<arrKeyword.length;i++) {
                                                                                        request.input(i+"keyword3", sql.VarChar, '%' + arrKeyword[i] + '%');
                                                                                        strFilter3+="t1.dept like @" + i + "keyword3 OR ";
                                                                                        //strFilter3+="@" + i + "keyword2 like " + " t4.nama OR ";
                                                                                    }
                                                                                    strFilter3 = strFilter3.substring(0,strFilter3.length-4);
                                                                                    var sqlw =  "SELECT t1.* FROM rpt.vw_KaryawanDivisi t1  WHERE nik=@nik and email<>'' and " + strFilter1+ " " + strFilter2 + " " + strFilter3
                                                                                    request.query(sqlw, function (err, recordset) {//request.query("SELECT t1.id_divisi,t1.divisi,t1.nik,t1.nama,t1.email,t1.posisi posst,\'\' dept FROM vw_KaryawanDivisi t1  WHERE " + strFilter1+ " " + strFilter2, function (err, recordset) {
                                                                                        if (err) {
                                                                                            sql.close();
                                                                                            writeErrorLog('Step 12a - SQL Execute',err);
                                                                                            return callback0z(err); 
                                                                                        }
                                                                                        else {
                                                                                            sql.close();
                                                                                            topics.HR_recipients.push(recordsetx.recordsets[0]);
                                                                                            var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                            dbo.collection("t_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                if (err2) {
                                                                                                    writeErrorLog('Step 13 - Role members',err2);
                                                                                                    return callback0z(err2);
                                                                                                }
                                                                                                else {
                                                                                                    async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                        topics.special_recipients.push({ 
                                                                                                            nik : obj3.nik,
                                                                                                            nama : obj3.nama,
                                                                                                            email : obj3.email,
                                                                                                            plants : obj3.plantselected,
                                                                                                            posisi : obj3.posisi,
                                                                                                            divisi : obj3.divisi,
                                                                                                            dept : obj3.dept
                                                                                                        });
                                                                                                        //send email
                                                                                                        return callback3();
                                                                                                    }, function(err3){
                                                                                                        if (err3) {
                                                                                                            writeErrorLog('Step 14 - Summary',err3);
                                                                                                            return callback0z(err3);
                                                                                                        }
                                                                                                        else {
                                                                                                            var activateExclude = 1;
                                                                                                            if (obj2.activateExclude!=null && obj2.activateExclude!=undefined) activateExclude = obj2.activateExclude;
                                                                                                            if (activateExclude==0) {
                                                                                                                return callback0z();
                                                                                                            }
                                                                                                            else {
                                                                                                                var query1 = { roleid : new ObjectId(obj2._id) };
                                                                                                                dbo.collection("t_exclude_role_members").find(query1).toArray(function(err2, rolemembers) {
                                                                                                                    if (err2) {
                                                                                                                        writeErrorLog('Step 11a',err2);
                                                                                                                        return callback0z(err2);
                                                                                                                    }
                                                                                                                    else {
                                                                                                                        async.eachSeries(rolemembers, function(obj3,callback3){
                                                                                                                            topics.ex_recipients.push({ 
                                                                                                                                nik : obj3.nik,
                                                                                                                                nama : obj3.nama,
                                                                                                                                email : obj3.email,
                                                                                                                                plants : obj3.plantselected,
                                                                                                                                posisi : obj3.posisi
                                                                                                                            });
                                                                                                                            //send email
                                                                                                                            return callback3();
                                                                                                                        }, function(err3) {
                                                                                                                            if (err3) {
                                                                                                                                writeErrorLog('Step 11a',err3);
                                                                                                                                return callback0z(err3);
                                                                                                                            }
                                                                                                                            else {
                                                                                                                                return callback0z();
                                                                                                                            }
                                                                                                                        });
                                                                                                                    }
                                                                                                                });
                                                                                                            }
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                            
                                                        }
                                                        else {
                                                            return callback0z();
                                                        }
                                                    }, function(err2){
                                                        if (err2) {
                                                            writeErrorLog('Step 15',err2);
                                                            return callback00(err2);
                                                        }
                                                        else {
                                                            jsonTopFinal.push(topics);
                                                            return callback00();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                            //    }
                            //});

                        }, function(errG){
                            if (errG) {
                                db.close();
                                writeErrorLog('Step 16',errG);
                                //console.log('2 ' + errG);
                                return;
                            }
                            else {
                                sendEmail(jsonTopFinal,false);
                                return;
                            }
                        });
                    }
                }
                
            );
        }
    });
});


function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

var x = scheduler.scheduleJob('*/60 * * * *', function(){

 const dtt = new Date()
    const bulan = dtt.getMonth()+1
    const tahun = dtt.getFullYear()
    const role_id = null
    var tempbln = null
    if (bulan==12)
        tempbln = 1
    else
        tempbln = bulan+1
    var dt1x = new Date(tahun + '-' + bulan + '-1');
    var dt1 = new Date(dt1x.setMonth(dt1x.getMonth()-1));
    var dt2x = addDays(new Date(tahun + '-' + tempbln + '-1'),-1)
    var dt2 = new Date(dt2x.setMonth(dt2x.getMonth()-1));

    var MongoClient = mongo.MongoClient;
    MongoClient.connect(url.getDbString(), function(err, db) {
        if (err) {
            console.log(err)
            db.close();     
            return;
        }
        else {
            var query;
            query = {};
            var dbo = db.db(dbname.getDbName());
            dbo.collection("t_kpi_server").find(query).toArray(function(err, result) {
                if (err) {
                    console.log(err)
                    db.close();
                    return;
                }
                else {
                    db.close();
                    var username = result[0].username;
                    var pwd = auth.decrypt(result[0].pwd);
                    var dbname = result[0].dbname;
                    var serveraddress = result[0].serveraddress;
                    var config = {
                        user: username,
                        password: pwd,
                        server: serveraddress, 
                        database: dbname,
                        port : 1433,
                            pool: {
                                max: 10,
                                min: 0,
                                idleTimeoutMillis: 600000
                                },
                                options: {
                                    encrypt: false, // for azure
                                    trustServerCertificate: true, // change to true for local dev / self-signed certs
                                    connectTimeout : 600000
                                } 
                    };
                    const pool = new sql.ConnectionPool(config, function (err) {
                        if (err) {
                            console.log(err)
                            pool.close();
                            return;
                        }
                        else {
                            var request = new sql.Request(pool);
                            var sqlx = 'select * from rpt.T_KPI_GROUP_2 where is_active=1 order by sequence';
                            request.query(sqlx,function (err, recordsetx){
                                if (err) {
                                    console.log(err)
                                    pool.close();
                                    return;
                                }
                                else {
                                    var jsonArr = []
                                    async.eachSeries(recordsetx.recordsets[0], function(obj,callback){
                                        var jsonRes = {
                                            config : {
                                                user: username,
                                                password: pwd,
                                                server: serveraddress, 
                                                database: dbname,
                                                port : 1433,
                                                pool: {
                                                        max: 10,
                                                        min: 0,
                                                        idleTimeoutMillis: 600000
                                                    },
                                                    options: {
                                                        encrypt: false, // for azure
                                                        trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                        connectTimeout : 600000
                                
                                                    } 
                                            },
                                            level : 2,
                                            kpiid : obj.id,
                                            group_code : obj.group_code,
                                            group_name : obj.group_name,
                                            sp_name : obj.sp_name,
                                            date1 : dt1,
                                            date2 : dt2
                                        }
                                        if (obj.sp_name!=null && obj.sp_name!='') jsonArr.push(jsonRes)
                                        
                                        return callback();
                                    },function(err) {
                                        if (err) {
                                            console.log(err)
                                            pool.close();
                                            return;
                                        }
                                        else {
                                            jsonArr.push({
                                                config : {
                                                    user: username,
                                                    password: pwd,
                                                    server: serveraddress, 
                                                    database: dbname,
                                                    port : 1433,
                                                    pool: {
                                                            max: 10,
                                                            min: 0,
                                                            idleTimeoutMillis: 600000
                                                        },
                                                        options: {
                                                            encrypt: false, // for azure
                                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                            connectTimeout : 600000
                                    
                                                        } 
                                                },
                                                level : 2,
                                                kpiid : '',
                                                group_code : '',
                                                group_name : '',
                                                sp_name : 'rpt.sp_KPI_Global_2',
                                                date1 : dt1,
                                                date2 : dt2,
                                                role_id : role_id
                                            })
                                            var request = new sql.Request(pool);
                                            var sqlx = 'select * from rpt.T_KPI_GROUP_3 where is_active=1 order by sequence';
                                            request.query(sqlx,function (err, recordsetx){
                                                if (err) {
                                                    console.log(err)
                                                    pool.close();
                                                    return;
                                                }
                                                else {
                                                    async.eachSeries(recordsetx.recordsets[0],function(obj,callback){
                                                        var jsonRes = {
                                                            config : {
                                                                user: username,
                                                                password: pwd,
                                                                server: serveraddress, 
                                                                database: dbname,
                                                                port : 1433,
                                                                pool: {
                                                                        max: 10,
                                                                        min: 0,
                                                                        idleTimeoutMillis: 600000
                                                                    },
                                                                    options: {
                                                                        encrypt: false, // for azure
                                                                        trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                                        connectTimeout : 600000
                                                
                                                                    } 
                                                            },
                                                            level : 3,
                                                            kpiid : obj.id,
                                                            group_code : obj.group_code,
                                                            group_name : obj.group_name,
                                                            sp_name : obj.sp_name,
                                                            date1 : dt1,
                                                            date2 : dt2
                                                        }
                                                        if (obj.sp_name!=null && obj.sp_name!='') jsonArr.push(jsonRes)
                                                        
                                                        return callback();
                                                    },function(err){
                                                        if (err) {
                                                            console.log(err)
                                                            pool.close();
                                                            return;
                                                        }
                                                        else {
                                                            jsonArr.push({
                                                                config : {
                                                                    user: username,
                                                                    password: pwd,
                                                                    server: serveraddress, 
                                                                    database: dbname,
                                                                    port : 1433,
                                                                    pool: {
                                                                            max: 10,
                                                                            min: 0,
                                                                            idleTimeoutMillis: 600000
                                                                        },
                                                                        options: {
                                                                            encrypt: false, // for azure
                                                                            trustServerCertificate: true, // change to true for local dev / self-signed certs
                                                                            connectTimeout : 600000
                                                    
                                                                        } 
                                                                },
                                                                level : 3,
                                                                kpiid : '',
                                                                group_code : '',
                                                                group_name : '',
                                                                sp_name : 'rpt.sp_KPI_Global_3',
                                                                date1 : dt1,
                                                                date2 : dt2,
                                                                role_id : role_id
                                                            })
                                                            async.eachSeries(jsonArr,function(obj,callback){
                                                                
                                                                executeKPIQueue.add(obj);
                                                                return callback();
                                                            },function(err){
                                                                if (err) {
                                                                    console.log(err)
                                                                    pool.close();
                                                                    return;
                                                                }
                                                                else {
                                                                    pool.close();
                                                                    return;
                                                                }
                                                            })
                                                        }
                                                    })
                                                    
                                                }
                                            });
                                        }
                                    })
                                    
                                }
                            })
                        }
                    });
                                                
                }
            });
        }
    });
});



var y = scheduler.scheduleJob('07 03 * * *', function() {
    var MongoClient = mongo.MongoClient;
    MongoClient.connect(url.getDbString(), function(err, db) {
        if (err) {
            //db.close();
            writeErrorLog('Step align schedule 1', err)
            //auth.setResponse(res,400,err);
            return;
        }
        else {
            var dbo = db.db(dbname.getDbName());
            dbo.collection("t_topics").find({is_active : 1}).toArray(function(err, result) {
                if (err) {
                    db.close();
                    writeErrorLog('Step align schedule 2', err)
                    //auth.setResponse(res,400,err);
                    return;
                }
                else {
                    async.eachSeries(result, function(obj,callback){
                        var tm = '07:00';
                        var dtnew = obj.emaillastrun;
                        var tmnew = obj.startdate;
                        
                        //console.log(dtnew + ' ' + tmnew);
                        var hr = tmnew.getHours();
                        if (obj.emailscheduleselected==5) {
                            hr = hr-5;
                            dtnew = new Date();
                        }
                        if (obj.emailscheduleselected==4) {
                            hr = hr-4;
                            dtnew = new Date();
                        }
                        if (obj.emailscheduleselected==8) {
                            hr = hr-8;
                            dtnew = new Date();
                        } 
                        if (tmnew!=null && tmnew!=undefined) tm = hr.toString() + ':' + tmnew.getMinutes().toString();
                        //console.log('itu ' + tm);
                        var dt1 = dtnew.getFullYear().toString() + '-' + (dtnew.getMonth()+1).toString() + '-' + dtnew.getDate().toString() + ' ' + tm; 
                        var dt = new Date(dt1);
                        //console.log('ini ' + dt)
                        
                        newvalues = { $set: 
                            {
                                emaillastrun : dt
                            }
                            
                        };
                        dbo.collection("t_topics").updateOne({_id : new ObjectId(obj._id)}, newvalues, function(err, result2) {
                            if (err) {
                                return callback(err);
                            }
                            else {
                                return callback();
                            }
                        });
                    }, function(err3){
                        if (err3) {
                            writeErrorLog('Error update schedule',err3);
                            db.close();
                            //auth.setResponse(res,400,err);
                            return;
                        }
                        else {
                            db.close();
                            return;// res.status(200).json('success');
                        }
                    });
                }
            });
        }
    });
});


var multer = require('multer');
var storagedriverid = multer.diskStorage({
    destination: (req, file, cb) => {
      //cb(null, '/usr/share/pdx.co.id/public_html/driverid')
      cb(null, '/home/adminpdx/pdxengine/nodesvrekasir/driverid')
      //cb(null, './driverid')
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var uploaddriverid = multer({storage: storagedriverid});
app.use(logger('dev'));
app.use(limiter);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
/*
    app.use(session({  
        secret: 'Mysuper_session-secret',  
        cookie: {  httpOnly: true,  secure: true  } 
    }));
*/

app.use(helmet({
        frameguard: {action: 'deny'}
    }));

app.all('/*', function (req, res, next) {
        // CORS headers
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        res.setHeader("Pragma", "no-cache");
        res.setHeader("Expires", 0);

        res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,PATCH');
        // Set custom headers for CORS
        res.header('Access-Control-Allow-Headers', 'Content-Type,Content-type,Accept,X-Access-Token,X-Key,Authorization,authorization,apikey');
        
        //res.header('Access-Control-Allow-Credentials', true);
        //res.setHeader('Last-Modified', (new Date()).toUTCString());
        //console.log('haeras')
        if (req.method == 'OPTIONS') {
            //console.log('haer')
            res.status(200).end();
        } else {
            next();
        }
    });

/*
app.post('/logindriver', function(req, res) {
        //var sess = req.session;
        if(req.body.username && req.body.password){
            var username = req.body.username;
            var password = req.body.password;
        }
        pool.getConnection(function (err, connection) {
            if (err) {
                if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                res.status(400).json({message:"error"});
                return;
            }
            else {
                var sql = 'select if(t1.player_id is null,\'\',t1.player_id) as player_id,t1.driver_pic,tpl.partner_group_id,tst.setting_value,t1.is_driver_on,tpl.logistics_type,tpl.is_centralized_schedule_for_outbound_destination,' + 
                't1.id,tv.vehicle_type_name,t2.id as driver_vehicle_id,tv.max_weight,tv.max_volume,t1.driver_user_name,' +
                't1.driver_password,t1.driver_name,t1.partner_logistics_id,t2.driver_license_plate,t2.vehicle_type_id ' + 
                'from t_drivers as t1 left outer join t_driver_vehicles as t2 on t1.id=t2.driver_id and ' +
                't2.is_active=1 left outer join t_vehicle_types tv on tv.id=t2.vehicle_type_id inner join ' + 
                't_partner_logistics tpl on tpl.id=t1.partner_logistics_id inner join t_settings tst on tst.id=1 ' +
                'where t1.is_active=1 and t1.driver_user_name=?'
                connection.query(sql,username, function (err, rows) {
                    if (err) {
                        if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                        res.status(400).json({message:"error"});
                        return;
                    }
                    else {
                        if (rows.length > 0) {
                            if (password==auth.decrypt(rows[0].driver_password)){
                                // from now on we'll identify the user by the id and the id is the only personalized value that goes into our token
                                
                                //var deviceBody = {
                                //    device_type: 1,
                                //    language: 'en'
                                //};
                                //myClient.addDevice(deviceBody, function (err, httpResponse, data) {
                                //   if (err) {
                                //        if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                //        res.status(400).json({message:"error"});
                                //        return;
                                //    }
                                //    else {
                                //        var player_id = data.id;
                                        var pairkey = 'xx';//cCode.makeid();
                                        var sqlx = 'update t_drivers set pairkey=? where driver_user_name=?';
                                        connection.query(sqlx,[auth.encrypt(pairkey),username],function(err,rowsx){
                                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                            if (err) {
                                                res.status(400).json({message:"error"});
                                                return;
                                            }
                                            else {
                                                var payload = rows[0].driver_user_name + '|' + pairkey;
                                                //sess.sesdriverkey = auth.encrypt(pairkey + '|' + rows[0].driver_user_name);
                                                var token = auth.encrypt(payload);
                                                return res.status(200).json({
                                                    //dataone : data,
                                                    id: rows[0].id, 
                                                    driver_user_name : rows[0].driver_user_name, 
                                                    driver_pic : (rows[0].driver_pic) ? '/driverprofile/' + rows[0].driver_pic : '',
                                                    driver_name : rows[0].driver_name, 
                                                    driver_license_plate : rows[0].driver_license_plate, 
                                                    vehicle_type_id : rows[0]. vehicle_type_id, 
                                                    vehicle_type_name : rows[0].vehicle_type_name,
                                                    driver_vehicle_id : rows[0].driver_vehicle_id, 
                                                    partner_logistics_id : rows[0].partner_logistics_id,
                                                    partner_group_id : rows[0].partner_group_id,  
                                                    max_weight : rows[0].max_weight,
                                                    max_volume : rows[0].max_volume,
                                                    is_driver_on : rows[0].is_driver_on,
                                                    logistics_type : rows[0].logistics_type,
                                                    is_centralized_schedule_for_outbound_destination : rows[0].is_centralized_schedule_for_outbound_destination,
                                                    waiting_time_before_next_schedule : rows[0].setting_value,
                                                    message: "ok",
                                                    player_id : rows[0].player_id, 
                                                    token: token
                                                });    
                                            }
                                        });
                                //    }
                                //});        
                            }
                            else {
                                if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                res.status(401).json({message:"passwords did not match"});
                                return;
                            }
                        }
                        else {
                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                            res.status(404).json({message:"no such user found"});
                            return;
                        }
                    }
                });
            }
        });    
    });
    */
/*
app.post('/apipartneruser/v1/uploaddriverid/', uploaddriverid.single('image'), (req, res, next) => {
    var driver_id = req.body.driver_id;
    if (!req.file) {
        auth.setResponse(res,400,'No file');
        return;
    } 
    else {
        pool.getConnection(function (err,connection){
            if (err) {
                if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                auth.setResponse(res,400,err);
                return;
            }
            else {
                connection.query('update t_drivers set driver_document=? where id=?',[req.file.filename,driver_id],function(err,rows){
                    if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                    if (err) {
                        auth.setResponse(res,400,err);
                        return;
                    }
                    else {
                        if (rows.affectedRows > 0) {
                            res.status(200).json({message:"upload successful"});
                            return;
                        }
                        else {
                            auth.setResponse(res,400,'No user is found');
                            return;
                        }
                    };
                })
            }
        });
    }
});
*/
//app.all('/apimsg/v1/*', [require('./middlewares/validateMsg')]);
app.use('/', require('./routes'));
app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

//server production
/*
    const httpsServer = https.createServer(credentials, app);

    httpsServer.listen(1338, () => {
        console.log('HTTPS Server running on port 1338');
        pool.getConnection(function (err, connection){
            if (err) {
                if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                console.log(err);
                return;
            }
            else {
                var sql = 'select * from t_onesignal_settings limit 1';
                connection.query(sql, function (err, rows) {
                    if (err) {
                        if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                        return;
                    }
                    else {
                        if (rows.length > 0) {
                            auth.appauthkey = rows[0].appauthkey;
                            auth.setappid = rows[0].appid;
                            auth.userauthkey = rows[0].userauthkey;
                        
                            var sql = 'select * from t_firebase_settings limit 1';
                            connection.query(sql, function (err, rows) {
                                if (err) {
                                    if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                    return;
                                }
                                else {
                                    if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                                    if (rows.length > 0) {
                                        auth.apikey = rows[0].apikey;
                                        auth.databaseurl = rows[0].databasename;
                                        return;
                                    }
                                    else {
                                        return;
                                    }
                                }
                            });
                        }
                        else {
                            if (pool._freeConnections.indexOf(connection)==-1) connection.release();
                            return;
                        }    
                    }
                });
            }
        });
    });
*/

//server development
var server = app.listen(1338, function () {
    console.log('Express server listening on port ' + server.address().port);
});

function writeErrorLog(step,err) {
    var MongoClient = mongo.MongoClient;
    MongoClient.connect(url.getDbString(), function(errx, db) {
        if (errx) {
            console.log('error mongodb1 ' + errx)
            //db.close();
            return;
        }
        else {
            var lastrun = new Date();
            var myobj = { 
                step : step,
                error_desc : JSON.stringify(err),
                lastmodified : lastrun,
            };
            var dbo = db.db(dbname.getDbName());
            dbo.collection("t_error_logs").insertOne(myobj, function(err, result) {
                if (err) {
                    console.log('error mongodb2 ' + err)
                    db.close();
                    return;
                }
            });
        }
    });
}

function GetSortOrder(prop) {    
    return function(a, b) {    
        if (a[prop] > b[prop]) {    
            return 1;    
        } else if (a[prop] < b[prop]) {    
            return -1;    
        }    
        return 0;    
    }    
}    

function distinctData(items,field) {
    var lookup = {};
    var result = [];
    for (var item, i = 0; item = items[i++];) {
        var name = item[field];
      
        if (!(name in lookup)) {
          lookup[name] = 1;
          result.push(name);
        }
    }
    return result;
}



function sendEmail(jsonRes,forceRun) {
    
    var filterEmp = [];
   
    //TOPIC ITERATION   
    var plantSelected = []
    var dtnowwtholiday = null;
    var dtnow = null;
    var d9days = null;
    var d3days = null;
    var dyes = null;
    var dyesd = null;
    var d7days = null;
    var d8days = null;
    var d14days = null;
    var d90days = null;
    var d120days = null;
    var d7plusdays = null;
    var d15days = null;
    var d30days = null;
    var dtgl1 = new Date(new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-1')
    //var d6lastmonth = null;
    var tm = new Date();
    
    if (tm.getHours()<19 && tm.getHours()>6) {
        helper.formatDateShortWorkingDay(new Date())
        .then(function(xtnow){
            dtnow = xtnow;
            return helper.formatDateShort(new Date());
        })
        .then(function(xtnow){
            
            dtnowwtholiday = xtnow;
            return helper.formatDateShortWorkingDay(new Date(),-9);
        })
        .then(function(x9days){
            d9days = x9days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-3));
        })
        .then(function(x3days){
            d3days = x3days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-1));
        })
        .then(function(xyes){
            dyes = xyes;
            return helper.formatDateShort(helper.addDays(new Date(),-1));
        })
        .then(function(xyesd){
            dyesd = xyesd;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-7));
        })
        .then(function(x7days){
            d7days = x7days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-30));
        })
        .then(function(x30days){
            d30days = x30days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-8));
        })
        .then(function(x8days){
            d8days = x8days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-90));
        })
        .then(function(x90days){
            d90days = x90days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-120));
        })
        .then(function(x120days){
            d120days = x120days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),7));
        })
        .then(function(x7plusdays){
            d7plusdays = x7plusdays;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-14));
        })
        .then(function(x14days){
            d14days = x14days;
            return helper.formatDateShortWorkingDay(helper.addDays(new Date(),-15));
        })
        .then(function(x15days){
            d15days = x15days;
            for (var z=0;z<jsonRes.length;z++) {
                topicid =  jsonRes[z]._id;
                topiccode = jsonRes[z].topiccode;
                topicname = jsonRes[z].topicname;

                var addcolumn = [];
                var additionalcolumn = '';
                if (jsonRes[z].reports.length>0) {
                    if (jsonRes[z].reports[0].additionalsplitcolumn==1) {
                        addcolumn = distinctData(jsonRes[z].reports[0].rows,jsonRes[z].reports[0].additionalcolumn)
                        additionalcolumn = jsonRes[z].reports[0].additionalcolumn;
                    }
                }
                
                try {
                    filterEmp = [];
                    var special_recipients = removeDuplicates(jsonRes[z].special_recipients);
                    if (addcolumn.length>0) {
                        for (var r=0;r<addcolumn.length;r++) {
                            for(var i=0;i<special_recipients.length;i++) {
                                filterEmp.push(special_recipients[i].nik);
                                var htm = '';
                                var oktosend = false;
                                htm = '<head>' +
                                '<style>' +
                                'table {' +
                                    'font-family: arial, sans-serif;' +
                                    'font-size:14px;' +
                                    'border-collapse: collapse;' +
                                    'width: 100%;' +
                                '}' +
                                
                                'td, th {' +
                                    'border: 1px solid #dddddd;' +
                                    'padding: 8px;' +
                                '}' +
                                
                                'tr:nth-child(even) {' +
                                    'background-color: #dddddd;' +
                                '}' +
                                
                            
                                '</style>' +
                                '</head>'; 
                                htm = htm + '<b>Kepada Bapak/Ibu ' + special_recipients[i].nama + '</b><p/>';
                                htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                                jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                                for (var t=0;t<jsonRes[z].reports.length;t++) {            
                                    if (special_recipients[i].email!=null && special_recipients[i].email!='' && special_recipients[i].email!=undefined) {
                                    
                                        var hdr = '';
                                        if (jsonRes[z].reports[t].rows.length>0) {
                                            var strheader = '';
                                            
                                        
                                            strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                            strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                            strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                            strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                            strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                            strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                            strheader = strheader.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                            strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                            strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                            strheader = strheader.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                            strheader = strheader.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                            strheader = strheader.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                            strheader = strheader.replace("$d30days$"," <b>" + helper.formatDateShort(d30days) + "</b> ");
                                            strheader = strheader.replace("$dtgl1$"," <b>" + helper.formatDateShort(dtgl1) + "</b> ");
                                            strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                            strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                            strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                            strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                            strheader = strheader.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                            strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");

                                            strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                            strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                            strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                            strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");
                                            htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                            hdr = '<tr>'
                                            for (var key in jsonRes[z].reports[t].rows[0]) {
                                                var found = false;
                                                var count = 0;
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                    } 
                                                    count++;
                                                }
                                                //sini plant 1
                                                found = false;
                                                count = 0;
                                                //console.log('sini ' + jsonRes[z].reports[t].isplantvisible)
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        //console.log('sni ' + jsonRes[z].reports[t].isplantvisible)
                                                        if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                        {
                                                            if (jsonRes[z].reports[t].isplantvisible==0)
                                                                hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                        }
                                                        else
                                                            hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                    }
                                                    count++;
                                                }
                                            }
                                            hdr = hdr + '</tr>'
                                        }
                                
                                        plantSelected = [];
                                        for(var c=0;c<special_recipients[i].plants.length;c++) {
                                            plantSelected.push(
                                                special_recipients[i].plants[c].value
                                            )
                                        }
                                        plantSelected.push('KMTR');
                                        //console.log(JSON.stringify(plantSelected))
                                        var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                        var rows = jsonRes[z].reports[t].rows;
                                        var rowsfiltered = rows;
                                        
                                        //TODO: sini plant modifikasi
                                        if (jsonRes[z].reports[t].isperorganization==1) {
                                            var strpos = special_recipients[i].posisi + ' ' + special_recipients[i].divisi + ' ' + special_recipients[i].dept;
                                            rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                        }
                                        else
                                            rowsfiltered = rows.filter(row => plantSelected.includes(row[plantcolname]));
                                        
                                        if (jsonRes[z].reports[0].additionalsplitcolumn==1) {
                                            rowsfiltered = rowsfiltered.filter(row => addcolumn[r]==row[additionalcolumn]);
                                        }  

                                        console.log('filtered ' + JSON.stringify(rowsfiltered));
                                        var ctnt = '';
                                        var ftr = '';
                                        var rw = '';
                                        var isfooter = false;

                                        var checkfooter = false;
                                        if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                            checkfooter = true;
                                        }

                                        for (var h=0;h<rowsfiltered.length;h++) {
                                            oktosend = true;
                                            var bgcolor = "#F5F5F5";
                                            rw = '';
                                            if (checkfooter==true) {
                                                for (var key in rowsfiltered[h]) {
                                                    var found = false;
                                                    var count = 0;
                                                    var val = rowsfiltered[h][key];
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                        if (key==jsonRes[z].reports[t].isfooter) {
                                                            if (val==1 || val=='1' || val==true) {
                                                                bgcolor = "#2b471d";
                                                                isfooter = true;
                                                                found = true;
                                                            }  
                                                        }
                                                        count++;
                                                    }
                                                }
                                            }
                                            var targetfound = false;
                                            var targetfound2 = false;
                                            var target = 0;
                                            var target2 = 0;
                                            for (var key in rowsfiltered[h]) {
                                                var val = rowsfiltered[h][key];
                                                var valx = val;

                                                var count = 0;
                                            
                                                while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                        targetfound = true;
                                                        target = valx;
                                                    }
                                                    count++;
                                                } 

                                                var count = 0;
                                            
                                                while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                        targetfound2 = true;
                                                        target2 = valx;
                                                    }
                                                    count++;
                                                } 

                                                var count = 0; 
                
                                                while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                        targetfound3 = true;
                                                        target3 = valx;
                                                    }
                                                    count++;
                                                } 



                                                var found = false;
                                                var count = 0;

                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {

                                                    if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        if (val!=null && val!=undefined) {
                                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                case 0:
                                                                    val = val;
                                                                    break;
                                                                case 1:
                                                                    val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 2:
                                                                    val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 3:
                                                                    val = helper.formatDateShort(val)
                                                                    break;
                                                                case 4:
                                                                    val = helper.formatDate(val)
                                                                    break;
                                                            }
                                                        }
                                                        var bgcolor2 = bgcolor;
                                                        var fontcolor = "<font>";
                                                        if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                if (valx>target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                if (valx>target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                if (valx>target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                            if (valx<0) fontcolor = "<font>";
                                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                            if (isfooter==false) 
                                                                bgcolor2 = "#b5ddc3";
                                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                        }
                                                    }
                                                    count++;
                                                } 
                                                //sini plant 2
                                                found = false;
                                                count = 0;
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                {
                                                    if (key==jsonRes[z].reports[t].plantcolumn) {
                                                        found = true;
                                                        if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                            if (jsonRes[z].reports[t].isplantvisible==0) {
                                                                /*
                                                                if (val!=null && val!=undefined) {
                                                                    switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                        case 0:
                                                                            val = val;
                                                                            break;
                                                                        case 1:
                                                                            val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 2:
                                                                            val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 3:
                                                                            val = helper.formatDateShort(val)
                                                                            break;
                                                                        case 4:
                                                                            val = helper.formatDate(val)
                                                                            break;
                                                                    }
                                                                }
                                                                */
                                                                var bgcolor2 = bgcolor;
                                                                if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                    if (isfooter==false) 
                                                                        bgcolor2 = "#b5ddc3";
                                                                var fontcolor = "<font>";
                                                                if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                        if (valx>target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                        if (valx>target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                        if (valx>target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                    if (valx<0) fontcolor = "<font>";
                                                                if (key!=jsonRes[z].reports[t].isfooter) {
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            if (val!=null && val!=undefined) {
                                                                /*
                                                                switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                    case 0:
                                                                        val = val;
                                                                        break;
                                                                    case 1:
                                                                        val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 2:
                                                                        val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 3:
                                                                        val = helper.formatDateShort(val)
                                                                        break;
                                                                    case 4:
                                                                        val = helper.formatDate(val)
                                                                        break;
                                                                }
                                                                */
                                                            }
                                                            var bgcolor2 = bgcolor;
                                                            if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                if (isfooter==false) 
                                                                    bgcolor2 = "#b5ddc3";
                                                            var fontcolor = "<font>";
                                                            if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                    if (valx>target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                    if (valx>target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                    if (valx>target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                if (valx<0) fontcolor = "<font>";
                                                            if (key!=jsonRes[z].reports[t].isfooter) {
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            }
                                                        }
                                                    }
                                                    count++;
                                                } 

                                            }
                                            if (isfooter==true) {
                                                isfooter = false;
                                                ftr = '<tr bgcolor=' + bgcolor + ' style="color:#FFFFFF">' + rw + '</tr>';
                                            }
                                            else {
                                                ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                            }
                                        }
                                        var strfooter = '';
                                        strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                        strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                        strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                        strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                        strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                        strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                        strfooter = strfooter.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                        strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                        strfooter = strfooter.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                        strfooter = strfooter.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                        strfooter = strfooter.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                        strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                        strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                        strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                        strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                        strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                        strfooter = strfooter.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                        strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                        if (ctnt!='')
                                            htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                        //else 
                                        //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                    }
                                }

                                htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                                var subject = ''
                                if (jsonRes[z].reports[0].additionalsplitcolumn==1)
                                    subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname + ' - ' + addcolumn[r] + ' - ' + helper.formatDateShort(new Date());
                                else
                                    subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname;
                                var mailOptions = {
                                    from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                                    to: special_recipients[i].email,//narendra.adinugraha@kiranamegatara.com
                                    subject: subject,
                                    html: htm,
                                };
                                if (oktosend==true) {
                                    console.log('SPECIAL : ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                    if (special_recipients[i].email!='' && special_recipients[i].email!=null && special_recipients[i].email!=undefined)
                                    //console.log(JSON.stringify(mailOptions));
                                        if (validateEmail(special_recipients[i].email)) sendMailQueue.add(mailOptions);
                                    //transport.sendMail(mailOptions, (error, info) => {
                                    //    if (error) {
                                    //        console.log(error);
                                    //        writeErrorLog('Error sending email to ' + special_recipients[i].email);
                                    //    }
                                    //});
                                }
                                else {
                                    console.log('empty special : ' + special_recipients[i].nama + ' ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                }
                            }

                            //NOTE: HR Recipient 
                            for(var i=0;i<jsonRes[z].ex_recipients.length;i++) {
                                filterEmp.push(jsonRes[z].ex_recipients[i].nik);
                            }
                            var rowsEmpFiltered =  [];
                            if (jsonRes[z].HR_recipients.length>0) {
                                var HR_emp  = [];
                                for(var i=0;i<jsonRes[z].HR_recipients.length;i++) {
                                    for(var j=0;j<jsonRes[z].HR_recipients[i].length;j++) {
                                        HR_emp.push(jsonRes[z].HR_recipients[i][j]);
                                    }
                                }
                                if (filterEmp.length>0)
                                    rowsEmpFiltered = HR_emp.filter(row => !filterEmp.includes(row.nik));
                                else 
                                    rowsEmpFiltered = HR_emp;
                            
                                rowsEmpFiltered = removeDuplicates(rowsEmpFiltered);
                                for (var g=0;g<rowsEmpFiltered.length;g++) {
                                    //for(var i=0;i<rowsEmpFiltered[g].length;i++) {
                                        var htm = '';
                                        var oktosend = false;
                                        htm = '<head>' +
                                                '<style>' +
                                                'table {' +
                                                    'font-family: arial, sans-serif;' +
                                                    'font-size:14px;' +
                                                    'border-collapse: collapse;' +
                                                    'width: 100%;' +
                                                '}' +
                                                
                                                'td, th {' +
                                                    'border: 1px solid #dddddd;' +
                                                    'padding: 8px;' +
                                                '}' +
                                                
                                                'tr:nth-child(even) {' +
                                                    'background-color: #dddddd;' +
                                                '}' +
                                                
                                            
                                                '</style>' +
                                                '</head>'; 
                                                htm = htm + '<b>Kepada Bapak/Ibu ' + rowsEmpFiltered[g].nama + '</b><p/>';
                                                htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                                        jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                                        for (var t=0;t<jsonRes[z].reports.length;t++) {
                                            if (rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=undefined) {
                                                
                                                var hdr = '';
                                                if (jsonRes[z].reports[t].rows.length>0) {
                                                    var strheader = '';
                                                    strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                                    strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                                    strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                                    strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                                    strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                                    strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                                    strheader = strheader.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                                    strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                                    strheader = strheader.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                                    strheader = strheader.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                                    strheader = strheader.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                                    strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                                    strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                                    strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                                    strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                                    strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                                    strheader = strheader.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                                    strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                                    
                                                    strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                                    strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                                    strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                                    strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");
                                                    
                                                    htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                                
                                                    hdr = '<tr>'
                                                    for (var key in jsonRes[z].reports[t].rows[0]) {
                                                        var found = false;
                                                        var count = 0;
                                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {
                                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                                found = true;
                                                                hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                            }
                                                            count++;
                                                        }
                                                        //sini plant 1
                                                        found = false;
                                                        count = 0;
                                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {
                                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                                found = true;
                                                                if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                                {
                                                                    if (jsonRes[z].reports[t].isplantvisible==0)
                                                                        hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                                }
                                                                else
                                                                    hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                            }
                                                            count++;
                                                        }
                                                    }
                                                    hdr = hdr + '</tr>'
                                                }
                                    
                                                var rows = jsonRes[z].reports[t].rows;
                                                var rowsfiltered = rows;
                                                //TODO: sini plant modifikasi
                                                
                                                if (jsonRes[z].reports[t].isperorganization==1) {
                                                    var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                    if (rowsEmpFiltered[g].ho!='n') {
                                                        var strpos = rowsEmpFiltered[g].posst + ' ' + rowsEmpFiltered[g].divisi + ' ' + rowsEmpFiltered[g].dept;
                                                        rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                                    }
                                                    else {
                                                        rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                                    }
                                                }
                                                else {
                                                    if (rowsEmpFiltered[g].ho=='n') {
                                                        var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                        rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                                    }
                                                }
                                                plantSelected.push('KMTR');
                                                if (jsonRes[z].reports[0].additionalsplitcolumn==1) {
                                                    rowsfiltered = rowsfiltered.filter(row => addcolumn[r]==row[additionalcolumn]);
                                                }
                                                var ctnt = '';
                                                var ftr = '';
                                                var rw = '';
                                                var isfooter = false;

                                                var checkfooter = false;
                                                if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                                    checkfooter = true;
                                                }
                                                for (var h=0;h<rowsfiltered.length;h++) {
                                                    oktosend = true;
                                                    var bgcolor = "#F5F5F5";
                                                    rw = '';
                                                    if (checkfooter==true) {
                                                        for (var key in rowsfiltered[h]) {
                                                            var val = rowsfiltered[h][key];
                                                            var found = false;
                                                            var count = 0;
                                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                                if (key==jsonRes[z].reports[t].isfooter) {
                                                                    if (val==1 || val=='1' || val==true) {
                                                                        bgcolor = "#2b471d";
                                                                        isfooter = true;
                                                                        found = true;
                                                                    }  
                                                                }
                                                                count++;
                                                            }
                                                        }
                                                    }
                                                    var targetfound = false;
                                                    var targetfound2 = false;
                                                    var targetfound3 = false;
                                                    var target = 0;
                                                    var target2 = 0;
                                                    var target3 = 0;
                                                    for (var key in rowsfiltered[h]) {
                                                        var val = rowsfiltered[h][key];
                                                        var valx = val;

                                                        var count = 0;
                                                    
                                                        while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {
                                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                                targetfound = true;
                                                                target = valx;
                                                            }
                                                            count++;
                                                        } 

                                                        var count = 0;
                                                    
                                                        while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {
                                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                                targetfound2 = true;
                                                                target2 = valx;
                                                            }
                                                            count++;
                                                        } 

                                                        var count = 0; 
                    
                                                        while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {
                                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                                targetfound3 = true;
                                                                target3 = valx;
                                                            }
                                                            count++;
                                                        } 


                                                        var found = false;
                                                        var count = 0;
                                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {

                                                            if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                                found = true;
                                                                if (val!=null && val!=undefined) {
                                                                    switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                        case 0:
                                                                            val = val;
                                                                            break;
                                                                        case 1:
                                                                            val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 2:
                                                                            val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 3:
                                                                            val = helper.formatDateShort(val)
                                                                            break;
                                                                        case 4:
                                                                            val = helper.formatDate(val)
                                                                            break;
                                                                    }
                                                                }
                                                                var bgcolor2 = bgcolor;
                                                                if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                    if (isfooter==false) 
                                                                        bgcolor2 = "#b5ddc3";
                                                                var fontcolor = "<font>";
                                                                if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                        if (valx>target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                        if (valx>target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                        if (valx>target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                    if (valx<0) fontcolor = "<font>";
                                                                if (key!=jsonRes[z].reports[t].isfooter) {
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                }
                                                            }
                                                            count++;
                                                        }
                                                        //sini plant 2
                                                        found = false;
                                                        count = 0;
                                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                        {
                                                            if (key==jsonRes[z].reports[t].plantcolumn) {
                                                                found = true;
                                                                if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                                    if (jsonRes[z].reports[t].isplantvisible==0) {
                                                                        /*
                                                                        if (val!=null && val!=undefined) {
                                                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                                case 0:
                                                                                    val = val;
                                                                                    break;
                                                                                case 1:
                                                                                    val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                    break;
                                                                                case 2:
                                                                                    val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                    break;
                                                                                case 3:
                                                                                    val = helper.formatDateShort(val)
                                                                                    break;
                                                                                case 4:
                                                                                    val = helper.formatDate(val)
                                                                                    break;
                                                                            }
                                                                        }
                                                                        */
                                                                        var bgcolor2 = bgcolor;
                                                                        var fontcolor = "<font>";
                                                                        if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                                if (valx>target) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                if (valx<target) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                        if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                                if (valx>target2) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                if (valx<target2) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                        if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                                if (valx>target3) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                if (valx<target3) {
                                                                                    fontcolor = "<font>"
                                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                    //    fontcolor = "<font color='#009900'>";
                                                                                    //else
                                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                                }
                                                                            }
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                            if (valx<0) fontcolor = "<font>";
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                            if (isfooter==false) 
                                                                                bgcolor2 = "#b5ddc3";
                                                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                        }
                                                                    }
                                                                }
                                                                else {
                                                                    if (val!=null && val!=undefined) {
                                                                        /*
                                                                        switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                            case 0:
                                                                                val = val;
                                                                                break;
                                                                            case 1:
                                                                                val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 2:
                                                                                val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 3:
                                                                                val = helper.formatDateShort(val)
                                                                                break;
                                                                            case 4:
                                                                                val = helper.formatDate(val)
                                                                                break;
                                                                        }
                                                                        */
                                                                    }
                                                                    var bgcolor2 = bgcolor;
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                        if (isfooter==false) 
                                                                            bgcolor2 = "#b5ddc3";
                                                                    var fontcolor = "<font>";
                                                                    if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                            if (valx>target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                            if (valx>target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                            if (valx>target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                        if (valx<0) fontcolor = "<font>";
                                                                    if (key!=jsonRes[z].reports[t].isfooter) {
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    }
                                                                }
                                                            }
                                                            count++;
                                                        } 
                                                    }
                                                    if (isfooter==true) {
                                                        isfooter = false;
                                                        ftr = '<tr bgcolor=' + bgcolor + ' style="color:#FFFFFF">' + rw + '</tr>';
                                                    }
                                                    else {
                                                        ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                                    }
                                                }
                                                var strfooter = '';
                                                strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                                strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                                strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                                strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                                strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                                strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                                strfooter = strfooter.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                                strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                                strfooter = strfooter.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                                strfooter = strfooter.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                                strfooter = strfooter.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                                strfooter = strfooter.replace("$d30days$"," <b>" + helper.formatDateShort(d30days) + "</b> ");
                                                strfooter = strfooter.replace("$dtgl1$"," <b>" + helper.formatDateShort(dtgl1) + "</b> ");
                                                strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                                strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                                strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                                strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                                strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                                strfooter = strfooter.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                                strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                                if (ctnt!='')
                                                    htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                                //else 
                                                //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                            }
                                        }
                                        htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                                        //console.log(htm);
                                        var subject = '';
                                        if (jsonRes[z].reports[0].additionalsplitcolumn==1)
                                            subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname + ' - ' + addcolumn[r]  + ' - ' + helper.formatDateShort(new Date());
                                        else
                                            subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname;
                                        var mailOptions = {
                                            from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                                            to: rowsEmpFiltered[g].email,//narendra.adinugraha@kiranamegatara.com
                                            subject: subject,
                                            html: htm,
                                        };
                                        if (oktosend==true) {
                                            console.log('HR : ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                            if (rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!=undefined)
                                                //console.log(JSON.stringify(mailOptions));
                                                if (validateEmail(rowsEmpFiltered[g].email)) sendMailQueue.add(mailOptions);
                                                //transport.sendMail(mailOptions, (error, info) => {
                                                //if (error) {
                                                //    console.log(error);
                                                //    writeErrorLog('Error sending email to ' + rowsEmpFiltered[g].email,error);
                                                //}
                                            //});
                                        }
                                        else {
                                            console.log('empty HR : ' + rowsEmpFiltered[g].nama + ' ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                        }
                                    //}
                                }
                            }
                        }
                    }
                    else {
                        //NOTE: Special Recipient
                        for(var i=0;i<special_recipients.length;i++) {
                            filterEmp.push(special_recipients[i].nik);
                            var htm = '';
                            var oktosend = false;
                            htm = '<head>' +
                            '<style>' +
                            'table {' +
                                'font-family: arial, sans-serif;' +
                                'font-size:14px;' +
                                'border-collapse: collapse;' +
                                'width: 100%;' +
                            '}' +
                            
                            'td, th {' +
                                'border: 1px solid #dddddd;' +
                                'padding: 8px;' +
                            '}' +
                            
                            'tr:nth-child(even) {' +
                                'background-color: #dddddd;' +
                            '}' +
                            
                        
                            '</style>' +
                            '</head>'; 
                            htm = htm + '<b>Kepada Bapak/Ibu ' + special_recipients[i].nama + '</b><p/>';
                            htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                            jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                            for (var t=0;t<jsonRes[z].reports.length;t++) {            
                                if (special_recipients[i].email!=null && special_recipients[i].email!='' && special_recipients[i].email!=undefined) {
                                
                                    var hdr = '';
                                    if (jsonRes[z].reports[t].rows.length>0) {
                                        var strheader = '';
                                        
                                    
                                        strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                        strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                        strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                        strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                        strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                        strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                        strheader = strheader.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                        strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                        strheader = strheader.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                        strheader = strheader.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                        strheader = strheader.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                        strheader = strheader.replace("$d30days$"," <b>" + helper.formatDateShort(d30days) + "</b> ");
                                        strheader = strheader.replace("$dtgl1$"," <b>" + helper.formatDateShort(dtgl1) + "</b> ");
                                        strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                        strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                        strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                        strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                        strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                        strheader = strheader.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                        strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");

                                        strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                        strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                        strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                        strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");
                                        htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                        hdr = '<tr>'
                                        for (var key in jsonRes[z].reports[t].rows[0]) {
                                            var found = false;
                                            var count = 0;
                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {
                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                    found = true;
                                                    hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                } 
                                                count++;
                                            }
                                            //sini plant 1
                                            found = false;
                                            count = 0;
                                            //console.log('sini ' + jsonRes[z].reports[t].isplantvisible)
                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {
                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                    found = true;
                                                    //console.log('sni ' + jsonRes[z].reports[t].isplantvisible)
                                                    if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                    {
                                                        if (jsonRes[z].reports[t].isplantvisible==0)
                                                            hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                    }
                                                    else
                                                        hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                }
                                                count++;
                                            }
                                        }
                                        hdr = hdr + '</tr>'
                                    }
                            
                                    plantSelected = [];
                                    for(var c=0;c<special_recipients[i].plants.length;c++) {
                                        plantSelected.push(
                                            special_recipients[i].plants[c].value
                                        )
                                    }
                                    plantSelected.push('KMTR');
                                    //console.log(JSON.stringify(plantSelected))
                                    var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                    var rows = jsonRes[z].reports[t].rows;
                                    var rowsfiltered = rows;
                                    
                                    //TODO: sini plant modifikasi
                                    if (jsonRes[z].reports[t].isperorganization==1) {
                                        var strpos = special_recipients[i].posisi + ' ' + special_recipients[i].divisi + ' ' + special_recipients[i].dept;
                                        rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                    }
                                    else
                                        rowsfiltered = rows.filter(row => plantSelected.includes(row[plantcolname]));
                                    //console.log(JSON.stringify(rowsfiltered));
                                    var ctnt = '';
                                    var ftr = '';
                                    var rw = '';
                                    var isfooter = false;

                                    var checkfooter = false;
                                    if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                        checkfooter = true;
                                    }

                                    for (var h=0;h<rowsfiltered.length;h++) {
                                        oktosend = true;
                                        var bgcolor = "#F5F5F5";
                                        rw = '';
                                        if (checkfooter==true) {
                                            for (var key in rowsfiltered[h]) {
                                                var found = false;
                                                var count = 0;
                                                var val = rowsfiltered[h][key];
                                                while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                    if (key==jsonRes[z].reports[t].isfooter) {
                                                        if (val==1 || val=='1' || val==true) {
                                                            bgcolor = "#2b471d";
                                                            isfooter = true;
                                                            found = true;
                                                        }  
                                                    }
                                                    count++;
                                                }
                                            }
                                        }
                                        var targetfound = false;
                                        var targetfound2 = false;
                                        var target = 0;
                                        var target2 = 0;
                                        for (var key in rowsfiltered[h]) {
                                            var val = rowsfiltered[h][key];
                                            var valx = val;

                                            var count = 0;
                                        
                                            while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {
                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                    targetfound = true;
                                                    target = valx;
                                                }
                                                count++;
                                            } 

                                            var count = 0;
                                        
                                            while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {
                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                    targetfound2 = true;
                                                    target2 = valx;
                                                }
                                                count++;
                                            } 

                                            var count = 0; 
            
                                            while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {
                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                    targetfound3 = true;
                                                    target3 = valx;
                                                }
                                                count++;
                                            } 



                                            var found = false;
                                            var count = 0;

                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {

                                                if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                    found = true;
                                                    if (val!=null && val!=undefined) {
                                                        switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                            case 0:
                                                                val = val;
                                                                break;
                                                            case 1:
                                                                val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                break;
                                                            case 2:
                                                                val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                break;
                                                            case 3:
                                                                val = helper.formatDateShort(val)
                                                                break;
                                                            case 4:
                                                                val = helper.formatDate(val)
                                                                break;
                                                        }
                                                    }
                                                    var bgcolor2 = bgcolor;
                                                    var fontcolor = "<font>";
                                                    if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                            if (valx>target) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                        else {
                                                            if (valx<target) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                    if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                            if (valx>target2) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                        else {
                                                            if (valx<target2) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                    if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                            if (valx>target3) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                        else {
                                                            if (valx<target3) {
                                                                fontcolor = "<font>"
                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                //    fontcolor = "<font color='#009900'>";
                                                                //else
                                                                //    fontcolor = "<font color='#FF0000'>";
                                                            }
                                                        }
                                                    if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                        if (valx<0) fontcolor = "<font>";
                                                    if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                        if (isfooter==false) 
                                                            bgcolor2 = "#b5ddc3";
                                                    if (key!=jsonRes[z].reports[t].isfooter) {
                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                    }
                                                }
                                                count++;
                                            } 
                                            //sini plant 2
                                            found = false;
                                            count = 0;
                                            while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                            {
                                                if (key==jsonRes[z].reports[t].plantcolumn) {
                                                    found = true;
                                                    if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                        if (jsonRes[z].reports[t].isplantvisible==0) {
                                                            /*
                                                            if (val!=null && val!=undefined) {
                                                                switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                    case 0:
                                                                        val = val;
                                                                        break;
                                                                    case 1:
                                                                        val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 2:
                                                                        val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 3:
                                                                        val = helper.formatDateShort(val)
                                                                        break;
                                                                    case 4:
                                                                        val = helper.formatDate(val)
                                                                        break;
                                                                }
                                                            }
                                                            */
                                                            var bgcolor2 = bgcolor;
                                                            if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                if (isfooter==false) 
                                                                    bgcolor2 = "#b5ddc3";
                                                            var fontcolor = "<font>";
                                                            if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                    if (valx>target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                    if (valx>target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                    if (valx>target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                if (valx<0) fontcolor = "<font>";
                                                            if (key!=jsonRes[z].reports[t].isfooter) {
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        if (val!=null && val!=undefined) {
                                                            /*
                                                            switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                case 0:
                                                                    val = val;
                                                                    break;
                                                                case 1:
                                                                    val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 2:
                                                                    val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                    break;
                                                                case 3:
                                                                    val = helper.formatDateShort(val)
                                                                    break;
                                                                case 4:
                                                                    val = helper.formatDate(val)
                                                                    break;
                                                            }
                                                            */
                                                        }
                                                        var bgcolor2 = bgcolor;
                                                        if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                            if (isfooter==false) 
                                                                bgcolor2 = "#b5ddc3";
                                                        var fontcolor = "<font>";
                                                        if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                if (valx>target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                if (valx>target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target2) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                            if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                if (valx>target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                            else {
                                                                if (valx<target3) {
                                                                    fontcolor = "<font>"
                                                                    //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                    //    fontcolor = "<font color='#009900'>";
                                                                    //else
                                                                    //    fontcolor = "<font color='#FF0000'>";
                                                                }
                                                            }
                                                        if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                            if (valx<0) fontcolor = "<font>";
                                                        if (key!=jsonRes[z].reports[t].isfooter) {
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                        }
                                                    }
                                                }
                                                count++;
                                            } 

                                        }
                                        if (isfooter==true) {
                                            isfooter = false;
                                            ftr = '<tr bgcolor=' + bgcolor + ' style="color:#FFFFFF">' + rw + '</tr>';
                                        }
                                        else {
                                            ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                        }
                                    }
                                    var strfooter = '';
                                    strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                    strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                    strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                    strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                    strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                    strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                    strfooter = strfooter.replace("$d7days$"," <b>" +helper.formatDateShort(d7days) + "</b> ");
                                    strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                    strfooter = strfooter.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                    strfooter = strfooter.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                    strfooter = strfooter.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                    strfooter = strfooter.replace("$d30days$"," <b>" + helper.formatDateShort(d30days) + "</b> ");
                                    strfooter = strfooter.replace("$dtgl1$"," <b>" + helper.formatDateShort(dtgl1) + "</b> ");
                                    strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                    strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                    strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                    strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                    strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                    strfooter = strfooter.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                    strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                    if (ctnt!='')
                                        htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                    //else 
                                    //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                }
                            }

                            htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                            var subject = ''
                            if (jsonRes[z].reports[0].additionalsplitcolumn==1)
                                subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname + ' - ' + addcolumn[r] + ' - ' + helper.formatDateShort(new Date());
                            else
                                subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname;
                            var mailOptions = {
                                from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                                to: special_recipients[i].email,//narendra.adinugraha@kiranamegatara.com
                                subject: subject,
                                html: htm,
                            };
                            if (oktosend==true) {
                                console.log('SPECIAL : ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                if (special_recipients[i].email!='' && special_recipients[i].email!=null && special_recipients[i].email!=undefined)
                                //console.log(JSON.stringify(mailOptions));
                                    if (validateEmail(special_recipients[i].email)) sendMailQueue.add(mailOptions);
                                //transport.sendMail(mailOptions, (error, info) => {
                                //    if (error) {
                                //        console.log(error);
                                //        writeErrorLog('Error sending email to ' + special_recipients[i].email);
                                //    }
                                //});
                            }
                            else {
                                console.log('empty special : ' + special_recipients[i].nama + ' ' + special_recipients[i].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                            }
                        }

                        //NOTE: HR Recipient 
                        for(var i=0;i<jsonRes[z].ex_recipients.length;i++) {
                            filterEmp.push(jsonRes[z].ex_recipients[i].nik);
                        }
                        var rowsEmpFiltered =  [];
                        if (jsonRes[z].HR_recipients.length>0) {
                            var HR_emp  = [];
                            for(var i=0;i<jsonRes[z].HR_recipients.length;i++) {
                                for(var j=0;j<jsonRes[z].HR_recipients[i].length;j++) {
                                    HR_emp.push(jsonRes[z].HR_recipients[i][j]);
                                }
                            }
                            if (filterEmp.length>0)
                                rowsEmpFiltered = HR_emp.filter(row => !filterEmp.includes(row.nik));
                            else 
                                rowsEmpFiltered = HR_emp;
                        
                            rowsEmpFiltered = removeDuplicates(rowsEmpFiltered);
                            for (var g=0;g<rowsEmpFiltered.length;g++) {
                                //for(var i=0;i<rowsEmpFiltered[g].length;i++) {
                                    var htm = '';
                                    var oktosend = false;
                                    htm = '<head>' +
                                            '<style>' +
                                            'table {' +
                                                'font-family: arial, sans-serif;' +
                                                'font-size:14px;' +
                                                'border-collapse: collapse;' +
                                                'width: 100%;' +
                                            '}' +
                                            
                                            'td, th {' +
                                                'border: 1px solid #dddddd;' +
                                                'padding: 8px;' +
                                            '}' +
                                            
                                            'tr:nth-child(even) {' +
                                                'background-color: #dddddd;' +
                                            '}' +
                                            
                                        
                                            '</style>' +
                                            '</head>'; 
                                            htm = htm + '<b>Kepada Bapak/Ibu ' + rowsEmpFiltered[g].nama + '</b><p/>';
                                            htm = htm + 'Berikut adalah laporan yang dikirimkan dari Kiranalytics : <p/>';
                                    jsonRes[z].reports.sort(GetSortOrder("reportcode"));
                                    for (var t=0;t<jsonRes[z].reports.length;t++) {
                                        if (rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=undefined) {
                                            
                                            var hdr = '';
                                            if (jsonRes[z].reports[t].rows.length>0) {
                                                var strheader = '';
                                                strheader = jsonRes[z].reports[t].reportheader.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                                strheader = strheader.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                                strheader = strheader.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                                strheader = strheader.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                                strheader = strheader.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                                strheader = strheader.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                                strheader = strheader.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                                strheader = strheader.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                                strheader = strheader.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                                strheader = strheader.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                                strheader = strheader.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                                strheader = strheader.replace("$d30days$"," <b>" + helper.formatDateShort(d30days) + "</b> ");
                                                strheader = strheader.replace("$dtgl1$"," <b>" + helper.formatDateShort(dtgl1) + "</b> ");
                                                strheader = strheader.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                                strheader = strheader.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                                strheader = strheader.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                                strheader = strheader.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                                strheader = strheader.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                                strheader = strheader.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                                strheader = strheader.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                                
                                                strheader = strheader.replace("$dnextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),1)) + "</b> ");
                                                strheader = strheader.replace("$d2nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),2)) + "</b> ");
                                                strheader = strheader.replace("$d3nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),3)) + "</b> ");
                                                strheader = strheader.replace("$d4nextmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),4)) + "</b> ");
                                                
                                                htm = htm + '<div><font size="5" color="#2b471d"><b><u>' + jsonRes[z].reports[t].reportcode + ' - ' + jsonRes[z].reports[t].reportname + '</u></b></font><p/>' + strheader + '<br/>'
                                            
                                                hdr = '<tr>'
                                                for (var key in jsonRes[z].reports[t].rows[0]) {
                                                    var found = false;
                                                    var count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].isfooter && key!=jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                        }
                                                        count++;
                                                    }
                                                    //sini plant 1
                                                    found = false;
                                                    count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key==jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined)
                                                            {
                                                                if (jsonRes[z].reports[t].isplantvisible==0)
                                                                    hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                            }
                                                            else
                                                                hdr = hdr + '<th bgcolor="#2b471d" style="color:#FFFFFF">' + jsonRes[z].reports[t].columnnames[count].displayname + '</th>'
                                                        }
                                                        count++;
                                                    }
                                                }
                                                hdr = hdr + '</tr>'
                                            }
                                
                                            var rows = jsonRes[z].reports[t].rows;
                                            var rowsfiltered = rows;
                                            //TODO: sini plant modifikasi
                                            
                                            if (jsonRes[z].reports[t].isperorganization==1) {
                                                var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                if (rowsEmpFiltered[g].ho!='n') {
                                                    var strpos = rowsEmpFiltered[g].posst + ' ' + rowsEmpFiltered[g].divisi + ' ' + rowsEmpFiltered[g].dept;
                                                    rowsfiltered = rows.filter(row => strpos.includes((row[plantcolname]).toString().replace("DIVISION","").replace("DIV","").replace("DEPARTMENT","").replace("DEPT","").substr(1)))
                                                }
                                                else {
                                                    rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                                }
                                            }
                                            else {
                                                if (rowsEmpFiltered[g].ho=='n') {
                                                    var plantcolname = jsonRes[z].reports[t].plantcolumn;
                                                    rowsfiltered = rows.filter(row => row[plantcolname] == rowsEmpFiltered[g].id_gedung);
                                                }
                                            }
                                            plantSelected.push('KMTR');
                                            var ctnt = '';
                                            var ftr = '';
                                            var rw = '';
                                            var isfooter = false;

                                            var checkfooter = false;
                                            if (jsonRes[z].reports[t].isfooter!='' && jsonRes[z].reports[t].isfooter!=null && jsonRes[z].reports[t].isfooter!=undefined) {
                                                checkfooter = true;
                                            }
                                            for (var h=0;h<rowsfiltered.length;h++) {
                                                oktosend = true;
                                                var bgcolor = "#F5F5F5";
                                                rw = '';
                                                if (checkfooter==true) {
                                                    for (var key in rowsfiltered[h]) {
                                                        var val = rowsfiltered[h][key];
                                                        var found = false;
                                                        var count = 0;
                                                        while (found==false && count<jsonRes[z].reports[t].columnnames.length) {
                                                            if (key==jsonRes[z].reports[t].isfooter) {
                                                                if (val==1 || val=='1' || val==true) {
                                                                    bgcolor = "#2b471d";
                                                                    isfooter = true;
                                                                    found = true;
                                                                }  
                                                            }
                                                            count++;
                                                        }
                                                    }
                                                }
                                                var targetfound = false;
                                                var targetfound2 = false;
                                                var targetfound3 = false;
                                                var target = 0;
                                                var target2 = 0;
                                                var target3 = 0;
                                                for (var key in rowsfiltered[h]) {
                                                    var val = rowsfiltered[h][key];
                                                    var valx = val;

                                                    var count = 0;
                                                
                                                    while (targetfound==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget==1) {
                                                            targetfound = true;
                                                            target = valx;
                                                        }
                                                        count++;
                                                    } 

                                                    var count = 0;
                                                
                                                    while (targetfound2==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget2==1) {
                                                            targetfound2 = true;
                                                            target2 = valx;
                                                        }
                                                        count++;
                                                    } 

                                                    var count = 0; 
                
                                                    while (targetfound3==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn && jsonRes[z].reports[t].columnnames[count].columntarget3==1) {
                                                            targetfound3 = true;
                                                            target3 = valx;
                                                        }
                                                        count++;
                                                    } 


                                                    var found = false;
                                                    var count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {

                                                        if (key==jsonRes[z].reports[t].columnnames[count].columnname && key!=jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            if (val!=null && val!=undefined) {
                                                                switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                    case 0:
                                                                        val = val;
                                                                        break;
                                                                    case 1:
                                                                        val = Number(val).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 2:
                                                                        val = Number(val).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                        break;
                                                                    case 3:
                                                                        val = helper.formatDateShort(val)
                                                                        break;
                                                                    case 4:
                                                                        val = helper.formatDate(val)
                                                                        break;
                                                                }
                                                            }
                                                            var bgcolor2 = bgcolor;
                                                            if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                if (isfooter==false) 
                                                                    bgcolor2 = "#b5ddc3";
                                                            var fontcolor = "<font>";
                                                            if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                    if (valx>target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                    if (valx>target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target2) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                    if (valx>target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                                else {
                                                                    if (valx<target3) {
                                                                        fontcolor = "<font>"
                                                                        //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                        //    fontcolor = "<font color='#009900'>";
                                                                        //else
                                                                        //    fontcolor = "<font color='#FF0000'>";
                                                                    }
                                                                }
                                                            if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                if (valx<0) fontcolor = "<font>";
                                                            if (key!=jsonRes[z].reports[t].isfooter) {
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                    rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                            }
                                                        }
                                                        count++;
                                                    }
                                                    //sini plant 2
                                                    found = false;
                                                    count = 0;
                                                    while (found==false && count<jsonRes[z].reports[t].columnnames.length)
                                                    {
                                                        if (key==jsonRes[z].reports[t].plantcolumn) {
                                                            found = true;
                                                            if (jsonRes[z].reports[t].isplantvisible!=null && jsonRes[z].reports[t].isplantvisible!=undefined) {
                                                                if (jsonRes[z].reports[t].isplantvisible==0) {
                                                                    /*
                                                                    if (val!=null && val!=undefined) {
                                                                        switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                            case 0:
                                                                                val = val;
                                                                                break;
                                                                            case 1:
                                                                                val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 2:
                                                                                val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                                break;
                                                                            case 3:
                                                                                val = helper.formatDateShort(val)
                                                                                break;
                                                                            case 4:
                                                                                val = helper.formatDate(val)
                                                                                break;
                                                                        }
                                                                    }
                                                                    */
                                                                    var bgcolor2 = bgcolor;
                                                                    var fontcolor = "<font>";
                                                                    if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                            if (valx>target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                            if (valx>target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target2) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                        if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                            if (valx>target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                        else {
                                                                            if (valx<target3) {
                                                                                fontcolor = "<font>"
                                                                                //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                                //    fontcolor = "<font color='#009900'>";
                                                                                //else
                                                                                //    fontcolor = "<font color='#FF0000'>";
                                                                            }
                                                                        }
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                        if (valx<0) fontcolor = "<font>";
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                        if (isfooter==false) 
                                                                            bgcolor2 = "#b5ddc3";
                                                                    if (key!=jsonRes[z].reports[t].isfooter) {
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                        if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                            rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                if (val!=null && val!=undefined) {
                                                                    /*
                                                                    switch(jsonRes[z].reports[t].columnnames[count].formatcolumn){
                                                                        case 0:
                                                                            val = val;
                                                                            break;
                                                                        case 1:
                                                                            val = val.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 2:
                                                                            val = val.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                                            break;
                                                                        case 3:
                                                                            val = helper.formatDateShort(val)
                                                                            break;
                                                                        case 4:
                                                                            val = helper.formatDate(val)
                                                                            break;
                                                                    }
                                                                    */
                                                                }
                                                                var bgcolor2 = bgcolor;
                                                                if (jsonRes[z].reports[t].columnnames[count].columnhighlight==1)
                                                                    if (isfooter==false) 
                                                                        bgcolor2 = "#b5ddc3";
                                                                var fontcolor = "<font>";
                                                                if (targetfound==true && jsonRes[z].reports[t].columnnames[count].columnactual==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand1==1) {
                                                                        if (valx>target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound2==true && jsonRes[z].reports[t].columnnames[count].columnactual2==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand2==1) {
                                                                        if (valx>target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target2) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual2==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (targetfound3==true && jsonRes[z].reports[t].columnnames[count].columnactual3==1)
                                                                    if (jsonRes[z].reports[t].columnnames[count].columnoperand3==1) {
                                                                        if (valx>target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (valx<target3) {
                                                                            fontcolor = "<font>"
                                                                            //if (jsonRes[z].reports[t].columnnames[count].columncoloractual3==1)
                                                                            //    fontcolor = "<font color='#009900'>";
                                                                            //else
                                                                            //    fontcolor = "<font color='#FF0000'>";
                                                                        }
                                                                    }
                                                                if (jsonRes[z].reports[t].columnnames[count].columnconditional==1) 
                                                                    if (valx<0) fontcolor = "<font>";
                                                                if (key!=jsonRes[z].reports[t].isfooter) {
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==0)
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                    if (jsonRes[z].reports[t].columnnames[count].textalignment==1) 
                                                                        rw = rw + '<td bgcolor="' + bgcolor2 + '" align="right">' + fontcolor + (val==null || val=='null' ? '' : val) + '</font></td>'
                                                                }
                                                            }
                                                        }
                                                        count++;
                                                    } 
                                                }
                                                if (isfooter==true) {
                                                    isfooter = false;
                                                    ftr = '<tr bgcolor=' + bgcolor + ' style="color:#FFFFFF">' + rw + '</tr>';
                                                }
                                                else {
                                                    ctnt = ctnt + '<tr bgcolor=' + bgcolor + '>' + rw + '</tr>'
                                                }
                                            }
                                            var strfooter = '';
                                            strfooter = jsonRes[z].reports[t].reportfooter.replace("$dnow$","<b>" + helper.formatDateShort(dtnow)+ "</b>");
                                            strfooter = strfooter.replace("$dnowwtholiday$"," <b>" + dtnowwtholiday + "</b> ");
                                            strfooter = strfooter.replace("$d9days$"," <b>" + helper.formatDateShort(d9days) + "</b> ");
                                            strfooter = strfooter.replace("$d3days$"," <b>" + helper.formatDateShort(d3days) + "</b> ");
                                            strfooter = strfooter.replace("$dyes$"," <b>" + helper.formatDateShort(dyes) + "</b> ");
                                            strfooter = strfooter.replace("$dyesd$"," <b>" + dyesd + "</b> ");
                                            strfooter = strfooter.replace("$d7days$"," <b>" + helper.formatDateShort(d7days) + "</b> ");
                                            strfooter = strfooter.replace("$d8days$"," <b>" + helper.formatDateShort(d8days) + "</b> ");
                                            strfooter = strfooter.replace("$d90days$"," <b>" + helper.formatDateShort(d90days) + "</b> ");
                                            strfooter = strfooter.replace("$d120days$"," <b>" + helper.formatDateShort(d120days) + "</b> ");
                                            strfooter = strfooter.replace("$d7plusdays$"," <b>" + helper.formatDateShort(d7plusdays) + "</b> ");
                                            strfooter = strfooter.replace("$d30days$"," <b>" + helper.formatDateShort(d30days) + "</b> ");
                                            strfooter = strfooter.replace("$dtgl1$"," <b>" + helper.formatDateShort(dtgl1) + "</b> ");
                                            strfooter = strfooter.replace("$d14days$"," <b>" + helper.formatDateShort(d14days) + "</b> ");
                                            strfooter = strfooter.replace("$d15days$"," <b>" + helper.formatDateShort(d15days) + "</b> ");
                                            strfooter = strfooter.replace("$dcurrentmonth$"," <b>" + helper.formatDateYearMonth(new Date()) + "</b> ");
                                            strfooter = strfooter.replace("$dlastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-1)) + "</b> ");
                                            strfooter = strfooter.replace("$d2lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-2)) + "</b> ");
                                            strfooter = strfooter.replace("$d3lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-3)) + "</b> ");
                                            strfooter = strfooter.replace("$d6lastmonth$"," <b>" + helper.formatDateYearMonth(helper.addMonth(new Date(),-6)) + "</b> ");
                                            if (ctnt!='')
                                                htm = htm + '<table>' + hdr + ctnt + ftr + '</table>' + strfooter + '</div>&nbsp;<p/>&nbsp;<p/>';
                                            //else 
                                            //    htm = htm + '</div>&nbsp;<p/>&nbsp;<p/>';
                                        }
                                    }
                                    htm = htm + '&nbsp;<p/>Harap segera ditindaklanjuti.<br><b>Kiranalytics Auto-Mail System</b>'
                                    //console.log(htm);
                                    var subject = ''
                                    if (jsonRes[z].reports[0].additionalsplitcolumn==1)
                                        subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname + ' - ' + addcolumn[r] + ' - ' + helper.formatDateShort(new Date());
                                    else
                                        subject = jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname;
                                    var mailOptions = {
                                        from: '"Kirana Megatara" <no-reply@kiranamegatara.com>',
                                        to: rowsEmpFiltered[g].email,//narendra.adinugraha@kiranamegatara.com
                                        subject: subject,
                                        html: htm,
                                    };
                                    if (oktosend==true) {
                                        console.log('HR : ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                        if (rowsEmpFiltered[g].email!='' && rowsEmpFiltered[g].email!=null && rowsEmpFiltered[g].email!=undefined)
                                            //console.log(JSON.stringify(mailOptions));
                                            if (validateEmail(rowsEmpFiltered[g].email))sendMailQueue.add(mailOptions);
                                            //transport.sendMail(mailOptions, (error, info) => {
                                            //if (error) {
                                            //    console.log(error);
                                            //    writeErrorLog('Error sending email to ' + rowsEmpFiltered[g].email,error);
                                            //}
                                        //});
                                    }
                                    else {
                                        console.log('empty HR : ' + rowsEmpFiltered[g].nama + ' ' + rowsEmpFiltered[g].email + ' ' + jsonRes[z].topiccode + ' - ' + jsonRes[z].topicname);
                                    }
                                //}
                            }
                        }
                    }
                
                //var MongoClient = mongo.MongoClient;
                }
                catch(err) {
                    writeErrorLog('step try catch', topiccode + ' - ' + topicname + ' : ' + err);
                }
                //LOGs
            
                
            }
            //transport.close();
        });
    }
}

const sendMailQueue = new Queue('sendMail', {
    redis: {
      host: '127.0.0.1',
      port: 6379,
      //password: 'root'
    }
});


sendMailQueue.process(async job => { 
    //console.log('masuk queue ' + JSON.stringify(job.data))
    return sendMail(job.data).then(
        function(result) { 
            /* handle a successful result */ 
            //console.log('done sending email to ' + job.data.to);
        },
        function(error) { 
            /* handle an error */
            //console.log('error 3 : ' + JSON.stringify(error));
            writeErrorLog('Error sending email to ' + job.data.to,JSON.stringify(error)); 
            var mailOptions = {
                from: job.data.from,
                to: job.data.to,
                subject: job.data.subject,
                html: job.data.html,
                counter : (job.data.counter) ? job.data.counter + 1 : 1
            };

            if (validateEmail(mailOptions.to) && mailOptions.subject!=null && mailOptions.subject!=undefined && mailOptions.subject!='' && mailOptions.to!='' && mailOptions.to!='-')
                sendMailQueue.add(mailOptions);
            
        }
    );
});

function validateEmail(email) {
    return email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
}

function getTransport(result) {
    if (trsprt==null || trsprt==undefined) {
        var transport = nodemailer.createTransport({
            pool: true,
            maxConnections: 1,
            host: result[0].serveraddress,
            port: result[0].port,
            secure: true, // use TLS
            auth: {
                user: result[0].username,
                pass: result[0].pwd,
            }
        });
        trsprt = transport
        return trsprt;
    }
    else {
        return trsprt;
    }
}

function getMailServerInfo() {
    return new Promise((resolve, reject) => {
        if (rstMail1==null || rstMail1==undefined) {
            var MongoClient = mongo.MongoClient;
            MongoClient.connect(url.getDbString(), function(err, db) {
                if (err) {
                    console.log('apa ' + err)
                    reject(null)
                }
                else {
                    var dbo = db.db(dbname.getDbName());
                    var query = {}
                    dbo.collection("t_mail_servers").find(query).toArray(function(err, result) {
                        if (err) {
                            db.close();
                            reject(null)
                        }
                        else {
                            db.close();
                            rstMail1 = result
                            resolve(rstMail1)
                        }
                    });
                }
            });
        }
        else
            resolve(rstMail1)
    });
}

function sendMail(mailOptions) {
    return new Promise((resolve, reject) => {
        getMailServerInfo().then(function(result) {
            if (result==null) {
                reject('Error koneksi');
            }
            else {
                var noreply = result[0].noreply;
                var transport = getTransport(result);
                mailOptions.from = noreply;
                
                var mailOpt = {
                    from: mailOptions.from,
                    to: mailOptions.to,
                    subject: mailOptions.subject,
                    html: mailOptions.html,
                }
                //console.log('mail options 1 : ' + JSON.stringify(mailOptions))
                if (mailOptions.counter) {
                    if (mailOptions.counter>3) {
                        resolve();
                    }
                    else {
                        transport.sendMail(mailOpt, (error, info) => {
                            if (error) {
                                //console.log('Error mail server 2 : ' + JSON.stringify(error))
                                reject(error);
                            }
                            else {
                                resolve(info);
                            }
                        });
                    }
                }
                else {
                    transport.sendMail(mailOpt, (error, info) => {
                        if (error) {
                            //console.log('Error mail server 2 : ' + JSON.stringify(error))
                            reject(error);
                        }
                        else {
                            resolve(info);
                        }
                    });
                }
            }
        })
       
    
        
    })
}


function removeDuplicates(arr) {
    return [...new Set(
         arr.map(el => JSON.stringify(el))
       )].map(e => JSON.parse(e));
 }

